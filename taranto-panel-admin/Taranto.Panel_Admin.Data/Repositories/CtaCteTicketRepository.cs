﻿using Taranto.Panel_Admin.Data.Repositories.Interfaces;

namespace Taranto.Panel_Admin.Data.Repositories
{
    public class CtaCteTicketRepository : BaseCRUDRepository<CtacteTicket, AdminEntities>, ICtaCteTicketRepository
    {
        public CtaCteTicketRepository(AdminEntities connection) : base(connection)
        {

        }
    }
}
