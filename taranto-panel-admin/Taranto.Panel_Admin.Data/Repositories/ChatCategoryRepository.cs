﻿using Taranto.Panel_Admin.Data.Repositories.Interfaces;

namespace Taranto.Panel_Admin.Data.Repositories
{
    public class ChatCategoryRepository : BaseCRUDRepository<ChatCategory, AdminEntities>, IChatCategoryRepository
    {
        public ChatCategoryRepository(AdminEntities connection) : base(connection)
        {

        }

       
    }
}
