﻿using Taranto.Panel_Admin.Data.Repositories.Interfaces;

namespace Taranto.Panel_Admin.Data.Repositories
{
    public class BoletinesRepository : BaseCRUDRepository<BoletinLanzamiento, AdminEntities>, IBoletinesRepository
    {
        public BoletinesRepository(AdminEntities connection) : base(connection)
        {

        }
    }
}
