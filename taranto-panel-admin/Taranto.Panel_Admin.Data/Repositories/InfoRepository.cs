﻿using Taranto.Panel_Admin.Data.Repositories.Interfaces;

namespace Taranto.Panel_Admin.Data.Repositories
{
    public class InfoRepository : BaseCRUDRepository<Info, AdminEntities>, IInfoRepository
    {
        public InfoRepository(AdminEntities connection) : base(connection)
        {

        }

       
    }
}
