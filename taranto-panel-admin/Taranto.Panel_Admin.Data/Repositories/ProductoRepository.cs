﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Taranto.Panel_Admin.Data.DTO;
using Taranto.Panel_Admin.Data.Repositories.Interfaces;

namespace Taranto.Panel_Admin.Data.Repositories
{
    public class ProductoRepository : BaseCRUDRepository<Producto, AdminEntities>, IProductoRepository
    {
        public ProductoRepository(AdminEntities connection) : base(connection)
        {

        }

        public List<ProductoDTO> GetAllProductoDTO()
        {
            return (from p in db.Productos
                    join pc in db.ProductosCategorias on p.IdCategoria equals pc.Id
                    select new ProductoDTO
                    {
                        Id = p.Id,
                        IdCategoria = p.IdCategoria,
                        DescripcionEs = p.DescripcionEs,
                        DescripcionEn = p.DescripcionEn,
                        DescripcionPr = p.DescripcionPr,
                        TituloEs = p.TituloEs,
                        TituloEn = p.TituloEn,
                        TituloPr = p.TituloPr,
                        ProductoCategoria = new ProductoCategoriaDTO { Id = pc.Id, Detalle = pc.Detalle }
                    }).ToList();
        }

        public List<ProductosCategoria> GetAllCategorias()
        {
            return db.ProductosCategorias.ToList();
        }

        public ProductosCategoria GetCategoria(int id)
        {
            return db.ProductosCategorias.Find(id);
        }

        public void UpdateCategoria(ProductosCategoria entity)
        {
            db.ProductosCategorias.AddOrUpdate(entity);
            db.SaveChanges();
        }

        public void DeleteCategoria(int id)
        {   
            var entity = db.ProductosCategorias.Find(id);
            db.ProductosCategorias.Remove(entity);
        }

        public void AddCategoria(ProductosCategoria entity)
        { 
            UpdateCategoria(entity);
        }
    }
}