﻿using Taranto.Panel_Admin.Data.Repositories.Interfaces;

namespace Taranto.Panel_Admin.Data.Repositories
{
    public class ListaDePreciosRepository : BaseCRUDRepository<ListasDePrecio, AdminEntities>, IListaDePreciosRepository
    {
        public ListaDePreciosRepository(AdminEntities connection) : base(connection)
        {

        }

       
    }
}
