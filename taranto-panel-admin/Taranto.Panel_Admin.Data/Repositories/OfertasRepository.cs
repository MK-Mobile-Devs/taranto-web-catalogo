﻿using Taranto.Panel_Admin.Data.Repositories.Interfaces;

namespace Taranto.Panel_Admin.Data.Repositories
{
    public class OfertasRepository : BaseCRUDRepository<Oferta, AdminEntities>, IOfertasRepository
    {
        public OfertasRepository(AdminEntities _context) : base(_context)
        {
        }
    }
}
