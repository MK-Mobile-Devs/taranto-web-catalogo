﻿using Taranto.Panel_Admin.Data.Repositories.Interfaces;

namespace Taranto.Panel_Admin.Data.Repositories
{
    public class UserDataEditRepository : BaseCRUDRepository<UserDataEditRequest, AdminEntities>, IUserDataEditRepository
    {
        public UserDataEditRepository(AdminEntities connection) : base(connection)
        {

        }
    }
}
