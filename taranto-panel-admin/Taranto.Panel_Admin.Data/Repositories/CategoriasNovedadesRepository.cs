﻿using Taranto.Panel_Admin.Data.Repositories.Interfaces;

namespace Taranto.Panel_Admin.Data.Repositories
{
    public class CategoriasNovedadesRepository : BaseCRUDRepository<NovedadesCategoria, AdminEntities>, ICategoriasNovedadesRepository
    {
        public CategoriasNovedadesRepository(AdminEntities connection) : base(connection)
        {

        }

       
    }
}
