﻿namespace Taranto.Panel_Admin.Data.Repositories
{
    public class SliderHomeRepository : BaseCRUDRepository<CarrouselContenido, AdminEntities>, ISliderHomeRepository
    {

        public SliderHomeRepository(AdminEntities connection) : base(connection)
        {

        }

    }
}
