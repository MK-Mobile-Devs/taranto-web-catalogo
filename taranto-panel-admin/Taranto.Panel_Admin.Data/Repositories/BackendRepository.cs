﻿using Taranto.Panel_Admin.Data.Repositories.Interfaces;

namespace Taranto.Panel_Admin.Data.Repositories
{
    public class BackendRepository : BaseCRUDRepository<Backend, AdminEntities>, IBackendRepository
    {
        public BackendRepository(AdminEntities connection) : base(connection)
        {

        }
    }
}
