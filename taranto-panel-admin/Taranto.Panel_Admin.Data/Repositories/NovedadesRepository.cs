﻿using System.Collections.Generic;
using System.Linq;
using Taranto.Panel_Admin.Data.Repositories.Interfaces;

namespace Taranto.Panel_Admin.Data.Repositories
{
    public class NovedadesRepository : BaseCRUDRepository<Novedade, AdminEntities>, INovedadesRepository
    {
        private AdminEntities _context;

        public NovedadesRepository(AdminEntities connection) : base(connection)
        {
            _context = connection;
        }

        

        public List<NovedadesCategoria> GetAllCategorias()
        {
            return _context.NovedadesCategorias.ToList();
        }

    }
}
