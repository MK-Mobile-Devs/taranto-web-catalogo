﻿using System.Collections.Generic;
using System.Linq;
using Taranto.Panel_Admin.Data.Repositories.Interfaces;

namespace Taranto.Panel_Admin.Data.Repositories
{
    public class ChatRepository : BaseCRUDRepository<Chat, AdminEntities>, IChatRepository
    {
        public ChatRepository(AdminEntities connection) : base(connection)
        {

        }

        public void AddChatMessage(ChatMessage chat)
        {
            db.ChatMessages.Add(chat);
            db.SaveChanges();
        }


        public List<ChatMessage> GetChatById(int IdChat)
        {
            return db.ChatMessages.Where(a => a.IdChat == IdChat).ToList();
        }

        public void UpdateChatMessage(ChatMessage chat)
        {
            var edited = db.ChatMessages
                    .Where(x => x.Id == chat.Id)
                    .First();

            db.Entry(edited).CurrentValues.SetValues(chat);
            db.SaveChanges();
        }
    }
}
