﻿using Taranto.Panel_Admin.Data.Repositories.Interfaces;

namespace Taranto.Panel_Admin.Data.Repositories
{
    public class InstitucionalRepository : BaseCRUDRepository<PaginasInstitucionale, AdminEntities>, IInstitucionalRepository
    {
        public InstitucionalRepository(AdminEntities connection) : base(connection)
        {

        }

    }
}
