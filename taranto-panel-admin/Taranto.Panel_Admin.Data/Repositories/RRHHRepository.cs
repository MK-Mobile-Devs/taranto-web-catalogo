﻿using Taranto.Panel_Admin.Data.Repositories.Interfaces;

namespace Taranto.Panel_Admin.Data.Repositories
{
    public class RRHHRepository : BaseCRUDRepository<RecursosHumanosBusqueda, AdminEntities>, IRRHHRepository
    {
        public RRHHRepository(AdminEntities connection) : base(connection)
        {

        }

        //public void Add(RecursosHumanosBusqueda rrhhBusqueda)
        //{
        //    db.RecursosHumanosBusquedas.Add(rrhhBusqueda);
        //    db.SaveChanges();
        //}

        //public void Edit(RecursosHumanosBusqueda rrhhBusqueda)
        //{
        //    db.Entry(rrhhBusqueda).State = EntityState.Modified;
        //    db.SaveChanges();
        //}


        //public void Delete(int id)
        //{
        //    RecursosHumanosBusqueda rrhhBusqueda = db.RecursosHumanosBusquedas.Find(id);
        //    db.RecursosHumanosBusquedas.Remove(rrhhBusqueda);
        //    db.SaveChanges();
        //}


        //public RecursosHumanosBusqueda Find(int? id)
        //{
        //    return db.RecursosHumanosBusquedas.Find(id);
        //}

        //public List<RecursosHumanosBusqueda> GetAll()
        //{
        //    return db.RecursosHumanosBusquedas.ToList();
        //}
    }
}
