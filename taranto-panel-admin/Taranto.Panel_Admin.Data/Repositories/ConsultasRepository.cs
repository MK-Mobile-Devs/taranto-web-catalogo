﻿using Taranto.Panel_Admin.Data.Repositories.Interfaces;

namespace Taranto.Panel_Admin.Data.Repositories
{
    public class ConsultasRepository : BaseCRUDRepository<Contacto, AdminEntities>, IConsultasRepository
    {
        public ConsultasRepository(AdminEntities connection) : base(connection)
        {

        }
    }
}
