﻿namespace Taranto.Panel_Admin.Data.Repositories.Interfaces
{
    public interface IUserDataEditRepository : IBaseCRUDRepository<UserDataEditRequest>
    {
        
    }
}
