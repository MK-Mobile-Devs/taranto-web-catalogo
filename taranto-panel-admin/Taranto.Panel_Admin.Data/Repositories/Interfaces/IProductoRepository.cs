﻿using System.Collections.Generic;
using Taranto.Panel_Admin.Data.DTO;

namespace Taranto.Panel_Admin.Data.Repositories.Interfaces
{
    public interface IProductoRepository : IBaseCRUDRepository<Producto>
    {
        List<ProductoDTO> GetAllProductoDTO();
        List<ProductosCategoria> GetAllCategorias();

        ProductosCategoria GetCategoria(int id);
        void UpdateCategoria(ProductosCategoria entity);
        void DeleteCategoria(int id);
        void AddCategoria(ProductosCategoria entity);
    }
}
