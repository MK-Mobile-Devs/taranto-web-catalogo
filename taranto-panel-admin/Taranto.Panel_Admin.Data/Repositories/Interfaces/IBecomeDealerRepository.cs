﻿namespace Taranto.Panel_Admin.Data.Repositories.Interfaces
{
    public interface IBecomeDealerRepository : IBaseCRUDRepository<BecomeDealer>
    {
        
    }
}
