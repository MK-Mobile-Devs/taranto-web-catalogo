﻿namespace Taranto.Panel_Admin.Data.Repositories.Interfaces
{
    public interface IInfoRepository : IBaseCRUDRepository<Info>
    {
    }
}
