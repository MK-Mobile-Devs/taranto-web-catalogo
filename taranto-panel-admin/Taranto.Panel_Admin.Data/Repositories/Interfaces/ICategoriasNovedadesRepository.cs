﻿namespace Taranto.Panel_Admin.Data.Repositories.Interfaces
{
    public interface ICategoriasNovedadesRepository : IBaseCRUDRepository<NovedadesCategoria>
    {
      
    }
}
