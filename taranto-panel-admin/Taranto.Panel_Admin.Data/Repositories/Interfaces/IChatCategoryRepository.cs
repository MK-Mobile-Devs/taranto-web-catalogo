﻿namespace Taranto.Panel_Admin.Data.Repositories.Interfaces
{
    public interface IChatCategoryRepository : IBaseCRUDRepository<ChatCategory>
    {
      
    }
}
