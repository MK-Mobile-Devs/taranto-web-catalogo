﻿namespace Taranto.Panel_Admin.Data.Repositories.Interfaces
{
    public interface IBackendRepository : IBaseCRUDRepository<Backend>
    {
    }
}
