﻿namespace Taranto.Panel_Admin.Data.Repositories.Interfaces
{
    public interface IListaDePreciosRepository : IBaseCRUDRepository<ListasDePrecio>
    {
        
    }
}
