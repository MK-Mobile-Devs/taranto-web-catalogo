﻿using System.Collections.Generic;

namespace Taranto.Panel_Admin.Data.Repositories.Interfaces
{
    public interface INovedadesRepository : IBaseCRUDRepository<Novedade>
    {
        List<NovedadesCategoria> GetAllCategorias();
        
    }
}
