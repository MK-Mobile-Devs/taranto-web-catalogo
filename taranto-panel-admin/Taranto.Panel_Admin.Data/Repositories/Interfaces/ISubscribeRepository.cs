﻿namespace Taranto.Panel_Admin.Data.Repositories.Interfaces
{
    public interface ISubscribeRepository : IBaseCRUDRepository<Subscribe>
    {
    }
}
