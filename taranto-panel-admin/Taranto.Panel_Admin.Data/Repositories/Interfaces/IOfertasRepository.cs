﻿namespace Taranto.Panel_Admin.Data.Repositories.Interfaces
{
    public interface IOfertasRepository : IBaseCRUDRepository<Oferta>
    {
    }
}
