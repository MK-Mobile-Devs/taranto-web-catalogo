﻿namespace Taranto.Panel_Admin.Data.Repositories.Interfaces
{
    public interface IBankAccountRepository : IBaseCRUDRepository<BankAccount>
    {
        
    }
}
