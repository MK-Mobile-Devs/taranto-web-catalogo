﻿namespace Taranto.Panel_Admin.Data.Repositories.Interfaces
{
    public interface IBoletinesRepository : IBaseCRUDRepository<BoletinLanzamiento>
    {
    }
}
