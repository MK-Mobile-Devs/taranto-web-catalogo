﻿using System.Collections.Generic;

namespace Taranto.Panel_Admin.Data.Repositories.Interfaces
{
    public interface IChatRepository : IBaseCRUDRepository<Chat>
    {
        List<ChatMessage> GetChatById(int IdChat);
        void AddChatMessage(ChatMessage chat);

        void UpdateChatMessage(ChatMessage chat);
    }
}
