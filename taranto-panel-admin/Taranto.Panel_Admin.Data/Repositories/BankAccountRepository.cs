﻿using Taranto.Panel_Admin.Data.Repositories.Interfaces;

namespace Taranto.Panel_Admin.Data.Repositories
{
    public class BankAccountRepository : BaseCRUDRepository<BankAccount, AdminEntities>, IBankAccountRepository
    {
        public BankAccountRepository(AdminEntities connection) : base(connection)
        {

        }
    }
}
