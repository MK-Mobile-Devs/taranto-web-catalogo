﻿namespace Taranto.Panel_Admin.Data.Repositories
{
    public class RevistaRepository : BaseCRUDRepository<Magazine, AdminEntities>, IRevistaRepository
    {

        public RevistaRepository(AdminEntities connection) : base(connection)
        {

        }

    }
}
