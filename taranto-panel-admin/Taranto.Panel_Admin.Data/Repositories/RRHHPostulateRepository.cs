﻿using Taranto.Panel_Admin.Data.Repositories.Interfaces;

namespace Taranto.Panel_Admin.Data.Repositories
{
    public class RRHHPostulateRepository : BaseCRUDRepository<RrhhPostulate, AdminEntities>, IRRHHPostulateRepository
    {
        public RRHHPostulateRepository(AdminEntities connection) : base(connection)
        {

        }
    }
}
