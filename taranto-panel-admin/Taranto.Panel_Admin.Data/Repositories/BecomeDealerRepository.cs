﻿using Taranto.Panel_Admin.Data.Repositories.Interfaces;

namespace Taranto.Panel_Admin.Data.Repositories
{
    public class BecomeDealerRepository : BaseCRUDRepository<BecomeDealer, AdminEntities>, IBecomeDealerRepository
    {
        public BecomeDealerRepository(AdminEntities connection) : base(connection)
        {

        }
    }
}
