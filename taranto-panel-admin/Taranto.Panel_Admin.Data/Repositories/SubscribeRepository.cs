﻿using Taranto.Panel_Admin.Data.Repositories.Interfaces;

namespace Taranto.Panel_Admin.Data.Repositories
{
    public class SubscribeRepository : BaseCRUDRepository<Subscribe, AdminEntities>, ISubscribeRepository
    {
        public SubscribeRepository(AdminEntities connection) : base(connection)
        {

        }

       
    }
}
