//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Taranto.Panel_Admin.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class RrhhPostulate
    {
        public int Id { get; set; }
        public System.DateTime PostulateFecha { get; set; }
        public int IdRrhhSearch { get; set; }
        public string FechaRrhhSearch { get; set; }
        public string Name { get; set; }
        public string Dni { get; set; }
        public string Nationality { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Tel { get; set; }
        public string Birthdate { get; set; }
        public string Sex { get; set; }
        public string Message { get; set; }
        public string File { get; set; }
        public int SentEmail { get; set; }
    }
}
