﻿namespace Taranto.Panel_Admin.Data.DTO
{
    public partial class ProductoCategoriaDTO
    {
        public int Id { get; set; }
        public string Detalle { get; set; }
    }
}
