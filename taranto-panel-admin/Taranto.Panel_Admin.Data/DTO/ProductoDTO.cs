﻿namespace Taranto.Panel_Admin.Data.DTO
{
    public partial class ProductoDTO
    {
        public int Id { get; set; }
        public int IdCategoria { get; set; }
        public string TituloEs { get; set; }
        public string TituloEn { get; set; }
        public string TituloPr { get; set; }
        public string DescripcionEs { get; set; }
        public string DescripcionEn { get; set; }
        public string DescripcionPr { get; set; }
        public virtual ProductoCategoriaDTO ProductoCategoria { get; set; }
    }
}
