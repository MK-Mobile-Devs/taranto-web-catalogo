//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Taranto.Panel_Admin.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProductOrder
    {
        public int Id { get; set; }
        public System.DateTime ProductOrderDate { get; set; }
        public string IdUser { get; set; }
        public string Platform { get; set; }
        public string StatusCode { get; set; }
        public string StatusMessage { get; set; }
        public string NumOrder { get; set; }
        public string Message { get; set; }
        public string DataSentSerialized { get; set; }
        public string DataReceivedSerialized { get; set; }
        public int SentEmail { get; set; }
    }
}
