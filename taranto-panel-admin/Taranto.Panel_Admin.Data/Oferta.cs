//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Taranto.Panel_Admin.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Oferta
    {
        public int Id { get; set; }
        public System.DateTime FechaAlta { get; set; }
        public System.DateTime FechaCierre { get; set; }
        public string Titulo { get; set; }
        public string Clase { get; set; }
        public string Cuota { get; set; }
        public string Excluir { get; set; }
        public string Imagen { get; set; }
        public string Archivo { get; set; }
    }
}
