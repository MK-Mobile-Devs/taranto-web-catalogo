﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Taranto.Panel_Admin.Data
{
    public abstract class BaseCRUDRepository<TEntity, TContext> : IBaseCRUDRepository<TEntity>
        where TEntity : class
        where TContext : AdminEntities
    {
        public TContext db { get; set; }

        //public DateTime FechaActualBaseDeDatos => Contexto.MVC_SQL_FechaHoraActual().FirstOrDefault().Value;

        protected BaseCRUDRepository(TContext _context)
        {
            db = _context; 
        }
        public TEntity Add(TEntity entity)
        {
            db.Set<TEntity>().Add(entity);
            db.SaveChanges();
            return entity;
        }

        public TEntity Delete(int id)
        {
            var entity = db.Set<TEntity>().Find(id);
            if (entity == null)
            {
                return entity;
            }

            db.Set<TEntity>().Remove(entity);
            db.SaveChanges();

            return entity;
        }

        public TEntity Get(int id)
        {
            return db.Set<TEntity>().Find(id);
        }

        public TEntity GetAsNoTracking(int id)
        {
            var entity = db.Set<TEntity>().Find(id);
            db.Entry(entity).State = EntityState.Detached;
            return entity;
        }

        public List<TEntity> GetAll()
        {
            return db.Set<TEntity>().ToList();
        }

        public IEnumerable<TEntity> GetAllEnumerable()
        {
            return db.Set<TEntity>();
        }

        public TEntity Update(TEntity entity)
        {
            db.Entry(entity).State = EntityState.Modified;
            db.SaveChanges();
            return entity;
        }
        public TEntity UpdateExcludingProperty(TEntity entity, string imagePropertyName)
        {
            var entry = db.Entry(entity);
            entry.State = EntityState.Modified;
            db.SaveChanges();
            return entity;
        }
        
    }

    public class BaseEntity
    {
        public int Id { get; set; }
    }
}
