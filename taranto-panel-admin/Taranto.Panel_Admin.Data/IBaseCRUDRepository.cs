﻿using System.Collections.Generic;

namespace Taranto.Panel_Admin.Data
{
    public interface IBaseCRUDRepository<TEntity>
    {
        TEntity Add(TEntity entity);
        TEntity Delete(int id);
        TEntity Get(int id);
        List<TEntity> GetAll();
        IEnumerable<TEntity> GetAllEnumerable();
        TEntity GetAsNoTracking(int id);
        TEntity Update(TEntity entity);
        TEntity UpdateExcludingProperty(TEntity entity, string imagePropertyName);
    }
}
