﻿using System;

namespace Taranto.ViewModels
{
    public class ListaDePreciosVMItem: BaseVM
    {
        public DateTime FechaCreacion { get; set; }
        public GenericFileViewModel Archivo1 { get; set; }
        public string NombreArchivo1 { get; set; }
        public GenericFileViewModel Archivo2 { get; set; }
        public string NombreArchivo2 { get; set; }
        public GenericFileViewModel Archivo3 { get; set; }
        public string NombreArchivo3 { get; set; }
    }
}