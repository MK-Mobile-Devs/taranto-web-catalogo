﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taranto.ViewModels
{
    public class TarantoTranslatableString : TranslatableString
    {
        public TarantoTranslatableString(string destinationName, string ES, string EN, string BR)
        {
            this.FieldName = destinationName;
            this.Translations = new Dictionary<string, string>();
            this.Translations.Add(Language.ES.ToString(), ES);
            this.Translations.Add(Language.EN.ToString(), EN);
            this.Translations.Add(Language.BR.ToString(), BR);
        }
        public TarantoTranslatableString(string fieldName)
        {
            this.FieldName = fieldName;
            this.Translations = new Dictionary<string, string>();
            this.Translations.Add(Language.ES.ToString(), "");
            this.Translations.Add(Language.EN.ToString(), "");
            this.Translations.Add(Language.BR.ToString(), "");
        }

        public TarantoTranslatableString()
        {

        }
        public string FieldName { get; set; }

        public override string ToString()
        {
            return this.Translations[Language.ES.ToString()];
        }
    }
    public class TarantoTranslatableStringSummernote : TranslatableStringSummernote
    {
        public TarantoTranslatableStringSummernote(string destinationName, string ES, string EN, string BR)
        {
            this.FieldName = destinationName;
            this.Translations = new Dictionary<string, string>();
            this.Translations.Add(Language.ES.ToString(), ES);
            this.Translations.Add(Language.EN.ToString(), EN);
            this.Translations.Add(Language.BR.ToString(), BR);
        }
        public TarantoTranslatableStringSummernote(string fieldName)
        {
            this.FieldName = fieldName;
            this.Translations = new Dictionary<string, string>();
            this.Translations.Add(Language.ES.ToString(), "");
            this.Translations.Add(Language.EN.ToString(), "");
            this.Translations.Add(Language.BR.ToString(), "");
        }

        public TarantoTranslatableStringSummernote()
        {

        }
        public string FieldName { get; set; }

        public override string ToString()
        {
            return this.Translations[Language.ES.ToString()];
        }
    }
    public class TranslatableString
    {
        public TranslatableString()
        {

        }
        public Dictionary<string, string> Translations { get; set; }
    }
    public class TranslatableStringSummernote
    {
        public TranslatableStringSummernote()
        {

        }
        public Dictionary<string, string> Translations { get; set; }
    }
    public enum Language
    {
        ES,
        EN,
        BR
    }
}
