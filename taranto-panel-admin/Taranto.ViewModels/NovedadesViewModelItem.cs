﻿using System;
using Taranto.ViewModels.Helpers;

namespace Taranto.ViewModels
{
    public class NovedadesViewModelItem
    {
        public NovedadesViewModelItem(bool initializeAdd)
        {
            Titulo = new TarantoTranslatableString("Titulo");
            Subtitulo = new TarantoTranslatableString("Subtitulo");
            Descripcion = new TarantoTranslatableStringSummernote("Descripcion");
            Image = new ImageViewModel();
            Galeria = new MultipleImageViewModel();
        }
        public NovedadesViewModelItem()
        {

        }
        public int Id { get; set; }

        public string existingPath { get; set; }
        public DateTime Fecha { get; set; }
        public int IdCategoria { get; set; }
        public TarantoTranslatableString Titulo { get; set; }
        public TarantoTranslatableString Subtitulo { get; set; }
        public TarantoTranslatableStringSummernote Descripcion { get; set; }
        public ImageViewModel Image { get; set; }
        public MultipleImageViewModel Galeria { get; set; }
        public virtual ProductoCategoriaViewModelItem ProductoCategoria { get; set; }
    }
}