﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taranto.ViewModels
{
    public class ContactoViewModel
    {
        public int Id { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string Nombre { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public string Country { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public int SentEmail { get; set; }
    }
}
