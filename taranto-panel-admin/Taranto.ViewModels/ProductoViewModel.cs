﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taranto.ViewModels
{
    public class ProductoViewModel
    {
        public ProductoViewModel(bool initialTranslate)
        {
            Items = new List<ProductoViewModelItem>();
        }
        public ProductoViewModel()
        {

        }
        public IEnumerable<ProductoViewModelItem> Items { get; set; }

        public int SelectedCategory { get; set; }

        public IEnumerable<ProductoCategoriaViewModelItem> Categorias { get; set; }
    }
}
