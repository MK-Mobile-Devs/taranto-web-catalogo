﻿namespace Taranto.ViewModels
{
    public class OfertaClaseCliente
    {
        public string CustomerNumber { get; set; }
        public int CodeInt { get; set; }
        public string Description { get; set; }
        public string Desc_POR { get; set; }
        public string Desc_ENG { get; set; }
        public bool Selected { get; set; }
    }
}