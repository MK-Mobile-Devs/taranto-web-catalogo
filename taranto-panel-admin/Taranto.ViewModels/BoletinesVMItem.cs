﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taranto.ViewModels
{
    public class BoletinesVMItem
    {
        public BoletinesVMItem()
        {

        }
        public BoletinesVMItem(bool v)
        {
            Titulo = new TarantoTranslatableString("Titulo");
            Imagen = new ImageViewModel();
            Archivo = new FileViewModel();
        }

        public int Id { get; set; }
        public System.DateTime FechaCreacion { get; set; }
        public TarantoTranslatableString Titulo { get; set; }
        public ImageViewModel Imagen { get; set; }
        public FileViewModel Archivo { get; set; }
    }
}
