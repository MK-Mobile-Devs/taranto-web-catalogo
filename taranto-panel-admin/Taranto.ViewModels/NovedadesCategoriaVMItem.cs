﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taranto.ViewModels
{
    public class NovedadesCategoriaVMItem : BaseVM
    {
        
        public NovedadesCategoriaVMItem(bool add)
        {
            this.Categoria = new TarantoTranslatableString("Categoria");
        }
        public NovedadesCategoriaVMItem()
        {

        }

        public TarantoTranslatableString Categoria { get; set; }
    }
}
