﻿using System;
using System.Collections.Generic;

namespace Taranto.ViewModels
{
    public class ChatVMItem: BaseVM
    {
        public ChatVMItem()
        {
            ChatMsg = new List<ChatMsgVM>();
        }
        public int ChatType { get; set; }
        public DateTime LastMessageDate { get; set; }
        public string IdUser { get; set; }
        public string Cod { get; set; }
        public string Category { get; set; }
        public int NotificationUser { get; set; }
        public int NotificationAdmin { get; set; }

        public List<ChatMsgVM> ChatMsg { get; set; }

        public string Messege { get; set; }
    }
}