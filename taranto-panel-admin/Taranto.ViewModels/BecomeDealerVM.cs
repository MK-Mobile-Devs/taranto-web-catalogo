﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taranto.ViewModels
{
    public class BecomeDealerVM
    {
        public IList<BecomeDealerVMItem> Items { get; set; }
    }
}
