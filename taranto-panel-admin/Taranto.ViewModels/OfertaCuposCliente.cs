﻿namespace Taranto.ViewModels
{
    public class OfertaCuposCliente
    {
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public bool Selected { get; set; }
    }
}