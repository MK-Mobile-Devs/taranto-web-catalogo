﻿using Taranto.ViewModels.Helpers;

namespace Taranto.ViewModels
{
    public class ProductoViewModelItem
    {
        public ProductoViewModelItem(bool initializeAdd)
        {
            Titulo = new TarantoTranslatableString("Titulo");
            Descripcion = new TarantoTranslatableStringSummernote("Descripcion");
        }
        public ProductoViewModelItem()
        {

        }
        public int Id { get; set; }
        public int IdCategoria { get; set; }
        public TarantoTranslatableString Titulo { get; set; }
        public TarantoTranslatableStringSummernote Descripcion { get; set; }
        public virtual ProductoCategoriaViewModelItem ProductoCategoria { get; set; }
    }
}