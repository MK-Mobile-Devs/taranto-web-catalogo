﻿using System;

namespace Taranto.ViewModels
{
    public class CtaCteTicketVMItem : BaseVM
    {
        public DateTime CtacteTicketDate { get; set; }
        public string IdUser { get; set; }
        public string Ref { get; set; }
        public string File { get; set; }
        public string Message { get; set; }
        public int SentEmail { get; set; }
        public int Received { get; set; }
    }
}