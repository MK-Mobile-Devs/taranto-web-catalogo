﻿using System;

namespace Taranto.ViewModels
{
    public class RRHHPostulateVMItem : BaseVM
    {
        public RRHHPostulateVMItem()
        {

        }
        public DateTime PostulateFecha { get; set; }
        public int IdRrhhSearch { get; set; }
        public string FechaRrhhSearch { get; set; }
        public string Name { get; set; }
        public string Dni { get; set; }
        public string Nationality { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Tel { get; set; }
        public string Birthdate { get; set; }
        public string Sex { get; set; }
        public string Message { get; set; }
        public string File { get; set; }
        public int SentEmail { get; set; }

    }
}