﻿using System;

namespace Taranto.ViewModels
{
    public class ConsultaVMItem: BaseVM
    {
        public ConsultaVMItem()
        {

        }

        public DateTime FechaCreacion { get; set; }
        public string Nombre { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public string Country { get; set; }
        public string Subject { get; set; }
        public string Messagge { get; set; }
        public int SentEmail { get; set; }
    }
}