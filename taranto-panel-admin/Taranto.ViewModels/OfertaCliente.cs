﻿namespace Taranto.ViewModels
{
    public class OfertaCliente
    {
        public int ClienteId { get; set; }
        public string CodigoCliente { get; set; }
        public double Latitud { get; set; }
        public double Longitud { get; set; }
        public int Tipo { get; set; }
        public string Icon { get; set; }
        public bool Selected { get; set; }
    }
}