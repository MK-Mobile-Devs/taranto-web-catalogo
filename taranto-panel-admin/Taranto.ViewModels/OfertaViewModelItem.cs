﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taranto.ViewModels
{
    public class OfertaViewModelItem
    {
        public string existingPath { get; set; }

        public OfertaViewModelItem()
        {

        }
        public OfertaViewModelItem(bool initialize)
        {
            Image = new ImageViewModel();
            ArchivoAdjunto = new FileViewModel();

        }
        public int Id { get; set; }
        public System.DateTime FechaAlta { get; set; }
        public System.DateTime FechaCierre { get; set; }
        public string Titulo { get; set; }
        public List<string> Clase { get; set; }
        public List<string> Cuota { get; set; }
        public List<string> Excluir { get; set; }
        public ImageViewModel Image { get; set; }
        public FileViewModel ArchivoAdjunto { get; set; }
    }
}
