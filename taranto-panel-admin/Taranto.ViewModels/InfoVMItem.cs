﻿namespace Taranto.ViewModels
{
    public class InfoVMItem : BaseVM
    {
        public InfoVMItem()
        {

        }

        public InfoVMItem(bool initializeAdd)
        {
            Title = new TarantoTranslatableString("Title");
            Description = new TarantoTranslatableStringSummernote("Description");
        }
        public TarantoTranslatableString Title { get; set; }
        public TarantoTranslatableStringSummernote Description { get; set; }

        public string UrlTitle { get; set; }
    }
}