﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taranto.ViewModels
{
    public class OfertaViewModel
    {
        public IEnumerable<OfertaViewModelItem> Items { get; set; }

        public IEnumerable<OfertaCliente> Clientes { get; set; }
        public IEnumerable<OfertaClaseCliente> ClasesCliente { get; set; }
        public IEnumerable<OfertaCuposCliente> CuposCliente { get; set; }

        public OfertaViewModel()
        {

        }
        public OfertaViewModel(bool initialize)
        {
            Items = new List<OfertaViewModelItem>() { new OfertaViewModelItem(true) };
        }
    }
}
