﻿namespace Taranto.ViewModels
{
    public class ProductoCategoriaViewModelItem
    {
        public int Id { get; set; }
        public string Detalle { get; set; }
    }
}