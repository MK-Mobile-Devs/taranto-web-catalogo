﻿namespace Taranto.ViewModels
{
    public class RRHHBusquedaVMItem : BaseVM
    {
        public RRHHBusquedaVMItem()
        {

        }
        public RRHHBusquedaVMItem(bool initializeAdd)
        {
            Funcion = new TarantoTranslatableString("Funcion");
            Equipo = new TarantoTranslatableString("Equipo");
            Ciudad = new TarantoTranslatableString("Ciudad");
            Descripcion = new TarantoTranslatableStringSummernote("Descripcion");
        }

        public TarantoTranslatableString Funcion { get; set; }
        public TarantoTranslatableString Equipo { get; set; }
        public TarantoTranslatableString Ciudad { get; set; }
        public TarantoTranslatableStringSummernote Descripcion { get; set; }

    }
}