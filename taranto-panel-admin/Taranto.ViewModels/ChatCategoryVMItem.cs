﻿using Taranto.ViewModels.Helpers;

namespace Taranto.ViewModels
{
    public class ChatCategoryVMItem : BaseVM
    {
        public ChatCategoryVMItem(bool add, ChatTypeEnum type)
        {
            this.Categoria = new TarantoTranslatableString("Categoria");
            this.ChatType = (int)type;
        }
        public ChatCategoryVMItem()
        {

        }

        public int ChatType { get; set; }
        public TarantoTranslatableString Categoria { get; set; }
    }

    //public enum ChatTypeEnum
    //{
    //    Sugerencias = 1,
    //    Reclamos = 2
    //}
}