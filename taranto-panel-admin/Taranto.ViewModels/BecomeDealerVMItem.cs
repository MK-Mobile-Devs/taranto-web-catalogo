﻿using System;

namespace Taranto.ViewModels
{
    public class BecomeDealerVMItem: BaseVM
    {
        public DateTime Request_date { get; set; }
        public string BusinessName { get; set; }
        public string Cuit { get; set; }
        public string PostalCode { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string Type { get; set; }
        public string Brands { get; set; }
        public string Email { get; set; }
        public string Tel { get; set; }
        public string Name { get; set; }
        public string Message { get; set; }
        public int SentEmail { get; set; }
    }

}