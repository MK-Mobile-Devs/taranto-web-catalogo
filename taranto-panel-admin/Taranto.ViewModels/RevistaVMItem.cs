﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Taranto.ViewModels
{
    public class RevistaVMItem: BaseVM
    {
        public RevistaVMItem(bool initializeAdd)
        {
            Image = new ImageViewModel();
            File = new FileViewModel();
            Titulo = new TarantoTranslatableString("Titulo");
            SubTitulo = new TarantoTranslatableString("Subtitulo");
        }
        public RevistaVMItem()
        {

        }

        public ImageViewModel Image { get; set; }
        public FileViewModel File { get; set; }
        public TarantoTranslatableString Titulo { get; set; }
        public TarantoTranslatableString SubTitulo { get; set; }
        public string Number { get; set; }
        public DateTime Date { get; set; }
        public string existingPath { get; set; }
    }







}
