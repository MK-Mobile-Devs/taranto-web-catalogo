﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taranto.ViewModels
{
    public class ChatCategoryVM
    {
        public IList<ChatCategoryVMItem> Items { get; set; }
    }
}
