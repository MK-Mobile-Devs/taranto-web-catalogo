﻿using System.Collections.Generic;

namespace Taranto.ViewModels
{
    public class CategoriasViewModel
    {
        public IEnumerable<CategoriaViewModelItem> Items { get; set; }
        
    }
}
