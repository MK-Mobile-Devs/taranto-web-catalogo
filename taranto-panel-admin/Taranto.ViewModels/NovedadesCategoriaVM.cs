﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taranto.ViewModels
{
    public class NovedadesCategoriaVM
    {
        public IList<NovedadesCategoriaVMItem> Items { get; set; }
    }
}
