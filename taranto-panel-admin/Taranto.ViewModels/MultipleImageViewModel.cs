﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Taranto.ViewModels
{
    public class MultipleImageViewModel
    {
        private string ImagesServerPath = "/images/";
        public override string ToString()
        {
            if (imagePath != null)
            {
                return Path.Combine(ImagesServerPath, imagePath);
            }
            return "";
        }
        public byte[][] fileByteArray;

        private HttpPostedFileBase[] file;
        public string imagePath;

        public string existingPath { get; set; }
        public MultipleImageViewModel()
        {

        }
        public MultipleImageViewModel(string imagePath)
        {
            this.imagePath = imagePath;
        }

        //No usar, existe solo para servir a MVC
        public HttpPostedFileBase[] File
        {
            get { return file; }
            set
            {
                file = value;
                int count = 0;
                fileByteArray = new byte[value.Length][];
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    if (file != null)
                    {
                        foreach (var file in file)
                        {
                            file.InputStream.CopyTo(memoryStream);
                            fileByteArray[count] = memoryStream.ToArray();
                            count++;
                        }
                    }

                }
            }
        }

    }
}
