﻿using System;

namespace Taranto.ViewModels
{
    public class ChatMsgVM: BaseVM
    {
        public DateTime MessageDate { get; set; }
        public int IdChat { get; set; }
        public string IdUserOrAdmin { get; set; }
        public string Message { get; set; }
        public int MessageSent { get; set; }
        public int MessageRead { get; set; }
    }
}