﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Taranto.ViewModels;

namespace Taranto.ViewModels.Helpers
{
    public static class EnumExtension
    {
        public static string GetName(this Language input)
        {
            switch (input)
            {
                case Language.BR:
                    return "Português";
                case Language.ES:
                default:
                    return "Español";
                case Language.EN:
                    return "English";
                
            }
        }
    }
}