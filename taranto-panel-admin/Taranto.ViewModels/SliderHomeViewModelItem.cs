﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Taranto.ViewModels
{
    public class SliderHomeViewModelItem 
    {
        public SliderHomeViewModelItem(bool initializeAdd)
        {
            Image = new ImageViewModel();
            Titulo = new TarantoTranslatableString("Titulo");
            SubTitulo = new TarantoTranslatableString("Subtitulo");
        }
        public SliderHomeViewModelItem()
        {

        }

        public ImageViewModel Image { get; set; }
        public TarantoTranslatableString Titulo { get; set; }
        public TarantoTranslatableString SubTitulo { get; set; }
        public int Id { get; set; }
        public int Rank { get; set; }
        public string existingPath { get; set; }
    }

   

    

    
   
}
