﻿using System;

namespace Taranto.ViewModels
{
    public class UserDataEditRequestVMItem: BaseVM
    {
        public DateTime Fecha { get; set; }
        public string IdUser { get; set; }
        public string InfoSerialized { get; set; }
        public string Img { get; set; }
        public string MoreImg { get; set; }
        public int SentMail { get; set; }
        public int Received { get; set; }
    }
}