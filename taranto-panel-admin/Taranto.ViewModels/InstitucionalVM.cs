﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taranto.ViewModels
{
    public class InstitucionalVM
    {
        public IList<InstitucionalVMItem> Items { get; set; }
    }
}
