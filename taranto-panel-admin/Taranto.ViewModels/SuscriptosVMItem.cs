﻿using System;

namespace Taranto.ViewModels
{
    public class SuscriptosVMItem: BaseVM
    {
        public DateTime SubscribeDate { get; set; }
        public string Email { get; set; }
        public int SentMail { get; set; }
    }
}
