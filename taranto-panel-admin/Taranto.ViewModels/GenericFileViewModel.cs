﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Taranto.ViewModels
{
    public class GenericFileViewModel
    {
        private string ImagesServerPath = "/images/";
        public override string ToString()
        {
            if (imagePath != null)
            {
                return Path.Combine(ImagesServerPath, imagePath);
            }
            return "";
        }
        public byte[] fileByteArray;
        public string FileName { get; set; }
        private HttpPostedFileBase file;
        public string imagePath;
        private DateTime fecha;

        public string existingPath { get; set; }
        public GenericFileViewModel()
        {

        }
        public GenericFileViewModel(string imagePath)
        {
            this.imagePath = imagePath;
        }

        //No usar, existe solo para servir a MVC
        public HttpPostedFileBase File
        {
            get { return file; }
            set
            {
                file = value;
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    if (file != null)
                    {
                        file.InputStream.CopyTo(memoryStream);
                        fileByteArray = memoryStream.ToArray();
                    }

                }
            }
        }

    }
}
