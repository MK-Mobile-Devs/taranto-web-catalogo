﻿using System;
using Taranto.ViewModels.Helpers;

namespace Taranto.ViewModels
{
    public class CategoriaViewModelItem
    {
        public CategoriaViewModelItem()
        {
            Titulo = new TarantoTranslatableString("Titulo");
        }    
        public TarantoTranslatableString Titulo { get; set; }

        public int Id { get; set; }
    }
}