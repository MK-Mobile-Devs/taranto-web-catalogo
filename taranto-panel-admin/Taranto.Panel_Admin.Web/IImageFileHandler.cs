﻿using System.Web;

namespace Taranto.Panel_Admin.Web
{
    public interface IImageFileHandler
    {
        byte[] GetImage(string fileName, HttpContextBase context, string subfolder);
        string SaveImage(byte[] file, string fileName, HttpContextBase context, string subfolder);
    }
}
