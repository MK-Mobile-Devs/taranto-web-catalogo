﻿using System.IO;
using System.Web;

namespace Taranto.Panel_Admin.Web
{
    public class ImageFileHandler : IImageFileHandler
    {
        const string path = "~/images";
        public string SaveImage(byte[] file, string fileName, HttpContextBase context, string subfolder)
        {
            string returnPath = Path.Combine($"images/{subfolder}", fileName);
            File.WriteAllBytes(Path.Combine(context.Server.MapPath($"{path}/{subfolder}"), fileName), file);
            return returnPath;
        }

        public byte[] GetImage(string fileName, HttpContextBase context, string subfolder)
        {
            return File.ReadAllBytes(Path.Combine(context.Server.MapPath($"{path}/{subfolder}"), fileName));
        }
    }
}
