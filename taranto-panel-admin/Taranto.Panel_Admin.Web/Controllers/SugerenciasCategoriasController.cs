﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Taranto.Panel_Admin.Data;
using Taranto.Panel_Admin.Data.Repositories.Interfaces;
using Taranto.ViewModels;
using Taranto.ViewModels.Helpers;

namespace Taranto.Panel_Admin.Web.Controllers
{
    [Authorize]
    public class SugerenciasCategoriasController : Controller
    {
        private readonly IChatCategoryRepository _repository;
        private readonly IMapper mapper;

        public SugerenciasCategoriasController(IChatCategoryRepository repository, IMapper mapper)
        {
            this._repository = repository;
            this.mapper = mapper;
        }

        public ActionResult Index()
        {
            var vm = new ChatCategoryVM()
            {
                Items = mapper.Map<IList<ChatCategoryVMItem>>(_repository.GetAll().Where(a=>a.ChatType == (int)ChatTypeEnum.Sugerencias).ToList())
            };
            return View(vm);
        }

        public ActionResult Add()  
        {
            return View(new ChatCategoryVMItem(true, ChatTypeEnum.Sugerencias));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(ChatCategoryVMItem item)
        {
            if (ModelState.IsValid)
            {
                _repository.Add(mapper.Map<ChatCategory>(item));

                return RedirectToAction("Index");
            }

            return View(item);
        }

        public ActionResult Edit(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ChatCategory item = _repository.Get(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(mapper.Map<ChatCategoryVMItem>(item));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ChatCategoryVMItem item)
        {
            if (ModelState.IsValid)
            {
                _repository.Update(mapper.Map<ChatCategory>(item));
                return RedirectToAction("Index");
            }
            return View(item);
        }

        public ActionResult Delete(int id)
        {
            _repository.Delete(id);
            return RedirectToAction("Index");
        }
    }
}
