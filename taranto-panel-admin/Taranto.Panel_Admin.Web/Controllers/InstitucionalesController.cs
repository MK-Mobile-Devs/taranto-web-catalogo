﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Taranto.Panel_Admin.Data;
using Taranto.Panel_Admin.Data.Repositories.Interfaces;
using Taranto.ViewModels;

namespace Taranto.Panel_Admin.Web.Controllers
{
    [Authorize]
    public class InstitucionalesController : Controller
    {
        private readonly IInstitucionalRepository institucionalRepository;
        private readonly IMapper mapper;
        private readonly List<string> tituloInstitucional;

        public InstitucionalesController(IInstitucionalRepository institucionalRepository, IMapper mapper)
        {
            this.institucionalRepository = institucionalRepository;
            this.mapper = mapper;
            this.tituloInstitucional = new List<string>(){ "EMPRESA", "CALIDAD, SEGURIDAD Y MEDIO AMBIENTE", "RRHH" };
        }


        // GET: Institucionales
        public ActionResult Index()
        {
            return View();
        }

        // GET: Institucionales/Details/5
        public ActionResult Details(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InstitucionalVMItem institucional = mapper.Map<InstitucionalVMItem>(institucionalRepository.Get(id));
            if (institucional == null)
            {
                return HttpNotFound();
            }
            return View(institucional);
        }

        // GET: Institucionales/Edit/1
        public ActionResult Edit(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            PaginasInstitucionale institucional = institucionalRepository.Get(id);

            ViewBag.TituloInstitucional = this.tituloInstitucional[id-1];
            if (institucional == null)
            {
                return HttpNotFound();
            }
            return View(mapper.Map<InstitucionalVMItem>(institucional));
        }

        // POST: Institucionales/Edit/1
        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(InstitucionalVMItem institucional)
        {
            if (ModelState.IsValid)
            {
                institucionalRepository.Update(mapper.Map<PaginasInstitucionale>(institucional));
                return RedirectToAction("Index");
            }
            return View(institucional);
        }
    }
}
