﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Taranto.Panel_Admin.Data;
using Taranto.Panel_Admin.Data.Repositories.Interfaces;
using Taranto.Panel_Admin.Web.Helpers;
using Taranto.ViewModels;

namespace Taranto.Panel_Admin.Web.Controllers
{
    [Authorize]
    public class ConsultasController : Controller
    {
        private readonly IConsultasRepository _repository;
        private readonly IMapper mapper;
        private readonly List<string> tituloInstitucional;

        public ConsultasController(IConsultasRepository repository, IMapper mapper)
        {
            this._repository = repository;
            this.mapper = mapper;
        }


        // GET: Info
        public ActionResult Index()
        {
            var consultas = _repository.GetAll();
            List<ConsultaVMItem> consultasItems = new List<ConsultaVMItem>();
            consultas.ForEach(i => consultasItems.Add(mapper.Map<ConsultaVMItem>(i)));

            return View(new ConsultaVM()
            {
                Items = consultasItems
            });
        }

        // GET: Info/Details/5
        public ActionResult Details(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ConsultaVMItem consulta = mapper.Map<ConsultaVMItem>(_repository.Get(id));
            if (consulta == null)
            {
                return HttpNotFound();
            }
            return View(consulta);
        }

        // POST: Info/Delete/5
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            _repository.Delete(id);
            return RedirectToAction("Index");
        }

        [HttpGet]
        [Authorize]
        public ActionResult downloadExcel()
        {
            CsvExport myExport = new CsvExport();

            var consultas = _repository.GetAll();
            List<ConsultaVMItem> consultasItems = new List<ConsultaVMItem>();
            consultas.ForEach(i => consultasItems.Add(mapper.Map<ConsultaVMItem>(i)));

            foreach (var item in consultasItems)
            {
                myExport.AddRow();
                myExport["Fecha/Hora"] = item.FechaCreacion;
                myExport["Motivo de Consulta"] = item.Subject;
                myExport["Sede"] = item.Country;
                myExport["Nombre y Apellido"] = item.Nombre;
                myExport["Email"] = item.Email;
                myExport["Telefono"] = item.Telefono;
                myExport["Consulta"] = item.Messagge;
                
            }
            return File(myExport.ExportToBytes(), "text/csv", "Taranto_Consultas.csv");

        }
    }
}
