﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using Taranto.Panel_Admin.Data;
using Taranto.Panel_Admin.Data.Repositories.Interfaces;
using Taranto.ViewModels;

namespace Taranto.Panel_Admin.Web.Controllers
{
    [Authorize]
    public class BoletinesController : Controller
    {
        private readonly IBoletinesRepository boletinesRepository;
        private readonly IMapper mapper;
        private readonly IImageFileHandler imageSaver;
        private const string Subfolder = "boletines";

        public BoletinesController(IBoletinesRepository boletinesRepository, IMapper mapper, IImageFileHandler imageSaver)
        {
            this.boletinesRepository = boletinesRepository;
            this.mapper = mapper;
            this.imageSaver = imageSaver;
        }
        // GET: Boletines
        public ActionResult Index()
        {
            var boletines = boletinesRepository.GetAll();
            List<BoletinesVMItem> boletinesItems = new List<BoletinesVMItem>();
            boletines.ForEach(i => boletinesItems.Add(mapper.Map<BoletinesVMItem>(i)));

            return View(new BoletinesVM()
            {
                Items = boletinesItems
            });
        }

        // GET: Boletines/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        public ActionResult Add()
        {
            return View(new BoletinesVMItem(true));
        }

        [HttpPost]
        public ActionResult Add(BoletinesVMItem item)
        {
            if (item.Archivo.File != null)
            {
                item.Archivo.FileName = GenerateFileName(item.Archivo.File.FileName);
                imageSaver.SaveImage(item.Archivo.fileByteArray, item.Archivo.FileName, HttpContext, Subfolder);
            }
            if (item.Imagen.File != null)
            {
                item.Imagen.FileName = GenerateFileName(item.Imagen.File.FileName);
                imageSaver.SaveImage(item.Imagen.fileByteArray, item.Imagen.FileName, HttpContext, Subfolder);
            }

            BoletinLanzamiento entity = mapper.Map<BoletinLanzamiento>(item);
            boletinesRepository.Add(entity);
            return RedirectToAction("Index");
        }

        private string GenerateFileName(string FileName)
        {
            Random random = new Random();

            string newName = random.Next().ToString();
            var extension = System.IO.Path.GetExtension(FileName);

            return newName + extension;
        }
        // GET: Boletines/Edit/5
        public ActionResult Edit(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            BoletinLanzamiento boletin = boletinesRepository.Get(id);

            return View(mapper.Map<BoletinesVMItem>(boletin));
        }

        // POST: Boletines/Edit/5
        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(BoletinesVMItem boletin)
        {
            BoletinLanzamiento entity = mapper.Map<BoletinLanzamiento>(boletin);
            BoletinLanzamiento entityFromDB = boletinesRepository.GetAsNoTracking(boletin.Id);

            if (boletin.Imagen.File != null)
            {
                imageSaver.SaveImage(boletin.Imagen.fileByteArray, boletin.Imagen.File.FileName, HttpContext, Subfolder);

            }
            else
            {
                entity.Imagen = entityFromDB.Imagen;
            }

            if (boletin.Archivo.File != null)
            {
                imageSaver.SaveImage(boletin.Archivo.fileByteArray, boletin.Archivo.File.FileName, HttpContext, Subfolder);

            }
            else
            {
                entity.Archivo = entityFromDB.Archivo;
            }

            boletinesRepository.Update(entity);
            return RedirectToAction("Index");
        }

        // GET: Boletines/Delete/5
        public ActionResult Delete(int id)
        {
            boletinesRepository.Delete(id);
            return RedirectToAction("Index");
        }

    }
}
