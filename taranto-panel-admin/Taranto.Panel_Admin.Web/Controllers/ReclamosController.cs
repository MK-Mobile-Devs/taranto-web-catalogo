﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Taranto.Panel_Admin.Data;
using Taranto.Panel_Admin.Data.Repositories.Interfaces;
using Taranto.ViewModels;
using Taranto.ViewModels.Helpers;

namespace Taranto.Panel_Admin.Web.Controllers
{
    [Authorize]
    public class ReclamosController : Controller
    {
        private readonly IChatRepository _repository;
        private readonly IMapper mapper;

        public ReclamosController(IChatRepository repository, IMapper mapper)
        {
            this._repository = repository;
            this.mapper = mapper;
        }

        public ActionResult Index()
        {
            var rrhh = _repository.GetAll().Where(a=> a.ChatType == (int)ChatTypeEnum.Reclamos).ToList();
            List<ChatVMItem> items = new List<ChatVMItem>();
            rrhh.ForEach(i => items.Add(mapper.Map<ChatVMItem>(i)));

            return View(new ChatVM()
            {
                Items = items
            });
        }

        public ActionResult Details(int id)
        {
            var item = _repository.Get(id);
            item.NotificationAdmin = 0;
            _repository.Update(item);
            var msg = _repository.GetChatById(item.Id);
            ChatVMItem chat = mapper.Map<ChatVMItem>(item);
            foreach (var mensaje in msg)
            {
                mensaje.MessageRead = 1;
                _repository.UpdateChatMessage(mensaje);
                chat.ChatMsg.Add(mapper.Map<ChatMsgVM>(mensaje));
            }

            chat.ChatMsg.OrderBy(a => a.MessageDate);
            return View(chat);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Details(ChatVMItem item)
        {
            item.NotificationAdmin = item.NotificationAdmin + 1;
            var mapChat = mapper.Map<Chat>(item);
            ChatMsgVM newMessage = new ChatMsgVM();
            newMessage.IdChat = item.Id;
            newMessage.MessageDate = DateTime.Now;
            newMessage.IdUserOrAdmin = "admin";
            newMessage.MessageSent = 1;
            newMessage.Message = item.Messege;

            var newDbMap = mapper.Map<ChatMessage>(newMessage);
            _repository.AddChatMessage(newDbMap);
            _repository.Update(mapChat);

            return RedirectToAction("Index");
        }
    }
}
