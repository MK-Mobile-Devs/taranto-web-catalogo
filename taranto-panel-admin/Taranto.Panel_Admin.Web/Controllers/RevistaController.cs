﻿using AutoMapper;
using System.Collections.Generic;
using System.Web.Mvc;
using Taranto.Panel_Admin.Data;
using Taranto.Panel_Admin.Data.Repositories;
using Taranto.ViewModels;


namespace Taranto.Panel_Admin.Web.Controllers
{
    [Authorize]
    public class RevistaController : Controller
    {
        private IImageFileHandler imageSaver;
        private readonly IRevistaRepository revistaRepository;
        private readonly IMapper mapper;
        private const string Subfolder = "revista";

        public RevistaController(IRevistaRepository revistaRepository, IMapper mapper, IImageFileHandler imageSaver)
        {
            this.imageSaver = imageSaver;
            this.revistaRepository = revistaRepository;
            this.mapper = mapper;
        }
       
        [Route("/[controller]/[action]")]
        public ActionResult Index()
        {
            if (TempData["message"] != null)
            {
                ViewBag.Message = TempData["message"];
                TempData["message"] = "";
            }
            var a = revistaRepository.GetAll();
            
            return View(new RevistaVM()
            {
                Items = mapper.Map<List<RevistaVMItem>>(a)
            });
        }

        public string Get(int id)
        {
            return "value";
        }
        [HttpGet]
        public ActionResult Add()
        {
            return View(new RevistaVMItem(true));
        }

        [HttpPost]
        public ActionResult Add(RevistaVMItem item)
        {
            if (item.File.File != null)
            {
                imageSaver.SaveImage(item.File.fileByteArray, item.File.File.FileName, HttpContext, Subfolder);
            }
            if (item.Image.File != null)
            {
                imageSaver.SaveImage(item.Image.fileByteArray, item.Image.File.FileName, HttpContext, Subfolder);
            }

            var result = revistaRepository.Add(mapper.Map<Magazine>(item));
            TempData["message"] = "Grabado Correctamente";
            return RedirectToAction("Index");
        }
        public ActionResult Edit(int id)
        {
            var item = revistaRepository.Get(id);

            return View(mapper.Map<RevistaVMItem>(item));
        }

       

        [HttpPost]
        public ActionResult Edit(RevistaVMItem edited)
        {
            Magazine entity = mapper.Map<Magazine>(edited);
            Magazine entityFromDB = revistaRepository.GetAsNoTracking(edited.Id);

            if (edited.Image.File != null)
            {
                imageSaver.SaveImage(edited.Image.fileByteArray, edited.Image.File.FileName, HttpContext, Subfolder);

            }
            else
            {
                entity.Img = entityFromDB.Img;
            }

            if (edited.File.File != null)
            {
                imageSaver.SaveImage(edited.File.fileByteArray, edited.File.File.FileName, HttpContext, Subfolder);

            }
            else
            {
                entity.File = entityFromDB.File;
            }

            revistaRepository.Update(entity);
            TempData["message"] = "Actualizado correctamente";
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            revistaRepository.Delete(id);
            TempData["message"] = "Borrado correctamente"; 
            return RedirectToAction("Index");
        }
    }
}
