﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Taranto.Panel_Admin.Data;
using Taranto.Panel_Admin.Data.Repositories;
using Taranto.Panel_Admin.Data.Repositories.Interfaces;
using Taranto.ViewModels;

namespace Taranto.Panel_Admin.Web.Controllers
{
    [Authorize]
    public class ProductosController : Controller
    {
        private readonly IProductoRepository _productoRepository;
        private readonly IMapper _mapper;
        private readonly IImageFileHandler _imageFileHandler;
        private const string Subfolder = "productos";

        public ProductosController(IProductoRepository productoRepository, IMapper mapper, IImageFileHandler imageFileHandler)
        {
            _productoRepository = productoRepository;
            _mapper = mapper;
            _imageFileHandler = imageFileHandler;
        }

        // GET: Productos
        public ActionResult Index()
        {
            var productos = _productoRepository.GetAllProductoDTO();
            var productoItems = new List<ProductoViewModelItem>();
            productos.ForEach(p=> productoItems.Add(_mapper.Map<ProductoViewModelItem>(p))); 

            return View(new ProductoViewModel
            {
                Items = productoItems,
                Categorias = GetAllProductosCategorias()
            });
        }

        public List<ProductoCategoriaViewModelItem> GetAllProductosCategorias()
        {
            var a = _productoRepository.GetAllCategorias();
            var res = new List<ProductoCategoriaViewModelItem>();
            a.ForEach(x => res.Add(new ProductoCategoriaViewModelItem { Id = x.Id, Detalle = x.Detalle }));

            return res;
        }

        // GET: Productos/Create
        public ActionResult Add()
        {
            List<SelectListItem> lst = new List<SelectListItem>();
            foreach (var item in GetAllProductosCategorias())
            {
                lst.Add(new SelectListItem { Text = item.Detalle, Value = item.Id.ToString() });
            }

            ViewBag.CategoriasList = lst;

            return View(new ProductoViewModelItem(true));
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult Add(ProductoViewModelItem producto)
        {
            var entity = _mapper.Map<Producto>(producto);
            _productoRepository.Add(entity);

            return RedirectToAction("Index");
        }

        [Route("uploadSummernoteImage")]
        [HttpPost, ValidateInput(false)]
        public JsonResult UploadSummernoteImage()
        {
            var files = Request.Files;
            var path = "";
            using (var memoryStream = new MemoryStream())
            {
                for (var i = 0; i < files.Count; i++)
                {
                    files[i].InputStream.CopyTo(memoryStream);
                    path = _imageFileHandler.SaveImage(memoryStream.ToArray(), files[i].FileName, HttpContext, Subfolder);
                }   
            }
            var a = HttpContext.Request.MapPath(path);

            return Json(path);
        }


        public ActionResult Edit(int id)
        {
            var lst = new List<SelectListItem>();
            foreach (var category in GetAllProductosCategorias())
            {
                lst.Add(new SelectListItem { Text = category.Detalle, Value = category.Id.ToString() });
            }

            ViewBag.CategoriasList = lst;

            var item = _productoRepository.Get(id);
            var selected = lst.Where(x => x.Value == item.IdCategoria.ToString());
            if (selected.Any())
            {
                selected.First().Selected = true;
            }
            ViewBag.CategoriasList = lst;
            return View(_mapper.Map<ProductoViewModelItem>(item));
        }

        [Route("Edit")]
        [HttpPost, ValidateInput(false)]
        public ActionResult Edit(ProductoViewModelItem producto)
        {
            var entity = _mapper.Map<Producto>(producto);
            _productoRepository.Update(entity);

            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            _productoRepository.Delete(id);
            return RedirectToAction("Index");
        }

    }
}
