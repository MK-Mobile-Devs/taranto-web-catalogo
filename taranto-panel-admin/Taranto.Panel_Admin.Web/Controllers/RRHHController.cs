﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Taranto.Panel_Admin.Data;
using Taranto.Panel_Admin.Data.Repositories.Interfaces;
using Taranto.ViewModels;

namespace Taranto.Panel_Admin.Web.Controllers
{
    [Authorize]
    public class RRHHController : Controller
    {
        private readonly IRRHHRepository rrhhRepository;
        private readonly IMapper mapper;

        public RRHHController(IRRHHRepository rrhhRepository, IMapper mapper)
        {
            this.rrhhRepository = rrhhRepository;
            this.mapper = mapper;
        }


        // GET: Info
        public ActionResult Index()
        {
            var rrhh = rrhhRepository.GetAll();
            List<RRHHBusquedaVMItem> busquedasItems = new List<RRHHBusquedaVMItem>();
            rrhh.ForEach(i => busquedasItems.Add(mapper.Map<RRHHBusquedaVMItem>(i)));

            return View(new RRHHBusquedaVM()
            {
                Items = busquedasItems
            });
        }

        // GET: Info/Details/5
        public ActionResult Details(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RRHHBusquedaVMItem rrhh = mapper.Map<RRHHBusquedaVMItem>(rrhhRepository.Get(id));
            if (rrhh == null)
            {
                return HttpNotFound();
            }
            return View(rrhh);
        }

        // GET: Info/Edit/1
        public ActionResult Edit(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RRHHBusquedaVMItem rrhh = mapper.Map<RRHHBusquedaVMItem>(rrhhRepository.Get(id));
            
            if (rrhh == null)
            {
                return HttpNotFound();
            }
            return View(rrhh);
        }

        // POST: Info/Edit/1
        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(RRHHBusquedaVMItem rrhh)
        {
            if (ModelState.IsValid)
            {
                rrhhRepository.Update(mapper.Map<RecursosHumanosBusqueda>(rrhh));
                return RedirectToAction("Index");
            }
            return View(rrhh);
        }

        // GET: Info/Create
        public ActionResult Add()
        {
            return View(new RRHHBusquedaVMItem(true));
        }

        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Add(RRHHBusquedaVMItem rrhh)
        {
            if (ModelState.IsValid)
            {
                rrhhRepository.Add(mapper.Map<RecursosHumanosBusqueda>(rrhh));

                return RedirectToAction("Index");
            }

            return View(rrhh);
        }

        // POST: Info/Delete/5
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            rrhhRepository.Delete(id);
            return RedirectToAction("Index");
        }
    }
}
