﻿using AutoMapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using Taranto.Panel_Admin.Data;
using Taranto.Panel_Admin.Data.Repositories.Interfaces;
using Taranto.ViewModels;

namespace Taranto.Panel_Admin.Web.Controllers
{
    [Authorize]
    public class OfertasController : Controller
    {
        private readonly IOfertasRepository ofertasRepository;
        private readonly IMapper mapper;
        private readonly IImageFileHandler imageSaver;
        private const string Subfolder = "ofertas";

        public OfertasController(IOfertasRepository ofertasRepository, IMapper mapper, IImageFileHandler imageSaver)
        {
            this.ofertasRepository = ofertasRepository;
            this.mapper = mapper;
            this.imageSaver = imageSaver;
        }
        // GET: Ofertas
        public ActionResult Index()
        {
            var ofertas = ofertasRepository.GetAll();
            var ofertaItems = new List<OfertaViewModelItem>();
            ofertas.ForEach(p => ofertaItems.Add(mapper.Map<OfertaViewModelItem>(p)));

            return View(new OfertaViewModel()
            {
                Items = ofertaItems
            });
        }


        // GET: Ofertas/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Ofertas/Add
        [HttpGet]
        public async Task<ActionResult> Add()
        {
            var vm = new OfertaViewModel(true)
            {
                ClasesCliente = await GetClaseCliente(),
                Clientes = await GetClientes(),
                CuposCliente = await GetCupoCliente()
            };



            return View(vm);
        }

        private async Task<IEnumerable<OfertaCuposCliente>> GetCupoCliente()
        {
            var apiUrl = WebConfigurationManager.AppSettings["ApiUrl"];
            var url = $"{apiUrl}distribuidores/cupos";

            IEnumerable<OfertaCuposCliente> result;
            using (var client = new HttpClient())
            {
                var content = await client.GetStringAsync(url);
                result = JsonConvert.DeserializeObject<List<OfertaCuposCliente>>(content);
            }
            return result;

        }

        private async Task<IEnumerable<OfertaCliente>> GetClientes()
        {
            var apiUrl = WebConfigurationManager.AppSettings["ApiUrl"];
            var url = $"{apiUrl}distribuidores/sucursales";
            IEnumerable<OfertaCliente> result;
            using (var client = new HttpClient())
            {

                var content = await client.GetStringAsync(url);
                result = JsonConvert.DeserializeObject<List<OfertaCliente>>(content);
            }
            return result;
        }

        private static async Task<IEnumerable<OfertaClaseCliente>> GetClaseCliente()
        {
            var apiUrl = WebConfigurationManager.AppSettings["ApiUrl"];
            var url = $"{apiUrl}distribuidores/tipos";

            IEnumerable<OfertaClaseCliente> result;
            using (var client = new HttpClient())
            {
                var content = await client.GetStringAsync(url);
                result = JsonConvert.DeserializeObject<List<OfertaClaseCliente>>(content);
            }
            return result;
        }

        // POST: Ofertas/Add
        [HttpPost]
        public ActionResult Add(OfertaViewModelItem oferta)
        {
            try
            {
                if(oferta.ArchivoAdjunto.File != null)
                {
                    imageSaver.SaveImage(oferta.ArchivoAdjunto.fileByteArray, oferta.ArchivoAdjunto.File.FileName, HttpContext, Subfolder);
                }
                if (oferta.Image.File != null)
                {
                    imageSaver.SaveImage(oferta.Image.fileByteArray, oferta.Image.File.FileName, HttpContext, Subfolder);
                }

                Oferta entity = mapper.Map<Oferta>(oferta);
                var newId = ofertasRepository.Add(entity);
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch (DbEntityValidationException ex)
            {
                return View();
            }
        }

        // GET: Ofertas/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            var vm = new OfertaViewModel(true);
            var item = ofertasRepository.Get(id);
            vm.ClasesCliente = await GetClaseCliente();
            vm.Clientes = await GetClientes();
            vm.CuposCliente = await GetCupoCliente();
            var claseSelected = item.Clase.Split(';').ToList();
            var clienteSelected = item.Excluir.Split(';').ToList();
            var cupoSelected = item.Cuota.Split(';').ToList();


            
            foreach (var claseCliente in vm.ClasesCliente)
            {
                if (claseSelected.Contains(claseCliente.CustomerNumber))
                {
                    claseCliente.Selected = true;
                }
            }
            foreach (var cliente in vm.Clientes)
            {
                if (clienteSelected.Contains(cliente.CodigoCliente))
                {
                    cliente.Selected = true;
                }
            }
            foreach (var cupo in vm.CuposCliente)
            {
                if (cupoSelected.Contains(cupo.Descripcion))
                {
                    cupo.Selected = true;
                }
            }

            vm.Items = new List<OfertaViewModelItem>() { mapper.Map<OfertaViewModelItem>(item) };
            
            return View(vm);
        }

        // POST: Ofertas/Edit/5
        [HttpPost, ValidateInput(false)]
        public ActionResult Edit(OfertaViewModelItem editItem)
        {
            try
            {
                Oferta entity = mapper.Map<Oferta>(editItem);
                Oferta entityFromDB = ofertasRepository.GetAsNoTracking(editItem.Id);

                if (!string.IsNullOrWhiteSpace(editItem.existingPath) && editItem.Image.File != null)
                {
                    imageSaver.SaveImage(editItem.Image.fileByteArray, editItem.Image.File.FileName, HttpContext, Subfolder);

                }
                else
                {
                    entity.Imagen = entityFromDB.Imagen;
                }

                if (editItem.ArchivoAdjunto.File != null)
                {
                    imageSaver.SaveImage(editItem.ArchivoAdjunto.fileByteArray, editItem.ArchivoAdjunto.File.FileName, HttpContext, Subfolder);

                }
                else
                {
                    entity.Archivo = entityFromDB.Archivo;
                }

                ofertasRepository.Update(entity);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View();
            }
        }
        
        public ActionResult Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                ofertasRepository.Delete(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
