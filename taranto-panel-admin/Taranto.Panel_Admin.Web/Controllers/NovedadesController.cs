﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Taranto.Panel_Admin.Data;
using Taranto.Panel_Admin.Data.Repositories.Interfaces;
using Taranto.ViewModels;

namespace Taranto.Panel_Admin.Web.Controllers
{
    [Authorize]
    public class NovedadesController : Controller
    {
        private readonly INovedadesRepository novedadesRepository;
        private readonly IMapper mapper;
        private readonly IImageFileHandler imageSaver;
        private const string Subfolder = "novedades";

        public NovedadesController(INovedadesRepository novedadesRepository, IMapper mapper, IImageFileHandler imageSaver)
        {
            this.novedadesRepository = novedadesRepository;
            this.mapper = mapper;
            this.imageSaver = imageSaver;
        }
        [Route("/[controller]/[action]")]
        public ActionResult Index()
        {
            var result = novedadesRepository.GetAll();

            return View(new NovedadesViewModel()
            {
                Items = mapper.Map<List<NovedadesViewModelItem>>(result)
            });
        }

        // GET: Novedades/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Novedades/Create
        public ActionResult Add()
        {
            List<SelectListItem> lst = new List<SelectListItem>();
            foreach (var item in GetAllNovedadesCategorias())
            {
                lst.Add(new SelectListItem() { Text = item.Categoria.Translations["ES"], Value = item.Id.ToString() });
            }

            ViewBag.CategoriasList = lst;

            return View(new NovedadesViewModelItem(true));
        }

        private List<NovedadesCategoriaVMItem> GetAllNovedadesCategorias()
        {
            var a = novedadesRepository.GetAllCategorias();
            List<NovedadesCategoriaVMItem> res = new List<NovedadesCategoriaVMItem>();
            a.ForEach(x => res.Add(new NovedadesCategoriaVMItem() { Id = x.Id, Categoria = new TarantoTranslatableString("Categoria", x.CategoriaEs, x.CategoriaEn, x.CategoriaPr)}));

            return res;
        }

        // POST: Novedades/Create
        [HttpPost, ValidateInput(false)]
        public ActionResult Add(NovedadesViewModelItem novedad)
        {
            try
            {
                for (var i = 0; i < novedad.Galeria.File.Length; i++)
                {
                    imageSaver.SaveImage(novedad.Galeria.fileByteArray[i], novedad.Galeria.File[i].FileName, HttpContext, Subfolder);
                }
                imageSaver.SaveImage(novedad.Image.fileByteArray, novedad.Image.File.FileName, HttpContext, Subfolder);
                var entity = mapper.Map<Novedade>(novedad);
                novedadesRepository.Add(entity);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Novedades/Edit/5
        public ActionResult Edit(int id)
        {
            List<SelectListItem> lst = new List<SelectListItem>();
            foreach (var category in GetAllNovedadesCategorias())
            {
                lst.Add(new SelectListItem() { Text = category.Categoria.Translations["ES"], Value = category.Id.ToString() });
            }

            var item = novedadesRepository.Get(id);
            var selected = lst.Where(x => x.Value == item.IdCategoria.ToString());
            if (selected.Any())
            {
                selected.First().Selected = true;
            }
            ViewBag.CategoriasList = lst;
            return View(mapper.Map<NovedadesViewModelItem>(item));
        }

        // POST: Novedades/Edit/5
        [HttpPost, ValidateInput(false)]
        public ActionResult Edit(NovedadesViewModelItem novedad)
        {
            try
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFileBase file = Request.Files[i];
                    if (file.ContentLength > 0)
                    {
                        //saving code here

                    }
                }
                Novedade entity = mapper.Map<Novedade>(novedad);
                if (!string.IsNullOrWhiteSpace(novedad.existingPath) && novedad.Image.File != null)
                {
                    imageSaver.SaveImage(novedad.Image.fileByteArray, novedad.Image.File.FileName, HttpContext, Subfolder);
                    
                }
                else
                {
                    entity.Imagen = novedad.existingPath;
                }
                novedadesRepository.Update(entity);
                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }

        public ActionResult Delete(int id)
        {
            novedadesRepository.Delete(id);
            return RedirectToAction("Index");
        }

        // POST: Novedades/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
