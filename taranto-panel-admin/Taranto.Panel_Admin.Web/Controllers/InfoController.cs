﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Taranto.Panel_Admin.Data;
using Taranto.Panel_Admin.Data.Repositories.Interfaces;
using Taranto.ViewModels;

namespace Taranto.Panel_Admin.Web.Controllers
{
    [Authorize]
    public class InfoController : Controller
    {
        private readonly IInfoRepository infoRepository;
        private readonly IMapper mapper;
        private readonly List<string> tituloInstitucional;

        public InfoController(IInfoRepository infoRepository, IMapper mapper)
        {
            this.infoRepository = infoRepository;
            this.mapper = mapper;
        }


        // GET: Info
        public ActionResult Index()
        {
            var info = infoRepository.GetAll();
            List<InfoVMItem> infoItems = new List<InfoVMItem>();
            info.ForEach(i => infoItems.Add(mapper.Map<InfoVMItem>(i)));

            return View(new InfoVM()
            {
                Items = infoItems
            });
        }

        // GET: Info/Details/5
        public ActionResult Details(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InfoVMItem institucional = mapper.Map<InfoVMItem>(infoRepository.Get(id));
            if (institucional == null)
            {
                return HttpNotFound();
            }
            return View(institucional);
        }

        // GET: Info/Edit/1
        public ActionResult Edit(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            Info info = infoRepository.Get(id);

            if (info == null)
            {
                return HttpNotFound();
            }
            return View(mapper.Map<InfoVMItem>(info));
        }

        // POST: Info/Edit/1
        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(InfoVMItem info)
        {
            if (ModelState.IsValid)
            {
                infoRepository.Update(mapper.Map<Info>(info));
                return RedirectToAction("Index");
            }
            return View(info);
        }

        // GET: Info/Create
        public ActionResult Add()
        {
            return View(new InfoVMItem(true));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(InfoVMItem info)
        {
            if (ModelState.IsValid)
            {

                infoRepository.Add(mapper.Map<Info>(info));

                return RedirectToAction("Index");
            }

            return View(info);
        }

        // POST: Info/Delete/5
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            infoRepository.Delete(id);
            return RedirectToAction("Index");
        }
    }
}
