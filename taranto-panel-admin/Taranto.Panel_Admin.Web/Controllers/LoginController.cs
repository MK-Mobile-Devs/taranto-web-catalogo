﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Web.Mvc;
using System.Web.Security;
using Taranto.Panel_Admin.Data.Repositories;
using Taranto.Panel_Admin.Data.Repositories.Interfaces;
using Taranto.ViewModels;

namespace Taranto.Panel_Admin.Web.Controllers
{
    [System.Web.Mvc.AllowAnonymous]
    public class LoginController : Controller
    {

        private readonly IBackendRepository _backendRepository;

        public LoginController(IBackendRepository backendRepository)
        {
            _backendRepository = backendRepository;
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Index(string login_user, string login_pass)
        {
            if (ModelState.IsValid)
            {
                var data = _backendRepository.GetAll().Where(s => s.User.Equals(login_user) && s.Pass.Equals(login_pass)).ToList();
                if (data.Count() > 0)
                {
                    //add session
                    Session["User"] = data.FirstOrDefault().User;
                    FormsAuthentication.SetAuthCookie(login_user, false);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ViewBag.error = "Login failed";
                    return RedirectToAction("Index");
                }
            }
            return View();
        }


    }
}
