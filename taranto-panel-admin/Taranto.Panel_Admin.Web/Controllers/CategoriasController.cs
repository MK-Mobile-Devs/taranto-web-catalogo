﻿using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using Taranto.Panel_Admin.Data;
using Taranto.Panel_Admin.Data.Repositories;
using Taranto.Panel_Admin.Data.Repositories.Interfaces;
using Taranto.ViewModels;

namespace Taranto.Panel_Admin.Web.Controllers
{
    public class CategoriasController : Controller
    {

        private readonly IProductoRepository _productoRepository;
        private readonly IMapper _mapper;

        public CategoriasController(IProductoRepository productoRepository, IMapper mapper)
        {
            _productoRepository = productoRepository;
            _mapper = mapper;
        }

        // GET: Categorias
        public ActionResult Index()
        {
            var categorias = _productoRepository.GetAllCategorias();
            return View(new CategoriasViewModel
            {
                Items = _mapper.Map<List<CategoriaViewModelItem>>(categorias)
            });
        }

        // GET: Categorias/Create
        public ActionResult Add()
        {
            return View(new CategoriaViewModelItem());
        }

        // POST: Categorias/Create
        [HttpPost]
        public ActionResult Add(CategoriaViewModelItem categoria)
        {
            try
            {
                var entity = _mapper.Map<ProductosCategoria>(categoria);
                _productoRepository.AddCategoria(entity);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Categorias/Edit/5
        public ActionResult Edit(int id)
        {
            var item = _productoRepository.GetCategoria(id);
            return View(_mapper.Map<CategoriaViewModelItem>(item));
        }

        // POST: Categorias/Edit/5
        [HttpPost]
        public ActionResult Edit(CategoriaViewModelItem categoria)
        {
            try
            {
                var entity = _mapper.Map<ProductosCategoria>(categoria);
                _productoRepository.UpdateCategoria(entity);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(int id)
        {
            _productoRepository.DeleteCategoria(id);
            return RedirectToAction("Index");
        }
    }
}
