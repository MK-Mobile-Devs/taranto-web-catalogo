﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Taranto.Panel_Admin.Data;
using Taranto.Panel_Admin.Data.Repositories.Interfaces;
using Taranto.Panel_Admin.Web.Helpers;
using Taranto.ViewModels;

namespace Taranto.Panel_Admin.Web.Controllers
{
    [Authorize]
    public class NuevosDistribuidoresController : Controller
    {
        private readonly IBecomeDealerRepository _repository;
        private readonly IMapper mapper;

        public NuevosDistribuidoresController(IBecomeDealerRepository repository, IMapper mapper)
        {
            this._repository = repository;
            this.mapper = mapper;
        }

        public ActionResult Index()
        {
            var dealers = _repository.GetAll();
            List<BecomeDealerVMItem> items = new List<BecomeDealerVMItem>();
            dealers.ForEach(i => items.Add(mapper.Map<BecomeDealerVMItem>(i)));

            return View(new BecomeDealerVM()
            {
                Items = items
            });
        }

        // POST: Info/Delete/5
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            _repository.Delete(id);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult downloadExcel()
        {
            CsvExport myExport = new CsvExport();
            var dealers = _repository.GetAll();
            List<BecomeDealerVMItem> items = new List<BecomeDealerVMItem>();
            dealers.ForEach(i => items.Add(mapper.Map<BecomeDealerVMItem>(i)));

            foreach (var item in dealers)
            {
                myExport.AddRow();
                myExport["Fecha/Hora"] = item.Request_date;
                myExport["Razon Social"] = item.BusinessName;
                myExport["CUIT"] = item.Cuit;
                myExport["Codigo Postal"] = item.PostalCode;
                myExport["Provincia"] = item.State;
                myExport["Localidad"] = item.City;
                myExport["Dirección"] = item.Address;
                myExport["Tipo de Cliente"] = item.Type;
                myExport["Marcas"] = item.Brands;
                myExport["Email del Propietario"] = item.Email;
                myExport["Teléfono del Propietario"] = item.Tel;
                myExport["Nombre del Propietario"] = item.Name;
                myExport["Comentarios"] = item.Message;
                
            }
            return File(myExport.ExportToBytes(), "text/csv", "Taranto_nuevos_distribuidores.csv");

        }
    }
}
