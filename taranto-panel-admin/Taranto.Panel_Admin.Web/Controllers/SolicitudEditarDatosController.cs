﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Taranto.Panel_Admin.Data;
using Taranto.Panel_Admin.Data.Repositories.Interfaces;
using Taranto.ViewModels;

namespace Taranto.Panel_Admin.Web.Controllers
{
    [Authorize]
    public class SolicitudEditarDatosController : Controller
    {
        private readonly IUserDataEditRepository _repository;
        private readonly IMapper mapper;

        public SolicitudEditarDatosController(IUserDataEditRepository repository, IMapper mapper)
        {
            this._repository = repository;
            this.mapper = mapper;
        }

        public ActionResult Index()
        {
            var rrhh = _repository.GetAll();
            List<UserDataEditRequestVMItem> items = new List<UserDataEditRequestVMItem>();
            rrhh.ForEach(i => items.Add(mapper.Map<UserDataEditRequestVMItem>(i)));

            return View(new UserDataEditRequestVM()
            {
                Items = items
            });
        }

        public ActionResult Aprobar(int id)
        {
            var item = _repository.Get(id);
            if (item != null)
            {
                item.Received = item.Received == 0 ? 1 : 0;
                _repository.Update(item);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Rechazar(int id)
        {
            var item = _repository.Get(id);
            if (item != null)
            {
                item.Received = item.Received == 0 ? 2 : 0;
                _repository.Update(item);
            }
            return RedirectToAction("Index");
        }
    }
}
