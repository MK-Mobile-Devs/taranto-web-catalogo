﻿using AutoMapper;
using System.Collections.Generic;
using System.Web.Mvc;
using Taranto.Panel_Admin.Data;
using Taranto.Panel_Admin.Data.Repositories;
using Taranto.ViewModels;


namespace Taranto.Panel_Admin.Web.Controllers
{
    [Authorize]
    public class SliderHomeController : Controller
    {
        private IImageFileHandler imageSaver;
        private readonly ISliderHomeRepository sliderHomeRepository;
        private readonly IMapper mapper;
        private const string Subfolder = "sliderHome";

        public SliderHomeController(ISliderHomeRepository sliderHomeRepository, IMapper mapper, IImageFileHandler imageSaver)
        {
            this.imageSaver = imageSaver;
            this.sliderHomeRepository = sliderHomeRepository;
            this.mapper = mapper;
        }
       
        [Route("/[controller]/[action]")]
        public ActionResult Index()
        {
            if (TempData["message"] != null)
            {
                ViewBag.Message = TempData["message"];
                TempData["message"] = "";
            }
            var a = sliderHomeRepository.GetAll();
            
            return View(new SliderHomeViewModel()
            {
                Items = mapper.Map<List<SliderHomeViewModelItem>>(a)
            });
        }

        // GET: api/SliderHome/5
        public string Get(int id)
        {
            return "value";
        }
        [HttpGet]
        public ActionResult Add()
        {
            return View(new SliderHomeViewModelItem(true));
        }

        [HttpPost]
        public ActionResult Add(SliderHomeViewModelItem item)
        {
            imageSaver.SaveImage(item.Image.fileByteArray, item.Image.File.FileName, HttpContext, Subfolder);
            var result = sliderHomeRepository.Add(mapper.Map<CarrouselContenido>(item));
            TempData["message"] = "Grabado Correctamente";
            return RedirectToAction("Index");
        }
        public ActionResult Edit(int id)
        {
            var item = sliderHomeRepository.Get(id);

            return View(mapper.Map<SliderHomeViewModelItem>(item));
        }

       

        [HttpPost]
        public ActionResult Edit(SliderHomeViewModelItem edited)
        {
            var entity = mapper.Map<CarrouselContenido>(edited);
            if (edited.Image.File != null)
            {
                imageSaver.SaveImage(edited.Image.fileByteArray, edited.Image.File.FileName, HttpContext, Subfolder);
            }
            else
            {
                entity.Imagen = edited.existingPath;
            }
            sliderHomeRepository.Update(entity);
            TempData["message"] = "Actualizado correctamente";
            return RedirectToAction("Index");
        }

        // DELETE: api/SliderHome/5
        public ActionResult Delete(int id)
        {
            sliderHomeRepository.Delete(id);
            TempData["message"] = "Borrado correctamente"; 
            return RedirectToAction("Index");
        }
    }
}
