﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Taranto.Panel_Admin.Data;
using Taranto.Panel_Admin.Data.Repositories;
using Taranto.Panel_Admin.Data.Repositories.Interfaces;
using Taranto.ViewModels;

namespace Taranto.Panel_Admin.Web.Controllers
{
    [Authorize]
    public class DatosCuentasBancariasController : Controller
    {
        private readonly IBankAccountRepository _repository;
        private readonly IMapper mapper;
        private readonly IImageFileHandler imageSaver;

        public DatosCuentasBancariasController(IBankAccountRepository repository, IMapper mapper, IImageFileHandler imageSaver)
        {
            this._repository = repository;
            this.mapper = mapper;
            this.imageSaver = imageSaver;
        }
        [Route("/[controller]/[action]")]
        public ActionResult Index()
        {
            var result = _repository.GetAll().First();

            return View(mapper.Map<BankAccountVM>(result));
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult Edit(BankAccountVM item)
        {
            try
            {
                BankAccount entity = mapper.Map<BankAccount>(item);
                _repository.Update(entity);
                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }
    }
}
