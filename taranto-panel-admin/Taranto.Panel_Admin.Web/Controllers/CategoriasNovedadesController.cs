﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Taranto.Panel_Admin.Data;
using Taranto.Panel_Admin.Data.Repositories.Interfaces;
using Taranto.ViewModels;

namespace Taranto.Panel_Admin.Web.Controllers
{
    [Authorize]
    public class CategoriasNovedadesController : Controller
    {
        private readonly ICategoriasNovedadesRepository categoriasNovedadesRepository;
        private readonly IMapper mapper;

        public CategoriasNovedadesController(ICategoriasNovedadesRepository categoriasNovedadesRepository, IMapper mapper)
        {
            this.categoriasNovedadesRepository = categoriasNovedadesRepository;
            this.mapper = mapper;
        }


        // GET: CategoriasNovedades
        public ActionResult Index()
        {
            var vm = new NovedadesCategoriaVM()
            {
                Items = mapper.Map<IList<NovedadesCategoriaVMItem>>(categoriasNovedadesRepository.GetAll())
            };
            return View(vm);
        }

        // GET: CategoriasNovedades/Details/5
        public ActionResult Details(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NovedadesCategoriaVMItem categoriasNovedades = mapper.Map<NovedadesCategoriaVMItem>(categoriasNovedadesRepository.Get(id));
            if (categoriasNovedades == null)
            {
                return HttpNotFound();
            }
            return View(categoriasNovedades);
        }

        // GET: CategoriasNovedades/Create
        public ActionResult Add()
        {
            return View(new NovedadesCategoriaVMItem(true));
        }

        // POST: CategoriasNovedades/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(NovedadesCategoriaVMItem categoriaNovedades)
        {
            if (ModelState.IsValid)
            {
                categoriasNovedadesRepository.Add(mapper.Map<NovedadesCategoria>(categoriaNovedades));
                
                return RedirectToAction("Index");
            }

            return View(categoriaNovedades);
        }

        // GET: CategoriasNovedades/Edit/5
        public ActionResult Edit(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            NovedadesCategoria novedadesCategoria = categoriasNovedadesRepository.Get(id);
            if (novedadesCategoria == null)
            {
                return HttpNotFound();
            }
            return View(mapper.Map<NovedadesCategoriaVMItem>(novedadesCategoria));
        }

        // POST: CategoriasNovedades/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(NovedadesCategoriaVMItem novedadesCategoria)
        {
            if (ModelState.IsValid)
            {
                categoriasNovedadesRepository.Update(mapper.Map<NovedadesCategoria>(novedadesCategoria));
                return RedirectToAction("Index");
            }
            return View(novedadesCategoria);
        }

        public ActionResult Delete(int id)
        {
            categoriasNovedadesRepository.Delete(id);
            return RedirectToAction("Index");
        }

        // GET: CategoriasNovedades/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }

        //    ////if (novedadesCategoria == null)
        //    ////{
        //    ////    return HttpNotFound();
        //    ////}
        //    //return View(novedadesCategoria);
        //    return View();
        //}

        //// POST: CategoriasNovedades/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    NovedadesCategoria novedadesCategoria = db.NovedadesCategorias.Find(id);
        //    db.NovedadesCategorias.Remove(novedadesCategoria);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
