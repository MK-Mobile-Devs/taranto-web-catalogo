﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Taranto.Panel_Admin.Data;
using Taranto.Panel_Admin.Data.Repositories.Interfaces;
using Taranto.ViewModels;

namespace Taranto.Panel_Admin.Web.Controllers
{
    [Authorize]
    public class ListaDePreciosController : Controller
    {
        private readonly IListaDePreciosRepository _repository;
        private readonly IMapper mapper;
        private readonly IImageFileHandler imageSaver;
        private const string Subfolder = "listaDePrecios";

        public ListaDePreciosController(IListaDePreciosRepository repository, IMapper mapper, IImageFileHandler imageSaver)
        {
            this._repository = repository;
            this.mapper = mapper;
            this.imageSaver = imageSaver;
        }
        [Route("/[controller]/[action]")]
        public ActionResult Index()
        {
            var result = _repository.GetAll();

            return View(new ListaDePreciosVM()
            {
                Items = mapper.Map<List<ListaDePreciosVMItem>>(result)
            });
        }

        // GET: Novedades/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Novedades/Create
        public ActionResult Add()
        {

            return View(new ListaDePreciosVMItem());
        }


        // POST: Novedades/Create
        [HttpPost, ValidateInput(false)]
        public ActionResult Add(ListaDePreciosVMItem item)
        {

            try
            {

                if (item.Archivo1?.File != null)
                {
                    item.Archivo1.FileName = "1" + GenerateFileName(item.Archivo1.File.FileName); 
                    imageSaver.SaveImage(item.Archivo1.fileByteArray, item.Archivo1.FileName, HttpContext, Subfolder);
                }
                if (item.Archivo2?.File != null)
                {
                    item.Archivo2.FileName = "2" + GenerateFileName(item.Archivo2.File.FileName);
                    imageSaver.SaveImage(item.Archivo2.fileByteArray, item.Archivo2.FileName, HttpContext, Subfolder);
                }
                else
                {
                    item.NombreArchivo2 = "";
                    item.Archivo2.FileName = "";
                }
                if (item.Archivo3?.File != null)
                {
                    item.Archivo3.FileName = "3" + GenerateFileName(item.Archivo3.File.FileName);
                    imageSaver.SaveImage(item.Archivo3.fileByteArray, item.Archivo3.FileName, HttpContext, Subfolder);
                }
                else
                {
                    item.NombreArchivo3 = "";
                    item.Archivo3.FileName = "";
                }

                var entity = mapper.Map<ListasDePrecio>(item);
                _repository.Add(entity);

                return RedirectToAction("Index");
            }
            catch(Exception e)
            {
                return Content(e.ToString());
            }
        }

        private static string GenerateFileName(string FileName)
        {
            var random = new Random();

            var newName = random.Next().ToString();
            var extension = System.IO.Path.GetExtension(FileName);
            return  newName + extension;
        }
        
        public ActionResult Edit(int id)
        {
            var item = _repository.Get(id);
            ListaDePreciosVMItem returnVm = mapper.Map<ListaDePreciosVMItem>(item);
            return View(returnVm);
        }
        
        [HttpPost, ValidateInput(false)]
        public ActionResult Edit(ListaDePreciosVMItem item)
        {
            try
            {
                ListasDePrecio entity = mapper.Map<ListasDePrecio>(item);
                _repository.Update(entity);
                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }

        public ActionResult Delete(int id)
        {
            _repository.Delete(id);
            return RedirectToAction("Index");
        }

       
    }
}
