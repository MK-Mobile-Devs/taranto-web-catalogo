﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Taranto.Panel_Admin.Data;
using Taranto.Panel_Admin.Data.Repositories.Interfaces;
using Taranto.ViewModels;

namespace Taranto.Panel_Admin.Web.Controllers
{
    [Authorize]
    public class CtaCteComprobantesController : Controller
    {
        private readonly ICtaCteTicketRepository _repository;
        private readonly IMapper mapper;

        public CtaCteComprobantesController(ICtaCteTicketRepository repository, IMapper mapper)
        {
            this._repository = repository;
            this.mapper = mapper;
        }


        // GET: Info
        public ActionResult Index()
        {
            var consultas = _repository.GetAll();
            List<CtaCteTicketVMItem> consultasItems = new List<CtaCteTicketVMItem>();
            consultas.ForEach(i => consultasItems.Add(mapper.Map<CtaCteTicketVMItem>(i)));

            return View(new CtaCteTicketVM()
            {
                Items = consultasItems
            });
        }

        // GET: Info/Details/5
        public ActionResult Details(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CtaCteTicketVMItem consulta = mapper.Map<CtaCteTicketVMItem>(_repository.Get(id));
            if (consulta == null)
            {
                return HttpNotFound();
            }
            return View(consulta);
        }

        // POST: Info/Delete/5
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult MarkAsReceived(int id)
        {
            var item = _repository.Get(id);
            if(item != null)
            {
                item.Received = item.Received == 0 ? 1 : 0;
                _repository.Update(item);
            }
            return RedirectToAction("Index");
        }
    }
}
