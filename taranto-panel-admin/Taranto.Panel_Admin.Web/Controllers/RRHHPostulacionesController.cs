﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Taranto.Panel_Admin.Data;
using Taranto.Panel_Admin.Data.Repositories.Interfaces;
using Taranto.Panel_Admin.Web.Helpers;
using Taranto.ViewModels;

namespace Taranto.Panel_Admin.Web.Controllers
{
    [Authorize]
    public class RRHHPostulacionesController : Controller
    {
        private readonly IRRHHPostulateRepository _repository;
        private readonly IMapper mapper;

        public RRHHPostulacionesController(IRRHHPostulateRepository repository, IMapper mapper)
        {
            this._repository = repository;
            this.mapper = mapper;
        }


        // GET: Info
        public ActionResult Index()
        {
            var rrhh = _repository.GetAllEnumerable().OrderByDescending(e => e.PostulateFecha).ToList();
            List<RRHHPostulateVMItem> items = new List<RRHHPostulateVMItem>();
            rrhh.ForEach(i => items.Add(mapper.Map<RRHHPostulateVMItem>(i)));

            return View(new RRHHPostulateVM()
            {
                Items = items
            });
        }

        // POST: Info/Delete/5
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            _repository.Delete(id);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult downloadExcel()
        {
            CsvExport myExport = new CsvExport();
            var rrhh = _repository.GetAll();
            List<RRHHPostulateVMItem> items = new List<RRHHPostulateVMItem>();
            rrhh.ForEach(i => items.Add(mapper.Map<RRHHPostulateVMItem>(i)));

            foreach (var item in rrhh)
            {
                myExport.AddRow();
                myExport["Fecha/Hora"] = item.PostulateFecha;
                myExport["Nombre y Apellido"] = item.Name;
                myExport["DNI"] = item.Dni;
                myExport["Nacionalidad"] = item.Nationality;
                myExport["Provincia"] = item.State;
                myExport["Localidad"] = item.City;
                myExport["Direccion"] = item.Address;
                myExport["Email"] = item.Email;
                myExport["Celular"] = item.Tel;
                myExport["Fecha de Naciemiento"] = item.Birthdate;
                myExport["Sexo"] = item.Sex;
                myExport["Comentarios"] = item.Message;
                myExport["CV adjunto"] = "http://www.taranto.com.ar/media/cv/" + item.File.Replace(" ", "%20");
            }
            return File(myExport.ExportToBytes(), "text/csv", "Taranto_cv_rrhh_postulaciones.csv");

        }
    }
}
