﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Taranto.Panel_Admin.Data;
using Taranto.Panel_Admin.Data.Repositories.Interfaces;
using Taranto.Panel_Admin.Web.Helpers;
using Taranto.ViewModels;

namespace Taranto.Panel_Admin.Web.Controllers
{
    [Authorize]
    public class SuscriptosController : Controller
    {
        private readonly ISubscribeRepository _repository;
        private readonly IMapper mapper;

        public SuscriptosController(ISubscribeRepository repository, IMapper mapper)
        {
            this._repository = repository;
            this.mapper = mapper;
        }


        // GET: Info
        public ActionResult Index()
        {
            var response = _repository.GetAll();
            List<SuscriptosVMItem> items = new List<SuscriptosVMItem>();
            response.ForEach(i => items.Add(mapper.Map<SuscriptosVMItem>(i)));

            return View(new SuscriptosVM()
            {
                Items = items
            });
        }

        // GET: Info/Details/5
        public ActionResult Details(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ConsultaVMItem consulta = mapper.Map<ConsultaVMItem>(_repository.Get(id));
            if (consulta == null)
            {
                return HttpNotFound();
            }
            return View(consulta);
        }

        // POST: Info/Delete/5
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            _repository.Delete(id);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult downloadExcel()
        {
            CsvExport myExport = new CsvExport();
            var response = _repository.GetAll();
            List<SuscriptosVMItem> items = new List<SuscriptosVMItem>();
            response.ForEach(i => items.Add(mapper.Map<SuscriptosVMItem>(i)));
            foreach (var item in items)
            {
                myExport.AddRow();
                myExport["Email"] = item.Email;

            }
            return File(myExport.ExportToBytes(), "text/csv", "Taranto_suscriptos_newsletter.csv");

        }
    }
}
