﻿using AutoMapper;
using System;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Taranto.Panel_Admin.Data;
using Taranto.Panel_Admin.Data.DTO;
using Taranto.Panel_Admin.Data.Repositories;
using Taranto.Panel_Admin.Data.Repositories.Interfaces;
using Taranto.ViewModels;
using Unity;
using Unity.AspNet.Mvc;

namespace Taranto.Panel_Admin.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            MapperConfig.RegisterProfiles();
            Initialise();

        }

        private static void Initialise()
        {
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));

            IDependencyResolver resolver = DependencyResolver.Current;

            DependencyResolver.SetResolver(resolver);

        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();
            
            container.RegisterType<IBackendRepository, BackendRepository>();
            container.RegisterType<ISliderHomeRepository, SliderHomeRepository>();
            container.RegisterType<IProductoRepository, ProductoRepository>();
            container.RegisterType<INovedadesRepository, NovedadesRepository>();
            container.RegisterType<ICategoriasNovedadesRepository, CategoriasNovedadesRepository>();
            container.RegisterType<IInstitucionalRepository, InstitucionalRepository>();
            container.RegisterType<IInfoRepository, InfoRepository>();
            container.RegisterType<IRRHHRepository, RRHHRepository>();
            container.RegisterType<IConsultasRepository, ConsultasRepository>();
            container.RegisterType<ISubscribeRepository, SubscribeRepository>();
            container.RegisterType<IRRHHPostulateRepository, RRHHPostulateRepository>();
            container.RegisterType<IBecomeDealerRepository, BecomeDealerRepository>();
            container.RegisterType<ICtaCteTicketRepository, CtaCteTicketRepository>();
            container.RegisterType<IUserDataEditRepository, UserDataEditRepository>();
            container.RegisterType<IChatRepository, ChatRepository>();
            container.RegisterType<IChatCategoryRepository, ChatCategoryRepository>();
            container.RegisterType<IOfertasRepository, OfertasRepository>();
            container.RegisterType<IListaDePreciosRepository, ListaDePreciosRepository>();
            container.RegisterType<IBankAccountRepository, BankAccountRepository>();
            container.RegisterType<IBoletinesRepository, BoletinesRepository>();
            container.RegisterType<IRevistaRepository, RevistaRepository>();
            container.RegisterType<IImageFileHandler, ImageFileHandler>();
            container.RegisterInstance<IMapper>(MapperConfig.Mapper);
            container.RegisterType<AdminEntities, AdminEntities>();

            return container;
        }
    }
    public class MapperConfig
    {
        public static IMapper Mapper { get; set; }
        public static void RegisterProfiles()
        {
            var config = new MapperConfiguration(cfg =>
            {
                #region Data To Viewmodels
                cfg.CreateMap<CarrouselContenido, SliderHomeViewModelItem>()
                .ForMember(x => x.Titulo, d => d.MapFrom(s => new TarantoTranslatableString(d.DestinationMember.Name, s.TituloEs, s.TituloEn, s.TituloPr)))
                .ForMember(x => x.Image, d => d.MapFrom(s => new ImageViewModel($"sliderHome/{s.Imagen}")))
                .ForMember(x => x.SubTitulo, d => d.MapFrom(s => new TarantoTranslatableString(d.DestinationMember.Name, s.SubtituloEs, s.SubtituloEn, s.SubtituloPr)));

                cfg.CreateMap<Producto, ProductoViewModelItem>()
                .ForMember(x => x.Titulo, d => d.MapFrom(s => new TarantoTranslatableString(d.DestinationMember.Name, s.TituloEs, s.TituloEn, s.TituloPr)))
                .ForMember(x => x.Descripcion, d => d.MapFrom(s => new TarantoTranslatableStringSummernote(d.DestinationMember.Name, s.DescripcionEs, s.DescripcionEn, s.DescripcionPr)));

                cfg.CreateMap<ProductoDTO, ProductoViewModelItem>()
                .ForMember(x => x.Titulo, d => d.MapFrom(s => new TarantoTranslatableString(d.DestinationMember.Name, s.TituloEs, s.TituloEn, s.TituloPr)))
                .ForMember(x => x.Descripcion, d => d.MapFrom(s => new TarantoTranslatableStringSummernote(d.DestinationMember.Name, s.DescripcionEs, s.DescripcionEn, s.DescripcionPr)));

                cfg.CreateMap<ProductoCategoriaDTO, ProductoCategoriaViewModelItem>();

                cfg.CreateMap<ProductosCategoria, CategoriaViewModelItem>()
                    .ForMember(x => x.Titulo,
                        d => d.MapFrom(s =>
                            new TarantoTranslatableString(d.DestinationMember.Name, s.Detalle, s.DetalleEn,
                                s.DetallePr)));
                cfg.CreateMap<CategoriaViewModelItem, ProductosCategoria>()
                    .ForMember(x => x.DetalleEn, d => d.MapFrom(s => s.Titulo.Translations[Language.EN.ToString()]))
                    .ForMember(x => x.Detalle, d => d.MapFrom(s => s.Titulo.Translations[Language.ES.ToString()]))
                    .ForMember(x => x.DetallePr, d => d.MapFrom(s => s.Titulo.Translations[Language.BR.ToString()]));


                cfg.CreateMap<NovedadesCategoria, NovedadesCategoriaVMItem>()
               .ForMember(x => x.Categoria, d => d.MapFrom(s => new TarantoTranslatableString(d.DestinationMember.Name, s.CategoriaEs, s.CategoriaEn, s.CategoriaPr)));

                cfg.CreateMap<Novedade, NovedadesViewModelItem>()
               .ForMember(x => x.Titulo, d => d.MapFrom(s => new TarantoTranslatableString(d.DestinationMember.Name, s.TituloEs, s.TituloEn, s.TituloPr)))
               .ForMember(x => x.Subtitulo, d => d.MapFrom(s => new TarantoTranslatableString(d.DestinationMember.Name, s.SubtituloEs, s.SubtituloEn, s.SubtituloPr)))
               .ForMember(x => x.Fecha, d => d.MapFrom(s => s.Fecha))
               .ForMember(x => x.Image, d => d.MapFrom(s => new ImageViewModel($"novedades/{s.Imagen}")))
               .ForMember(x => x.Descripcion, d => d.MapFrom(s => new TarantoTranslatableStringSummernote(d.DestinationMember.Name, s.DescripcionEs, s.DescripcionEn, s.DescripcionPr)));

                cfg.CreateMap<PaginasInstitucionale, InstitucionalVMItem>()
               .ForMember(x => x.Descripcion, d => d.MapFrom(s => new TarantoTranslatableStringSummernote(d.DestinationMember.Name, s.DescripcionEs, s.DescripcionEn, s.DescripcionPr)));

                cfg.CreateMap<Info, InfoVMItem>()
                .ForMember(x => x.Title, d => d.MapFrom(s => new TarantoTranslatableString(d.DestinationMember.Name, s.TitleEs, s.TitleEn, s.TitlePr)))
                .ForMember(x => x.Description, d => d.MapFrom(s => new TarantoTranslatableStringSummernote(d.DestinationMember.Name, s.DescriptionEs, s.DescriptionEn, s.DescriptionPr)));

                cfg.CreateMap<RecursosHumanosBusqueda, RRHHBusquedaVMItem>()
                .ForMember(x => x.Funcion, d => d.MapFrom(s => new TarantoTranslatableString(d.DestinationMember.Name, s.FuncionEs, s.FuncionEn, s.FuncionPr)))
                .ForMember(x => x.Equipo, d => d.MapFrom(s => new TarantoTranslatableString(d.DestinationMember.Name, s.EquipoEs, s.EquipoEn, s.EquipoPr)))
                .ForMember(x => x.Ciudad, d => d.MapFrom(s => new TarantoTranslatableString(d.DestinationMember.Name, s.CiudadEs, s.CiudadEn, s.CiudadPr)))
                .ForMember(x => x.Descripcion, d => d.MapFrom(s => new TarantoTranslatableStringSummernote(d.DestinationMember.Name, s.DescripcionEs, s.DescripcionEn, s.DescripcionPr)));

                cfg.CreateMap<Contacto, ConsultaVMItem>();
                cfg.CreateMap<Subscribe, SuscriptosVMItem>();
                cfg.CreateMap<RrhhPostulate, RRHHPostulateVMItem>();
                cfg.CreateMap<BecomeDealer, BecomeDealerVMItem>();
                cfg.CreateMap<CtacteTicket, CtaCteTicketVMItem>();
                cfg.CreateMap<UserDataEditRequest, UserDataEditRequestVMItem>()
                .ForMember(x=> x.Img, d=> d.Ignore());
                cfg.CreateMap<ChatMessage, ChatMsgVM>();
                cfg.CreateMap<Chat, ChatVMItem>();
                cfg.CreateMap<ChatCategory, ChatCategoryVMItem>()
               .ForMember(x => x.Categoria, d => d.MapFrom(s => new TarantoTranslatableString(d.DestinationMember.Name, s.CategoryEs, s.CategoryEn, s.CategoryPr)));
                cfg.CreateMap<ListasDePrecio, ListaDePreciosVMItem>()
                .ForMember(x => x.Archivo1, d => d.MapFrom(s => new GenericFileViewModel($"listaDePrecios/{s.Archivo1}")))
                .ForMember(x => x.Archivo2, d => d.MapFrom(s => new GenericFileViewModel($"listaDePrecios/{s.Archivo2}")))
                .ForMember(x => x.Archivo3, d => d.MapFrom(s => new GenericFileViewModel($"listaDePrecios/{s.Archivo3}")));

                cfg.CreateMap<Oferta, OfertaViewModelItem>()
               .ForMember(x => x.ArchivoAdjunto, d => d.MapFrom(s => new FileViewModel($"ofertas/{s.Archivo}")))
               .ForMember(x => x.Clase, d => d.MapFrom(s => s.Clase.Split(';')))
               .ForMember(x => x.Cuota, d => d.MapFrom(s => s.Cuota.Split(';')))
               .ForMember(x => x.Excluir, d => d.MapFrom(s => s.Excluir.Split(';')))
               .ForMember(x => x.FechaAlta, d => d.MapFrom(s => s.FechaAlta))
               .ForMember(x => x.FechaCierre, d => d.MapFrom(s => s.FechaCierre))
               .ForMember(x => x.Image, d => d.MapFrom(s => new ImageViewModel($"ofertas/{s.Imagen}")))
               .ForMember(x => x.Titulo, d => d.MapFrom(s => s.Titulo));

                cfg.CreateMap<BankAccount, BankAccountVM>();

                cfg.CreateMap<BoletinLanzamiento, BoletinesVMItem>()
                    .ForMember(x => x.Archivo, d => d.MapFrom(s => new FileViewModel($"boletines/{s.Archivo}")))
                    .ForMember(x => x.Imagen, d => d.MapFrom(s => new ImageViewModel($"boletines/{s.Imagen}")))
                    .ForMember(x => x.Titulo, d => d.MapFrom(s => new TarantoTranslatableString(d.DestinationMember.Name, s.TituloEs, s.TituloEn, s.TituloPr)));

                cfg.CreateMap<Magazine, RevistaVMItem>()
                .ForMember(x => x.Titulo, d => d.MapFrom(s => new TarantoTranslatableString(d.DestinationMember.Name, s.TitleEs, s.TitleEn, s.TitlePr)))
                .ForMember(x => x.SubTitulo, d => d.MapFrom(s => new TarantoTranslatableString(d.DestinationMember.Name, s.SubtitleEs, s.SubtitleEn, s.SubtitlePr)))
                .ForMember(x => x.File, d => d.MapFrom(s => new FileViewModel($"revista/{s.File}")))
                .ForMember(x => x.Image, d => d.MapFrom(s => new ImageViewModel($"revista/{s.Img}")));


                #endregion


                #region ViewModels to data
                cfg.CreateMap<SliderHomeViewModelItem, CarrouselContenido>()
               .ForMember(x => x.TituloEn, d => d.MapFrom(s => s.Titulo.Translations[Language.EN.ToString()]))
               .ForMember(x => x.TituloEs, d => d.MapFrom(s => s.Titulo.Translations[Language.ES.ToString()]))
               .ForMember(x => x.TituloPr, d => d.MapFrom(s => s.Titulo.Translations[Language.BR.ToString()]))

               .ForMember(x => x.SubtituloEn, d => d.MapFrom(s => s.SubTitulo.Translations[Language.EN.ToString()]))
               .ForMember(x => x.SubtituloEs, d => d.MapFrom(s => s.SubTitulo.Translations[Language.ES.ToString()]))
               .ForMember(x => x.SubtituloPr, d => d.MapFrom(s => s.SubTitulo.Translations[Language.BR.ToString()]))

               .ForMember(x => x.Imagen, d => d.MapFrom(s => s.Image != null && s.Image.File != null ? s.Image.File.FileName : ""))
               .ForMember(x => x.Rank, d => d.MapFrom(s => s.Rank));

                cfg.CreateMap<ProductoViewModelItem, Producto>()
               .ForMember(x => x.TituloEn, d => d.MapFrom(s => s.Titulo.Translations[Language.EN.ToString()]))
               .ForMember(x => x.TituloEs, d => d.MapFrom(s => s.Titulo.Translations[Language.ES.ToString()]))
               .ForMember(x => x.TituloPr, d => d.MapFrom(s => s.Titulo.Translations[Language.BR.ToString()]))

               .ForMember(x => x.DescripcionEn, d => d.MapFrom(s => s.Descripcion.Translations[Language.EN.ToString()]))
               .ForMember(x => x.DescripcionEs, d => d.MapFrom(s => s.Descripcion.Translations[Language.ES.ToString()]))
               .ForMember(x => x.DescripcionPr, d => d.MapFrom(s => s.Descripcion.Translations[Language.BR.ToString()]));

                cfg.CreateMap<NovedadesViewModelItem, Novedade>()
                .ForMember(x => x.TituloEn, d => d.MapFrom(s => s.Titulo.Translations[Language.EN.ToString()]))
                .ForMember(x => x.TituloEs, d => d.MapFrom(s => s.Titulo.Translations[Language.ES.ToString()]))
                .ForMember(x => x.TituloPr, d => d.MapFrom(s => s.Titulo.Translations[Language.BR.ToString()]))
                .ForMember(x => x.SubtituloEn, d => d.MapFrom(s => s.Subtitulo.Translations[Language.EN.ToString()]))
                .ForMember(x => x.SubtituloEs, d => d.MapFrom(s => s.Subtitulo.Translations[Language.ES.ToString()]))
                .ForMember(x => x.SubtituloPr, d => d.MapFrom(s => s.Subtitulo.Translations[Language.BR.ToString()]))
                .ForMember(x => x.DescripcionEn, d => d.MapFrom(s => s.Descripcion.Translations[Language.EN.ToString()]))
                .ForMember(x => x.DescripcionEs, d => d.MapFrom(s => s.Descripcion.Translations[Language.ES.ToString()]))
                .ForMember(x => x.DescripcionPr, d => d.MapFrom(s => s.Descripcion.Translations[Language.BR.ToString()]))
                .ForMember(x => x.Imagen, d => d.MapFrom(s => s.Image != null && s.Image.File != null ? s.Image.File.FileName : ""));

                cfg.CreateMap<InstitucionalVMItem, PaginasInstitucionale>()
                .ForMember(x => x.DescripcionEn, d => d.MapFrom(s => s.Descripcion.Translations[Language.EN.ToString()]))
                .ForMember(x => x.DescripcionEs, d => d.MapFrom(s => s.Descripcion.Translations[Language.ES.ToString()]))
                .ForMember(x => x.DescripcionPr, d => d.MapFrom(s => s.Descripcion.Translations[Language.BR.ToString()]));

                cfg.CreateMap<InfoVMItem, Info>()
                .ForMember(x => x.TitleEn, d => d.MapFrom(s => s.Title.Translations[Language.EN.ToString()]))
                .ForMember(x => x.TitleEs, d => d.MapFrom(s => s.Title.Translations[Language.ES.ToString()]))
                .ForMember(x => x.TitlePr, d => d.MapFrom(s => s.Title.Translations[Language.BR.ToString()]))
                .ForMember(x => x.DescriptionEn, d => d.MapFrom(s => s.Description.Translations[Language.EN.ToString()]))
                .ForMember(x => x.DescriptionEs, d => d.MapFrom(s => s.Description.Translations[Language.ES.ToString()]))
                .ForMember(x => x.DescriptionPr, d => d.MapFrom(s => s.Description.Translations[Language.BR.ToString()]))
                .ForMember(x => x.UrlTitle, d => d.MapFrom(s => string.Format("http://www.taranto.com.ar/info-{0}", s.Title.Translations[Language.ES.ToString()]).Replace(" ", "-")));

                cfg.CreateMap<RRHHBusquedaVMItem, RecursosHumanosBusqueda>()
                .ForMember(x => x.FuncionEn, d => d.MapFrom(s => s.Funcion.Translations[Language.EN.ToString()]))
                .ForMember(x => x.FuncionEs, d => d.MapFrom(s => s.Funcion.Translations[Language.ES.ToString()]))
                .ForMember(x => x.FuncionPr, d => d.MapFrom(s => s.Funcion.Translations[Language.BR.ToString()]))
                .ForMember(x => x.EquipoEn, d => d.MapFrom(s => s.Equipo.Translations[Language.EN.ToString()]))
                .ForMember(x => x.EquipoEs, d => d.MapFrom(s => s.Equipo.Translations[Language.ES.ToString()]))
                .ForMember(x => x.EquipoPr, d => d.MapFrom(s => s.Equipo.Translations[Language.BR.ToString()]))
                .ForMember(x => x.CiudadEn, d => d.MapFrom(s => s.Ciudad.Translations[Language.EN.ToString()]))
                .ForMember(x => x.CiudadEs, d => d.MapFrom(s => s.Ciudad.Translations[Language.ES.ToString()]))
                .ForMember(x => x.CiudadPr, d => d.MapFrom(s => s.Ciudad.Translations[Language.BR.ToString()]))
                .ForMember(x => x.DescripcionEn, d => d.MapFrom(s => s.Descripcion.Translations[Language.EN.ToString()]))
                .ForMember(x => x.DescripcionEs, d => d.MapFrom(s => s.Descripcion.Translations[Language.ES.ToString()]))
                .ForMember(x => x.DescripcionPr, d => d.MapFrom(s => s.Descripcion.Translations[Language.BR.ToString()]));

                cfg.CreateMap<NovedadesCategoriaVMItem, NovedadesCategoria>()
               .ForMember(x => x.CategoriaEn, d => d.MapFrom(s => s.Categoria.Translations[Language.EN.ToString()]))
               .ForMember(x => x.CategoriaEs, d => d.MapFrom(s => s.Categoria.Translations[Language.ES.ToString()]))
               .ForMember(x => x.CategoriaPr, d => d.MapFrom(s => s.Categoria.Translations[Language.BR.ToString()]));

                cfg.CreateMap<ChatCategoryVMItem, ChatCategory>()
                .ForMember(x => x.CategoryEn, d => d.MapFrom(s => s.Categoria.Translations[Language.EN.ToString()]))
                .ForMember(x => x.CategoryEs, d => d.MapFrom(s => s.Categoria.Translations[Language.ES.ToString()]))
                .ForMember(x => x.CategoryPr, d => d.MapFrom(s => s.Categoria.Translations[Language.BR.ToString()]));

                cfg.CreateMap<ChatVMItem, Chat>();
                cfg.CreateMap<ChatMsgVM, ChatMessage>();

                cfg.CreateMap<ListaDePreciosVMItem, ListasDePrecio>()
                 .ForMember(x => x.Archivo1, d => d.MapFrom(s => s.Archivo1 != null && s.Archivo1.File != null ? s.Archivo1.FileName : ""))
                 .ForMember(x => x.Archivo2, d => d.MapFrom(s => s.Archivo2 != null && s.Archivo2.File != null ? s.Archivo2.FileName : ""))
                 .ForMember(x => x.Archivo3, d => d.MapFrom(s => s.Archivo3 != null && s.Archivo3.File != null ? s.Archivo3.FileName : ""));

                cfg.CreateMap<OfertaViewModelItem, Oferta>()
                 .ForMember(x => x.Archivo, d => d.MapFrom(s => s.ArchivoAdjunto != null && s.ArchivoAdjunto.File != null ? s.ArchivoAdjunto.File.FileName : ""))
                 .ForMember(x => x.Clase, d => d.MapFrom(s => String.Join(";", s.Clase)))
                 .ForMember(x => x.Cuota, d => d.MapFrom(s => String.Join(";", s.Cuota)))
                 .ForMember(x => x.Excluir, d => d.MapFrom(s => String.Join(";", s.Excluir)))
                 .ForMember(x => x.FechaAlta, d => d.MapFrom(s => s.FechaAlta))
                 .ForMember(x => x.FechaCierre, d => d.MapFrom(s => s.FechaCierre))
                 .ForMember(x => x.Imagen, d => d.MapFrom(s => s.Image != null && s.Image.File != null ? s.Image.File.FileName : ""))
                 .ForMember(x => x.Titulo, d => d.MapFrom(s => s.Titulo));

                cfg.CreateMap<BankAccountVM, BankAccount>();

                cfg.CreateMap<BoletinesVMItem, BoletinLanzamiento>()
                    .ForMember(x => x.Archivo, d => d.MapFrom(s => s.Archivo != null && s.Archivo.File != null ? s.Archivo.FileName : ""))
                    .ForMember(x => x.Imagen, d => d.MapFrom(s => s.Imagen != null && s.Imagen.File != null ? s.Imagen.FileName : ""))
                    .ForMember(x => x.TituloEn, d => d.MapFrom(s => s.Titulo.Translations[Language.EN.ToString()]))
                    .ForMember(x => x.TituloEs, d => d.MapFrom(s => s.Titulo.Translations[Language.ES.ToString()]))
                    .ForMember(x => x.TituloPr, d => d.MapFrom(s => s.Titulo.Translations[Language.BR.ToString()]));

                cfg.CreateMap<RevistaVMItem, Magazine>()
               .ForMember(x => x.TitleEn, d => d.MapFrom(s => s.Titulo.Translations[Language.EN.ToString()]))
               .ForMember(x => x.TitleEs, d => d.MapFrom(s => s.Titulo.Translations[Language.ES.ToString()]))
               .ForMember(x => x.TitlePr, d => d.MapFrom(s => s.Titulo.Translations[Language.BR.ToString()]))
               .ForMember(x => x.SubtitleEn, d => d.MapFrom(s => s.SubTitulo.Translations[Language.EN.ToString()]))
               .ForMember(x => x.SubtitleEs, d => d.MapFrom(s => s.SubTitulo.Translations[Language.ES.ToString()]))
               .ForMember(x => x.SubtitlePr, d => d.MapFrom(s => s.SubTitulo.Translations[Language.BR.ToString()]))
               .ForMember(x => x.Img, d => d.MapFrom(s => s.Image != null && s.Image.File != null ? s.Image.File.FileName : ""))
               .ForMember(x => x.File, d => d.MapFrom(s => s.File != null && s.File.File != null ? s.File.File.FileName : ""))
               .ForMember(x => x.Number, d => d.MapFrom(s => s.Number));

                #endregion

            });
            //config.AssertConfigurationIsValid();
            Mapper = config.CreateMapper();
        }
    }
}
