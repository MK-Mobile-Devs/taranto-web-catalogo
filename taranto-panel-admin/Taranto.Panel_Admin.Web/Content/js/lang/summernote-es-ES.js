(function($) {
  $.extend($.summernote.lang, {
    'es-ES': {
      font: {
        bold: 'Negrita',
        italic: 'Cursiva',
        underline: 'Subrayado',
        clear: 'Quitar estilo de fuente',
        height: 'Altura de l�nea',
        name: 'Fuente',
        strikethrough: 'Tachado',
        superscript: 'Super�ndice',
        subscript: 'Sub�ndice',
        size: 'Tama�o de la fuente'
      },
      image: {
        image: 'Imagen',
        insert: 'Insertar imagen',
        resizeFull: 'Redimensionar a tama�o completo',
        resizeHalf: 'Redimensionar a la mitad',
        resizeQuarter: 'Redimensionar a un cuarto',
        floatLeft: 'Flotar a la izquierda',
        floatRight: 'Flotar a la derecha',
        floatNone: 'No flotar',
        shapeRounded: 'Forma: Redondeado',
        shapeCircle: 'Forma: C�rculo',
        shapeThumbnail: 'Forma: Marco',
        shapeNone: 'Forma: Ninguna',
        dragImageHere: 'Arrastrar una imagen o texto aqu�',
        dropImage: 'Suelta la imagen o texto',
        selectFromFiles: 'Examinar',
        maximumFileSize: 'Tama�o m�ximo del archivo',
        maximumFileSizeError: 'Has superado el tama�o m�ximo del archivo.',
        url: 'URL de la imagen',
        remove: 'Eliminar imagen',
        original: 'Original'
      },
      video: {
        video: 'video',
        videoLink: 'Link del video',
        insert: 'Insertar video',
        url: 'URL del video',
        providers: '(YouTube, Vimeo)'
      },
      link: {
        link: 'Link',
        insert: 'Insertar link',
        unlink: 'Quitar link',
        edit: 'Editar',
        textToDisplay: 'Texto para mostrar',
        url: 'URL',
        openInNewWindow: 'Abrir en una nueva ventana'
      },
      table: {
        table: 'Tabla',
        addRowAbove: 'A�adir fila encima',
        addRowBelow: 'A�adir fila debajo',
        addColLeft: 'A�adir columna izquierda',
        addColRight: 'A�adir columna derecha',
        delRow: 'Borrar fila',
        delCol: 'Eliminar columna',
        delTable: 'Eliminar tabla'
      },
      hr: {
        insert: 'Insertar l�nea horizontal'
      },
      style: {
        style: 'Estilo',
        p: 'P�rrafo',
        blockquote: 'Cita',
        pre: 'C�digo',
        h1: 'T�tulo 1',
        h2: 'T�tulo',
        h3: 'Sub t�tulo',
        h4: 'T�tulo 4',
        h5: 'T�tulo 5',
        h6: 'T�tulo 6'
      },
      lists: {
        unordered: 'Lista desordenada',
        ordered: 'Lista ordenada'
      },
      options: {
        help: 'Ayuda',
        fullscreen: 'Pantalla completa',
        codeview: 'Ver c�digo HTML'
      },
      paragraph: {
        paragraph: 'Alineaci�n',
        outdent: 'Menos tabulaci�n',
        indent: 'M�s tabulaci�n',
        left: 'Alinear a la izquierda',
        center: 'Alinear al centro',
        right: 'Alinear a la derecha',
        justify: 'Justificar'
      },
      color: {
        recent: '�ltimo color',
        more: 'M�s colores',
        background: 'Color de fondo',
        foreground: 'Color de fuente',
        transparent: 'Transparente',
        setTransparent: 'Establecer transparente',
        reset: 'Restaurar',
        resetToDefault: 'Restaurar por defecto'
      },
      shortcut: {
        shortcuts: 'Atajos de teclado',
        close: 'Cerrar',
        textFormatting: 'Formato de texto',
        action: 'Acci�n',
        paragraphFormatting: 'Formato de p�rrafo',
        documentStyle: 'Estilo de documento',
        extraKeys: 'Teclas adicionales'
      },
      help: {
        'insertParagraph': 'Insertar p�rrafo',
        'undo': 'Deshacer �ltima acci�n',
        'redo': 'Rehacer �ltima acci�n',
        'tab': 'Tabular',
        'untab': 'Eliminar tabulaci�n',
        'bold': 'Establecer estilo negrita',
        'italic': 'Establecer estilo cursiva',
        'underline': 'Establecer estilo subrayado',
        'strikethrough': 'Establecer estilo tachado',
        'removeFormat': 'Limpiar estilo',
        'justifyLeft': 'Alinear a la izquierda',
        'justifyCenter': 'Alinear al centro',
        'justifyRight': 'Alinear a la derecha',
        'justifyFull': 'Justificar',
        'insertUnorderedList': 'Insertar lista desordenada',
        'insertOrderedList': 'Insertar lista ordenada',
        'outdent': 'Reducir tabulaci�n del p�rrafo',
        'indent': 'Aumentar tabulaci�n del p�rrafo',
        'formatPara': 'Cambiar estilo del bloque a p�rrafo (etiqueta P)',
        'formatH1': 'Cambiar estilo del bloque a H1',
        'formatH2': 'Cambiar estilo del bloque a H2',
        'formatH3': 'Cambiar estilo del bloque a H3',
        'formatH4': 'Cambiar estilo del bloque a H4',
        'formatH5': 'Cambiar estilo del bloque a H5',
        'formatH6': 'Cambiar estilo del bloque a H6',
        'insertHorizontalRule': 'Insertar l�nea horizontal',
        'linkDialog.show': 'Mostrar panel enlaces'
      },
      history: {
        undo: 'Deshacer',
        redo: 'Rehacer'
      },
      specialChar: {
        specialChar: 'CARACTERES ESPECIALES',
        select: 'Selecciona Caracteres especiales'
      }
    }
  });
})(jQuery);
