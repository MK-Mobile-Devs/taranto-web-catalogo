﻿
$(".tabs-idioma li").click(function (e) {
    var lang = e.target.getAttribute("href").substr(5, 2).toUpperCase();
    $(".language-div").hide();
    $(".lang-" + lang).show(); 
});

$(document).ready(function (e) {
    $('.selectpicker').selectpicker();
});