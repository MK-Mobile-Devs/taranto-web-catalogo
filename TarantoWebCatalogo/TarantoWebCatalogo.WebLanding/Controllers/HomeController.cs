﻿using System.Web.Mvc;

namespace TarantoWebCatalogo.WebLanding.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}