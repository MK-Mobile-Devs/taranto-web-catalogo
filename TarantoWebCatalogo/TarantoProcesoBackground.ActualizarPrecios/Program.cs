﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Newtonsoft.Json;
using TarantoWebCatalogo.Data.UnitOfWork;
using TarantoWebCatalogo.Services;
using TarantoWebCatalogo.Services.Pedidos;
using TarantoWebCatalogo.ViewModels.Pedidos;

namespace TarantoProcesoBackground.ActualizarPrecios
{
    static class Program
    {
        private static IUnitOfWork _unitOfWork;
        private static IPedidosService _pedidosService;


        static void Main()
        {
            AutoMapperServiceConfiguration.Configure();
            _unitOfWork = new UnitOfWork();
            var factory = new ServiceFactory(_unitOfWork);
            _pedidosService = factory.CrearPedidoService();

            ActualizarPrecios();

        }

        private static void ActualizarPrecios()
        {
            var pedidos = _pedidosService.ObtenerTodosLosArticulosDePedidosPendiente();

            foreach (var pedido in pedidos)
            {
                if (String.IsNullOrEmpty(pedido.Articulo)) continue;
                var url = "http://192.9.200.109:8080/catalogo/articulosParaCarrito?codigo=" + pedido.Articulo;
                var client = new WebClient();
                var response = client.DownloadString(url);
                var articuloActualizado = JsonConvert.DeserializeObject<List<PedidoDetail>>(response);
                if (!articuloActualizado.Any()) continue;
                _pedidosService.ActualizarPreciosDelPedidoPendiente(pedido.Articulo, articuloActualizado.FirstOrDefault().PrecioFinal, pedido.IdPedido, articuloActualizado.FirstOrDefault().Stock);
            }
        }
    }
}
