﻿namespace TarantoWebCatalogo.ViewModels.Usuarios.Api
{
    public class UsuarioLogeado
    {
        public string Usuario { get; set; }
        public string Descripcion { get; set; }
        public decimal LimiteDeCredito { get; set; }
        public string Estado { get; set; }
        public bool EsProveedor { get; set; }
        public string Clase { get; set; }
        public string Cupo { get; set; }
    }
}