﻿namespace TarantoWebCatalogo.ViewModels.PaginasInstitucionales
{
    public class PaginaInstitucional
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
    }
}