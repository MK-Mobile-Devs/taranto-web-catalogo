﻿using System.Collections.Generic;
using TarantoWebCatalogo.ViewModels.Novedades;
using TarantoWebCatalogo.ViewModels.Utilidades;

namespace TarantoWebCatalogo.ViewModels.Home
{
    public class HomeIndex
    {
        public HomeIndex()
        {
            ContenidoCarrousel = new List<ContenidoCarrousel>();
            Novedades = new List<Novedad>();
        }   

        public List<ContenidoCarrousel> ContenidoCarrousel { get; set; }
        public List<Novedad> Novedades { get; set; }
    }
}
