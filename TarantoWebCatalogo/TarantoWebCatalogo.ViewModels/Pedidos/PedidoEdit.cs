﻿using System.ComponentModel.DataAnnotations;

namespace TarantoWebCatalogo.ViewModels.Pedidos
{
    public class PedidoEdit
    {
        public string Articulo { get; set; }
        [Range(1, 999)]
        public int Cantidad { get; set; }
    }
}
