﻿namespace TarantoWebCatalogo.ViewModels.Pedidos
{
    public class PedidoProcesado
    {
        public int IdPedido { get; set; }
        public string Observacion { get; set; }
        public string Usuario { get; set; }
        public string Codigo { get; set; }
        public int Cantidad { get; set; }
    }
}
