﻿using System.ComponentModel.DataAnnotations;

namespace TarantoWebCatalogo.ViewModels.Pedidos
{
    public class PedidoCreate
    {
        public string Articulo { get; set; }
        public string Descripcion { get; set; }
        [Range(1, 999)]
        public int Cantidad { get; set; }
        public decimal PrecioUnitario { get; set; }
        public string Stock { get; set; }
    }
}