﻿using System.Collections.Generic;

namespace TarantoWebCatalogo.ViewModels.Pedidos
{
    public class PedidoDesdeExcel
    {
        public string Usuario { get; set; }
        public string Observaciones { get; set; }
        public List<PedidoDetalle> Articulos { get; set; }
    }
}