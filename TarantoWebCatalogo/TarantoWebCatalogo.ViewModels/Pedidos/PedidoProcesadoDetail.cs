﻿using System;

namespace TarantoWebCatalogo.ViewModels.Pedidos
{
    public class PedidoProcesadoDetail
    {
        public int Id { get; set; }
        public int? IdOracle { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string Origen { get; set; }
        public string Observacion { get; set; }
    }
}
