﻿namespace TarantoWebCatalogo.ViewModels.Pedidos
{
    public class PedidoExcel
    {
        public string Codigo { get; set; }
        public int Cantidad { get; set; }
    }
}
