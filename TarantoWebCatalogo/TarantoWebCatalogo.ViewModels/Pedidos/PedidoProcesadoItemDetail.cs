﻿using System;
namespace TarantoWebCatalogo.ViewModels.Pedidos
{
    public class PedidoProcesadoItemDetail
    {
        public string Articulo { get; set; }
        public string Descripcion { get; set; }
        public int Cantidad { get; set; }
    }
}
