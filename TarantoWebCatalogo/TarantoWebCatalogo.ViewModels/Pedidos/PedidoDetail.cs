﻿using System.ComponentModel.DataAnnotations;

namespace TarantoWebCatalogo.ViewModels.Pedidos
{
    public class PedidoDetail
    {
        public int IdPedido { get; set; }
        public string Usuario { get; set; }
        public decimal Total { get; set; }
        public string Articulo { get; set; }
        public string Descripcion { get; set; }
        public int Cantidad { get; set; }
        public decimal Subtotal { get; set; }
        public decimal PrecioUnitario { get; set; }
        public decimal PrecioFinal { get; set; }
        public string Stock { get; set; }
    }
}