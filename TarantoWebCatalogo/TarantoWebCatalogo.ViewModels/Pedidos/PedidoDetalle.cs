﻿using System.ComponentModel.DataAnnotations;

namespace TarantoWebCatalogo.ViewModels.Pedidos
{
    public class PedidoDetalle
    {
        public string Articulo { get; set; }
        public string Descripcion { get; set; }
        public int Cantidad { get; set; }
        public decimal PrecioUnitario { get; set; }
        public string Stock { get; set; }
    }
}