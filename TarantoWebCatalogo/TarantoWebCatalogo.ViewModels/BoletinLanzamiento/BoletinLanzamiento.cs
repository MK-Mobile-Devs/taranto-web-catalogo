﻿using System;

namespace TarantoWebCatalogo.ViewModels.BoletinLanzamiento
{
    public class BoletinLanzamiento
    {
        public int Id { get; set; }
        public string FechaCreacion { get; set; }
        public string TituloEs { get; set; }
        public string TituloEn { get; set; }
        public string TituloPr { get; set; }
        public string Imagen { get; set; }
        public string Archivo { get; set; }
    }
}
