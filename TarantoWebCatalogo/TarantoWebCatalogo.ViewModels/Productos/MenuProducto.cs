﻿namespace TarantoWebCatalogo.ViewModels.Productos
{
    public class MenuProducto
    {
        public int Id { get; set; }
        public int IdCategoria { get; set; }
        public string Categoria { get; set; }
        public string Titulo { get; set; }
        public string UrlTitulo { get; set; }
    }
}
