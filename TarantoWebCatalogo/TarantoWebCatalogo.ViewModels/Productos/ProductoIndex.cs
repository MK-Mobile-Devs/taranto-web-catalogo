﻿using System.Collections.Generic;

namespace TarantoWebCatalogo.ViewModels.Productos
{
    public class ProductoIndex
    {
        public ProductoIndex()
        {
            Producto = new ProductoDetalle();
            MenuProductos = new List<MenuProducto>();
        }

        public ProductoDetalle Producto { get; set; }
        public List<MenuProducto> MenuProductos { get; set; }
    }
}