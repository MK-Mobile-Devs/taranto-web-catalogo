﻿using System.Collections.Generic;
using System.Linq;

namespace TarantoWebCatalogo.ViewModels.Productos
{
    public class TopBarProductoView
    {
        public TopBarProductoView()
        {
            Productos = new List<MenuProducto>();
        }

        public List<MenuProducto> Productos { get; set; }

        public string Titulo => Productos.FirstOrDefault()?.Categoria ?? string.Empty;
    }
}