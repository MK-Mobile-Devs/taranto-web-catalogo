﻿namespace TarantoWebCatalogo.ViewModels.Productos
{
    public class ProductoDetalle
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public int IdCategoria { get; set; }
        public string CategoriaDetalle { get; set; }
    }
}
