﻿namespace TarantoWebCatalogo.ViewModels.ApiHelper
{
    public class GenericResponse
    {
        public string ErrorMessage { get; set; }
    }
}
