﻿
namespace TarantoWebCatalogo.ViewModels.Distribuidores
{
    public class TipoDeDistribuidor
    {
        public string CustomerNumber { get; set; }
        public string CustomerName { get; set; }
        //public int CodeInt { get; set; }
        public string Tipo { get; set; }
        public string Description { get; set; }

        public string DescriptionPor { get; set; }
        public string DescriptionEng { get; set; }

        //"CustomerNumber": "A905",
        //"CustomerName": "ABA FORD REPUESTOS S.R.L.",
        //"Tipo": "INT",
        //"Description": "Línea Ford",
        //"DescriptionPor": "Linha Ford",
        //"DescriptionEng": "Ford Line"


        public string Code { get; set; }
        public int CodeInt { get; set; }

        public string Desc { get; set; }
        public string Desc_POR { get; set; }
        public string Desc_ENG { get; set; }
    }
}
