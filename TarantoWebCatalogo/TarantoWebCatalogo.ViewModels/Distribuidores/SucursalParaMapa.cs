﻿using System.Collections.Generic;

namespace TarantoWebCatalogo.ViewModels.Distribuidores
{
    public class SucursalParaMapa
    {
        public double[,] position { get; set; }
        public int clienteId { get; set; }
        public int Tipo { get; set; }
        public string icon { get; set; }
    }
}
