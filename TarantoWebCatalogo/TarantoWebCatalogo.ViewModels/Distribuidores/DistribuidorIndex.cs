﻿using System.Collections.Generic;

namespace TarantoWebCatalogo.ViewModels.Distribuidores
{
    public class DistribuidorIndex
    {
        public DistribuidorIndex()
        {
            TiposDeDistribuidores = new List<TipoDeDistribuidor>();
            SucursalesParaMapa = new List<SucursalParaMapa>();
        }

        public List<SucursalParaMapa> SucursalesParaMapa { get; set; }
        public List<TipoDeDistribuidor> TiposDeDistribuidores { get; set; }

    }
}
