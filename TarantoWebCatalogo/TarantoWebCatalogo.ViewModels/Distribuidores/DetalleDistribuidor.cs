﻿namespace TarantoWebCatalogo.ViewModels.Distribuidores
{
    public class DetalleDistribuidor
    {
        public int CustomerId { get; set; }
        public string CustomerNumber { get; set; }
        public string CustomerName { get; set; }
        public string ZonaReparto { get; set; }
        public string Descripcion { get; set; }
        public string DescripcionPor { get; set; }
        public string DescripcionEng { get; set; }
        public string MetroInterior { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }
        public string Direccion { get; set; }
        public string Provincia { get; set; }
        public string Ciudad { get; set; }
        public string CodigoPostal { get; set; }
        public int IdSucursal { get; set; }
    }
}
