﻿namespace TarantoWebCatalogo.ViewModels.Distribuidores
{
    public class Sucursal
    {
        public int ClienteId { get; set; }
        public string CodigoCliente{ get; set; }        
        public double Latitud { get; set; }
        public double Longitud { get; set; }
        public int Tipo { get; set; }

        public string clienteCodigo { get; set; }

    }
}
