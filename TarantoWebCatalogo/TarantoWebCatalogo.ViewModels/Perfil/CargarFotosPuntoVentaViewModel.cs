﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TarantoWebCatalogo.ViewModels.Perfil
{
    public class CargarFotosPuntoVentaViewModel
    {
        public string Comentarios { get; set; }
        public string ImagenPrincipal { get; set; }
        public List<string> MoreImages { get; set; }
        public string ClienteId { get; set; }
    }
}
