﻿using Newtonsoft.Json;

namespace TarantoWebCatalogo.ViewModels.Perfil
{
    public class CambiarMisDatosViewModel
    {
        public string NombreDelComercio { get; set; }
        public string RazonSocial { get; set; }
        public string DireccionEntregaTaranto { get; set; }
        public string TelefonoContactoTaranto { get; set; }
        public string EmailContactoAdministrativo { get; set; }
        public string Sucursal { get; set; }
        public string DireccionDelComercio { get; set; }
        public string TelefonoDelComercio { get; set; }
        public string Email { get; set; }
        public string DiasYHorarios { get; set; }
        public string Comentarios { get; set; }

        public override string ToString()
        {
            /*$the_date = date("Y-m-d H:i:s");*/
            /*$info_serialized = base64_encode(serialize($info));*/
            /*$query = "INSERT INTO user_data_edit_request (the_date, id_user, info_serialized, img, more_img) VALUES ('".$the_date."', '".$_SESSION['extranet_tnto']['clienteId']."', '".$info_serialized."', '".$img."', '".implode(';',$more_img)."')";*/
            var infoSerialized = JsonConvert.SerializeObject(this);
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(infoSerialized));
            var objSerialized = System.Convert.ToBase64String(plainTextBytes);
            return objSerialized;
            // return base.ToString();
            
        }
    }
}
