﻿using System.Collections.Generic;

namespace TarantoWebCatalogo.ViewModels.Novedades
{
    public class NovedadesIndex
    {
        public NovedadesIndex()
        {
            UltimasNovedades = new List<Novedad>();
        }
        
        public List<Novedad> UltimasNovedades { get; set; }
    }
}
