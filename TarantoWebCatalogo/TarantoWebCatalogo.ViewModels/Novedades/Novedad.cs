﻿using System;

namespace TarantoWebCatalogo.ViewModels.Novedades
{
    public class Novedad
    {
        public int Id { get; set; }
        public System.DateTime Fecha { get; set; }
        public int IdCategoria { get; set; }
        public string Titulo { get; set; }
        public string Subtitulo { get; set; }
        public string Descripcion { get; set; }
        public string Imagen { get; set; }
        public string NombreCategoria { get; set; }
    }
}
