﻿using System.Collections.Generic;

namespace TarantoWebCatalogo.ViewModels.Novedades
{
    public class NovedadesDetalle
    {
        public NovedadesDetalle()
        {
            UltimasNovedades = new List<Novedad>();
        }
        
        public List<Novedad> UltimasNovedades { get; set; }
        public Novedad Novedad { get; set; }
    }
}
