﻿using System.Collections.Generic;
using TarantoWebCatalogo.ViewModels.PaginasInstitucionales;

namespace TarantoWebCatalogo.ViewModels.RecursosHumanos
{
    public class Index
    {
        public Index()
        {
            Busquedas = new List<Busqueda>();
            PaginaInstitucional = new PaginaInstitucional();
        }
        public List<Busqueda> Busquedas { get; set; }
        public PaginaInstitucional PaginaInstitucional { get; set; }

    }
}
