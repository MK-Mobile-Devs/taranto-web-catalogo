﻿namespace TarantoWebCatalogo.ViewModels.RecursosHumanos
{
    public class Busqueda
    {
        public int Id { get; set; }
        public string Funcion { get; set; }
        public string Equipo { get; set; }
        public string Ciudad { get; set; }
        public string Descripcion { get; set; }
    }
}
