﻿using System.Web;

namespace TarantoWebCatalogo.ViewModels.RecursosHumanos
{
    public class Cv
    {
        public int IdRhh { get; set; }
        public string Nombre { get; set; }
        public string Dni { get; set; }
        public string Nacionalidad { get; set; }
        public string Provincia { get; set; }
        public string Ciudad { get; set; }
        public string Direccion { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public string FechaDeNacimiento { get; set; }
        public string Sexo { get; set; }
        public string Mensaje { get; set; }
        public string nombreArchivo { get; set; }
        public HttpPostedFileBase Archivo { get; set; }
    }
}