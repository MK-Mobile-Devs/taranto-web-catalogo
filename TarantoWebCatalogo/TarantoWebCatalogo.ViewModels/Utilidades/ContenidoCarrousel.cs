﻿namespace TarantoWebCatalogo.ViewModels.Utilidades
{
    public class ContenidoCarrousel
    {
        public string Titulo { get; set; }
        public string SubTitulo { get; set; }
        public string Imagen { get; set; }
    }
}
