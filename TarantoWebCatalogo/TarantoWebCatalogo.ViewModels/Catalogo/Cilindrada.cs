﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TarantoWebCatalogo.ViewModels.Catalogo
{
    public class Cilindrada
    {
        public string Cilincod { get; set; }
        public string Cilindesc { get; set; }
        public int Cilindesdedis { get; set; }
        public int Cilinhastadis { get; set; }
        public int Cilindesde { get; set; }
        public int Cilinhasta { get; set; }
    }
}
