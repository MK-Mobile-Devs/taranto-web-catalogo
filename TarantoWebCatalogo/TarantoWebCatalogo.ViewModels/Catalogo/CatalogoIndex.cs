﻿using System.Collections.Generic;

namespace TarantoWebCatalogo.ViewModels.Catalogo
{
    public class CatalogoIndex
    {
        public CatalogoIndex()
        {
            Marcas = new List<Marca>();
            Combustibles = new List<Combustible>();
        }
        public List<Marca> Marcas { get; set; }
        public List<Combustible> Combustibles { get; set; }
    }
}
