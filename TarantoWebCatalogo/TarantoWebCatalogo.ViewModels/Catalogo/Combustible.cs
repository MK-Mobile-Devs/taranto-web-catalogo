﻿namespace TarantoWebCatalogo.ViewModels.Catalogo
{
    public class Combustible
    {
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
    }
}
