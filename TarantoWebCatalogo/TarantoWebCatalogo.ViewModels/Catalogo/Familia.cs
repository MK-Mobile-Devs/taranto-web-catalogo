﻿namespace TarantoWebCatalogo.ViewModels.Catalogo
{
    public class Familia
    {
        public long Fami_id { get; set; }
        
        public string Nombre { get; set; }
        
        public string Nombre_Por { get; set; }
        
        public string Nombre_Eng { get; set; }
        
        public bool Fami_Acti { get; set; }
        
        public string Tipo { get; set; }
    }
}
