﻿namespace TarantoWebCatalogo.ViewModels.Catalogo
{
    public class Marca
    {
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public int Orden { get; set; }
    }
}
