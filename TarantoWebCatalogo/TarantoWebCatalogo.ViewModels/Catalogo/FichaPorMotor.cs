﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TarantoWebCatalogo.ViewModels.Catalogo
{
    public class FichaPorMotor
    {
        public string FICOD { get; set; }
        public string FICOM { get; set; }
        public int FICAN { get; set; }
        public object NRORI { get; set; }
        public object FICANCARBO { get; set; }
        public object FIMAT { get; set; }
        public string FIDESC { get; set; }
        public int FIORDEN { get; set; }
    }
}
