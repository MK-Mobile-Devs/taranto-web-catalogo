﻿using System;

namespace TarantoWebCatalogo.ViewModels.ListaDePrecios
{
    public class ListaDePrecio
    {
        public int Id { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string Archivo1 { get; set; }
        public string NombreArchivo1 { get; set; }
        public string Archivo2 { get; set; }
        public string NombreArchivo2 { get; set; }
        public string Archivo3 { get; set; }
        public string NombreArchivo3 { get; set; }
    }
}
