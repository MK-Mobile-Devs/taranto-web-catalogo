﻿using System;
using System.Web.Mvc;
using TarantoWebCatalogo.Services.PaginasInstitucionales;
using TarantoWebCatalogo.Services.RecursosHumanos;
using TarantoWebCatalogo.Web.helpers.Enums;
using TarantoWebCatalogo.ViewModels.RecursosHumanos;
using TarantoWebCatalogo.Services.Core;
using System.Net.Mail;
using System.IO;

namespace TarantoWebCatalogo.Web.Controllers
{
    public partial class PaginaInstitucionalController : BaseController
    {
        private readonly IPaginasInstitucionalesService _paginasInstitucionalesService;
        private readonly IRecursosHumanosService _recursosHumanosService;
        private readonly IEnvioMailService _envioMailService;

        public PaginaInstitucionalController()
        {
            _paginasInstitucionalesService = Factory.CrearPaginasInstitucionalesService();
            _recursosHumanosService = Factory.CrearRecursosHumanosService();
            _envioMailService = Factory.CrearEnvioMailService();
        }

        public virtual ActionResult Index(int id)
        {
            var idioma = Session["Idioma"].ToString();

            if (id == (int)PaginaInstitucionales.Rrhh)
            {
                return RedirectToAction(MVC.PaginaInstitucional.RecursosHumanosIndex(idioma, id, null));
            }

            var paginaInstitucional =
                _paginasInstitucionalesService.ObtenerPaginaPorIdiomaySeccion(idioma, id);
            if (paginaInstitucional == null )
            {
                return RedirectToAction(MVC.Home.Index());
            }

            if (id == (int)PaginaInstitucionales.CalidadSeguridadMedioAmbiente){
                ViewBag.mostrarReclamo = true;
            }
            
            return View(paginaInstitucional);
        }

        public virtual ActionResult RecursosHumanosIndex(string idioma, int id, string mensaje)
        {
            ViewBag.Mensaje = mensaje;
            return View(new Index
                {
                    Busquedas = _recursosHumanosService.ObtenerTodasPorIdioma(idioma),
                    PaginaInstitucional = _paginasInstitucionalesService.ObtenerPaginaPorIdiomaySeccion(idioma, id)
                }
            );
        }

        public void EnviarEmailReclamo()
        {
            //_envioMailService.SendEmailTwilio("rrhh@taranto.com.ar", subject, body);

        }


        public virtual ActionResult AgregarCv(Cv cv)
        {

            string mensaje = "";
            try
            {
                _recursosHumanosService.Agregar(cv.IdRhh, cv.Nombre, cv.Dni, cv.Nacionalidad, cv.Provincia, cv.Ciudad, cv.Direccion
                , cv.Email, cv.Telefono, cv.FechaDeNacimiento, cv.Sexo, cv.Mensaje ?? "", cv.Archivo.FileName);

                var attachment = new Attachment(cv.Archivo.InputStream, cv.Archivo.FileName);

                var folder = Server.MapPath("~/media/cv");
                var urlPdf = "http://www.taranto.com.ar/media/cv/" + cv.Archivo.FileName.Replace(" ", "%20");
                cv.Archivo.SaveAs(Path.Combine(folder, cv.Archivo.FileName));


                var subject = "Taranto - Postulantes RRHH";
                mensaje =   "Nombre y Apellido: " + cv.Nombre + "<br/>" +
                            "DNI: " + cv.Dni + "<br/>" +
                            "Nacionalidad: " + cv.Nacionalidad + "<br/>" +
                            "Provincia: " + cv.Provincia + "<br/>" +
                            "Localidad: " + cv.Ciudad + "<br/>" +
                            "Dirección: " + cv.Direccion + "<br/>" +
                            "Email: " + cv.Email + "<br/>" +
                            "Tel: " + cv.Telefono + "<br/>" +
                            "Fecha de Naciemiento: " + cv.FechaDeNacimiento + "<br/>" +
                            "Sexo: " + cv.Sexo + "<br/>" +
                            "Comentario: " + cv.Mensaje + "<br/>" +
                            "CV Adjunto: " + urlPdf;

                var body = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" +
                            "<html xmlns =\"http://www.w3.org/1999/xhtml\">" +
                            "<head>"+  
                            "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />" +
                            "<title></title>" +
                            "</head>" +
                            "<body style=\"margin:0; padding:0;\">" +
                            "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse: collapse; border-spacing: 0;\">" +
                            "<tbody>" +
                                "<tr height=\"10\" bgcolor =\"#FFFFFF\" > " +
                                "<td height=\"10\" colspan=\"3\"></td>" +
                                "</tr>" +
                                        "<tr bgcolor=\"#FFFFFF\">" +
                                "	<td width=\"2%\"></td>" +

                                "	<td width=\"96%\" align=\"left\" valign=\"middle\" style=\"color:#000000;\">" +
                                "		<img src=\"http://www.taranto.com.ar/images/taranto.png\" alt=\"Taranto\" width=\"270\" height=\"110\" border=\"0\" style=\"width:270px; height:110px;\" />" +
                                "	</td>" +
                                "	<td width=\"2%\"></td>" +
                                "</tr>" +
                                "<tr height=\"10\" bgcolor=\"#FFFFFF\">" +
                                "	<td height=\"10\" colspan=\"3\"></td>" +
                                "</tr>		" +
                                "<tr height=\"10\" bgcolor=\"#FFFFFF\">" +
                                "	<td height=\"10\"></td>" +
                                "</tr>" +
                                "<tr bgcolor=\"#FFFFFF\">" +
                                "	<td width=\"2%\"></td>" +
                                "	<td width=\"96%\" align=\"left\" valign=\"middle\">" +
                                 "		<h3 style=\"font-family:Arial, Helvetica, sans-serif; font-size:17px; font-weight:bold; line-height:24px; color:#333; padding:0; margin:10px 0;\">" + subject + "</h3>" +
                                "		<p style=\"font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:30px; color:#333; padding:0; margin:5px 0;\">" + mensaje + "</p>" +
                                "	</td>" +
                                "	<td width=\"2%\"></td>" +
                                "</tr>" +
                                "<tr height=\"30\" bgcolor=\"#FFFFFF\">" +
                                "	<td height=\"30\"></td>" +
                                "</tr>" +
                            "</tbody>" +
                            "</table>" +
                            "</body>" +
                            "</html>";



                //_envioMailService.SendEmail("rrhh@taranto.com.ar", subject, body, attachment, true);
                _envioMailService.SendEmailTwilio("rrhh@taranto.com.ar", subject, body);

                mensaje = "Curriculum enviado exitosamente";
            }
            catch (Exception e)
            {
                mensaje = "Ocurrio un problema, por favor vuelva a intentar mas tarde";
            }

            var idioma = Session["Idioma"].ToString();
            return RedirectToAction(MVC.PaginaInstitucional.RecursosHumanosIndex(idioma, (int)PaginaInstitucionales.Rrhh, mensaje));
        }
    }
}