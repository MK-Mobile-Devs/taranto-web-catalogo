﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using ClosedXML.Excel;
using log4net;
using Newtonsoft.Json;
using TarantoWebCatalogo.Services.BoletinLanzamiento;
using TarantoWebCatalogo.Services.Core;
using TarantoWebCatalogo.Services.ListaDePrecios;
using TarantoWebCatalogo.Services.Pedidos;
using TarantoWebCatalogo.ViewModels.Pedidos;
using TarantoWebCatalogo.Web.helpers;

namespace TarantoWebCatalogo.Web.Controllers
{
    public partial class CatalogoController : BaseController
    {
        private readonly IPedidosService _pedidosService;
        private readonly ISeguridadService _seguridadService;
        private readonly IListaDePrecioService _listaDePrecioService;
        private readonly IBoletinLanzamientoService _boletinLanzamientoService;
        private static readonly ILog Log = LogManager.GetLogger("LogFileAppender");

        public CatalogoController()
        {
            _pedidosService = Factory.CrearPedidoService();
            var webConfigHelper = new WebConfigHelper();
            _seguridadService = Factory.CrearSeguridadService(webConfigHelper.ApiUrl);
            _listaDePrecioService = Factory.CrearListaDePrecioService();
            _boletinLanzamientoService = Factory.CrearBoletinLanzamientoService();
        }

        public virtual ActionResult Index()
        {

            ViewBag.EsCliente = false;
            if (_seguridadService.HayUsuarioLogueado)
            {
                ViewBag.EsCliente = !_seguridadService.UsuarioLogueadoEsProveedor;
                ViewBag.ApiLocalUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["ApiLocalUrl"];
            }
            ViewBag.ApiUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["ApiUrl"];
            return View();
        }

        public virtual ActionResult ListaDePrecios()
        {
            var listaDePrecios = _listaDePrecioService.ObtenerTodas();
            return View(listaDePrecios);
        }

        public virtual ActionResult BoletinLanzamiento()
        {
            var boletines = _boletinLanzamientoService.ObtenerTodos();
            return View(boletines);
        }

        public virtual ActionResult Ofertas()
        {
            var boletines = _boletinLanzamientoService.ObtenerTodos();
            return View(boletines);
        }


        public virtual ActionResult CarritoDeComprasIndex()
        {
            if (!_seguridadService.UsuarioLogueadoEsProveedor)
            {
                var pedido = _pedidosService.ObtenerPorUsuario(_seguridadService.UsuarioLogueadoCodigo);
                ViewBag.ApiLocalUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["ApiLocalUrl"];
                ViewBag.BaseUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["BaseUrl"];
                return View(pedido);
            }
            return RedirectToAction(MVC.Home.Perfil());
        }

        [ChildActionOnly]
        public virtual ActionResult MostrarProducto()
        {
            var pedido = _pedidosService.ObtenerPorUsuario(_seguridadService.UsuarioLogueadoCodigo);
            return PartialView("_DetallePedido", pedido);
        }
        
        [HttpGet]
        public virtual ActionResult CrearPedido()
        {
            if (_seguridadService.UsuarioLogueadoEsProveedor)
            {
                return RedirectToAction(MVC.Catalogo.Index());
            }
            return View(new PedidoCreate{ Cantidad = 1});
        }

        [HttpPost]
        public virtual ActionResult CrearPedido(PedidoCreate pedido)
        {
            _pedidosService.Agregar(_seguridadService.UsuarioLogueadoCodigo,
                pedido.Articulo, pedido.Descripcion, pedido.Cantidad, pedido.PrecioUnitario, pedido.Stock, false, null);

            return RedirectToAction(MVC.Catalogo.CarritoDeComprasIndex());
        }
        
        public virtual ActionResult EliminarPedido(string articulo, int idPedido)
        {

            _pedidosService.Eliminar(_seguridadService.UsuarioLogueadoCodigo, articulo, idPedido);
            return RedirectToAction(MVC.Catalogo.CarritoDeComprasIndex());
            
        }

        public virtual ActionResult Eliminar(int idPedido)
        {
            _pedidosService.Eliminar(_seguridadService.UsuarioLogueadoCodigo, idPedido);
            return RedirectToAction(MVC.Catalogo.CarritoDeComprasIndex());
        }

        public virtual ActionResult PedidoPorExcel(int processStatus)
        {
            if (!_seguridadService.HayUsuarioLogueado || _seguridadService.UsuarioLogueadoEsProveedor)
            {
                return RedirectToAction(MVC.Catalogo.Index());
            }

            ViewBag.ProcessStatus = processStatus;

            return View();
        }

        public virtual ActionResult Pedidos()
        {
            if (!_seguridadService.HayUsuarioLogueado)
            {
                return RedirectToAction(MVC.Home.Perfil());
            }

            var pedido = _pedidosService.ObtenerPedidosProcesadosPorUsuario(_seguridadService.UsuarioLogueadoCodigo);
            return View(pedido);
        }

        public virtual ActionResult PedidoDetalleArticulos(int idPedido, string fecha, string observacion, string origen, int idOracle)
        {
            var pedido = _pedidosService.ObtenerDetallePedidosProcesadosPorIdPedido(idPedido);
            ViewBag.Fecha = fecha;
            ViewBag.Observacion = observacion;
            ViewBag.Origen = origen;
            ViewBag.IdOracle = idOracle;
            return View(pedido);
        }

        [HttpPost]
        public virtual ActionResult PedidoPorExcel(HttpPostedFileBase archivoExcel)
        {
             if (ValidarPedidoExcel(archivoExcel, out var actionResult)) return actionResult;

            const string mensaje = "El archivo seleccionado no es un archivo de Excel válido.";

            ViewBag.Mensaje = mensaje;
            return View();
        }

        private bool ValidarPedidoExcel(HttpPostedFileBase archivoExcel, out ActionResult actionResult)
        {
            if (archivoExcel?.FileName == null)
            {
                ViewBag.Mensaje = "Debe seleccionar un archivo de tipo Excel";
                {
                    actionResult = View();
                    return true;
                }
            }

            if (archivoExcel.FileName.EndsWith("xlsx"))
            {
                string mensaje = null;
                try
                {
                    using (var workBook = new XLWorkbook(archivoExcel.InputStream))
                    {
                        var workSheet = workBook.Worksheet(1);
                        var dt = new DataTable();
                        var firstRow = true;
                        if (workSheet.Rows().Count() > 70) //Tope elegido por el cliente
                        {
                            mensaje = "El Excel seleccionado no debe superar las 70 líneas";
                        }
                        else if (!workSheet.Rows().Any())
                        {
                            mensaje = "El Excel seleccionado esta vacío";
                        }
                        else
                        {

                            foreach (var row in workSheet.Rows())
                            {
                                if (firstRow)
                                {
                                    foreach (var cell in row.Cells())
                                    {
                                        dt.Columns.Add(cell.Value.ToString());
                                    }

                                    firstRow = false;
                                }

                                dt.Rows.Add();
                                var i = 0;
                                foreach (var cell in row.Cells())
                                {
                                    dt.Rows[dt.Rows.Count - 1][i] = cell.Value.ToString();
                                    i++;
                                }
                            }

                            var pedidos = new List<PedidoExcel>();
                            var cargarPedido = true;
                            for (var i = 0; i < dt.Rows.Count; i++)
                            {
                                for (var j = 0; j < 1; j++)
                                {
                                    if (dt.Rows[i].ItemArray[1].ToString().Trim().IsInt())
                                    {
                                        var item = new PedidoExcel
                                        {
                                            Codigo = dt.Rows[i].ItemArray[0].ToString().Trim().ToUpper(), //Replace(".", "").Trim(),
                                            Cantidad = int.Parse(dt.Rows[i].ItemArray[1].ToString().Replace(".", "").Trim())
                                        };
                                        pedidos.Add(item);
                                        mensaje = "El archivo " + archivoExcel.FileName + " se cargo correctamente";
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(dt.Rows[i].ItemArray[1].ToString().Trim()))
                                        {
                                            mensaje = $"La columna B (cantidad) contiene caracteres inválidos ({dt.Rows[i].ItemArray[1].ToString().Trim()}) en la fila {i}, corríjalo y vuelva a intentar.";
                                            cargarPedido = false;
                                            break;
                                        }                                    }
                                }
                            }

                            if (cargarPedido)
                            {
                                var urlApiV3 = System.Web.Configuration.WebConfigurationManager.AppSettings["ApiUrl"];
                                var pedidoCargado = new List<PedidoDetail>();

                                foreach (var pedido in pedidos)
                                {
                                    if (string.IsNullOrEmpty(pedido.Codigo)) continue;
                                    var url = urlApiV3 + "catalogo/articulosParaCarrito?codigo=" + pedido.Codigo;
                                    //Log.Debug(url);
                                    var client = new WebClient();
                                    var response = client.DownloadString(url);
                                    var articuloActualizado = JsonConvert.DeserializeObject<List<PedidoDetail>>(response);

                                    if (!articuloActualizado.Any())
                                    {
                                        //Log.Debug("No se encuentra el artículo " + pedido.Codigo);
                                        continue;
                                    }
                                    var articulo = articuloActualizado.First();

                                    var item = new PedidoDetail
                                    {
                                        Articulo = pedido.Codigo.ToUpper(),
                                        Cantidad = pedido.Cantidad,
                                        Descripcion = articulo.Descripcion,
                                        IdPedido = 0,
                                        PrecioFinal = articulo.PrecioFinal,
                                        PrecioUnitario = articulo.PrecioUnitario,
                                        Stock = articulo.Stock,
                                        Subtotal = articulo.Subtotal,
                                        Total = 0,
                                        Usuario = _seguridadService.UsuarioLogueadoCodigo
                                    };
                                    pedidoCargado.Add(item);
                                }
                                {
                                    ViewBag.ApiLocalUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["ApiLocalUrl"];
                                    actionResult = View(pedidoCargado);
                                    return true;
                                }

                            }
                        }
                    }
                }
                catch
                {
                    mensaje = "Formato de archivo inválido. Por favor, verifique el archivo Excel.";
                }

                ViewBag.Mensaje = mensaje;
                {
                    actionResult = View();
                    return true;
                }
            }

            actionResult = null;
            return false;
        }
    }
}