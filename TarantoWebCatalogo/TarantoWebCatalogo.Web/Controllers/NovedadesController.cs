﻿using System.Web.Mvc;
using TarantoWebCatalogo.Services.Novedades;
using TarantoWebCatalogo.ViewModels.Novedades;
using TarantoWebCatalogo.Web.helpers;

namespace TarantoWebCatalogo.Web.Controllers
{
    public partial class NovedadesController : BaseController
    {
        private readonly INovedadesService _novedadesService;

        public NovedadesController()
        {
            _novedadesService = Factory.CrearNovedadesService();
        }

        public virtual ActionResult Index(int? idCategoria)
        {
            var categoriaId = idCategoria ?? -1;
            ViewBag.BaseUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["BaseUrl"];
            ViewBag.WebAdminUri = System.Web.Configuration.WebConfigurationManager.AppSettings["WebAdminUri"];
            ViewBag.IdCategoria = new SelectList(SelectListHelper.AgregarItemInicial(
                _novedadesService.ObtenerCategoriasParaDropdownPorIdioma(Session["Idioma"].ToString()), 
                "Seleccione una categoría", -1), "Id", "Name");
            return View(CrearNovedadIndex(Session["Idioma"].ToString(), categoriaId));
        }

        public virtual ActionResult Detalle(int idNovedad)
        {
            ViewBag.BaseUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["BaseUrl"];
            ViewBag.WebAdminUri = System.Web.Configuration.WebConfigurationManager.AppSettings["WebAdminUri"];
            return View(CrearNovedadDetalle(Session["Idioma"].ToString(), idNovedad));
        }

        #region Private

        public NovedadesIndex CrearNovedadIndex(string idioma, int idCategoria)
        {
            return new NovedadesIndex
            {
                UltimasNovedades = _novedadesService.ObtenerUltimasPorIdioma(idioma)
            };
        }

        public NovedadesDetalle CrearNovedadDetalle(string idioma, int idNovedad)
        {
            var novedad = _novedadesService.ObtenerPorId(idioma, idNovedad);
            return new NovedadesDetalle
            {
                UltimasNovedades = _novedadesService.ObtenerUltimasPorCategoria(idioma, novedad.IdCategoria, novedad.Id),
                Novedad = novedad
            };
        }

        #endregion
    }
}