﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;
using TarantoWebCatalogo.ViewModels.Distribuidores;

namespace TarantoWebCatalogo.Web.Controllers
{
    public partial class DistribuidoresController : BaseController
    {
        public virtual async Task<ActionResult> Index()
        {
            var dto = new DistribuidorIndex
            {
                SucursalesParaMapa = await Sucursales(),
                TiposDeDistribuidores = await Tipos()
            };
            
            ViewBag.BaseUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["BaseUrl"];
            ViewBag.ApiUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["ApiLocalUrl"];
            ViewBag.ApiUrlFabio = System.Web.Configuration.WebConfigurationManager.AppSettings["ApiUrlFabio"];
            return View(dto);
        }

        private static async Task<List<SucursalParaMapa>> Sucursales()
        {
            using (var client = new HttpClient())
            {
                var urlApiV3 = System.Web.Configuration.WebConfigurationManager.AppSettings["ApiUrl"];
                var urlApiFabio = System.Web.Configuration.WebConfigurationManager.AppSettings["ApiUrlFabio"];
                //var content = await client.GetStringAsync($"{urlApiV3}distribuidores/sucursales");
                var content_old = await client.GetStringAsync($"{urlApiFabio}serv.asmx/GetSucursalesAll");
                var sucursales = JsonConvert.DeserializeObject<List<Sucursal>>(content_old);
                
                var sucursalesParaMapa = sucursales
                    .Select(t => new SucursalParaMapa
                    {
                        position = new[,] { { t.Latitud, t.Longitud } },
                        clienteId = t.ClienteId,
                        Tipo = t.Tipo,
                        icon = "images/maker.png"
                    }).ToList();
            
                return sucursalesParaMapa;
            }
        }

        private static async Task<List<TipoDeDistribuidor>> Tipos()
        {
            var urlApiV3 = System.Web.Configuration.WebConfigurationManager.AppSettings["ApiUrl"];
            using (var client = new HttpClient())
            {
                var urlApiFabio = System.Web.Configuration.WebConfigurationManager.AppSettings["ApiUrlFabio"];
                //var content = await client.GetStringAsync($"{urlApiV3}distribuidores/tipos");
                var content_old = await client.GetStringAsync($"{urlApiFabio}serv.asmx/GetTiposDistribuidores");
                var result = JsonConvert.DeserializeObject<List<TipoDeDistribuidor>>(content_old);
                var resultDistinct = result.GroupBy(a => a.Description).Select(group => group.First()).ToList();
                //var resultDistinctTipo = result.GroupBy(a => a.Tipo).ToList();
                return result;
            }
        }
    }
}