﻿using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TarantoWebCatalogo.Services.Core;
using TarantoWebCatalogo.Services.Perfil;
using TarantoWebCatalogo.ViewModels.Perfil;
using TarantoWebCatalogo.Web.helpers;

namespace TarantoWebCatalogo.Web.Controllers
{
    public partial class PerfilController : BaseController
    {
        private readonly IEnvioMailService _envioMailService;
        private readonly ISeguridadService _seguridadService;
        private readonly IPerfilService _perfilService;
        private static readonly ILog Log = LogManager.GetLogger("LogFileAppender");

        public PerfilController()
        {
            _envioMailService = Factory.CrearEnvioMailService();
            var webConfigHelper = new WebConfigHelper();
            _seguridadService = Factory.CrearSeguridadService(webConfigHelper.ApiUrl);
            _perfilService = Factory.CrearPerfilService();
        }

        public virtual ActionResult DatosBancarios()
        {
            return View();
        }

        public virtual ActionResult CambiarContrasena()
        {
            ViewBag.ApiUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["ApiUrl"];
            return View();
        }
        public virtual ActionResult FotoPuntoDeVenta()
        {
            var data = _perfilService.ObtenerEditarMisDatos(_seguridadService.UsuarioLogueadoCodigo);
            var enabled_to_edit = true;
            var last_request = false;
            if (data != null)
            {
                if (data.received == 0)
                {
                    enabled_to_edit = false;
                }
                else
                {
                    last_request = data.received == 1;
                }
            }
            ViewBag.LastRequest = last_request;
            ViewBag.EnableToEdit = enabled_to_edit;
            ViewBag.ApiUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["ApiUrl"];
            return View();
        }

        [HttpPost]
        public void CargarFotosPuntoVenta(string clienteId, string Comentarios, HttpPostedFileBase ImagenPrincipal, HttpPostedFileBase[] MoreImages)
        {
            CargarFotosPuntoVentaViewModel viewModel = new CargarFotosPuntoVentaViewModel();
            var filePrincipal = Request.Files[0];
            viewModel.Comentarios = Comentarios;
            viewModel.ClienteId = clienteId;
            viewModel.ImagenPrincipal = GuardarImagen(ImagenPrincipal);
            if (MoreImages != null)
            {
                viewModel.MoreImages = new List<string>();
                foreach (var img in MoreImages)
                {
                    viewModel.MoreImages.Add(GuardarImagen(img));
                }
            }
            _perfilService.CargarFotoPuntoVenta(viewModel);
        }

        private string GuardarImagen(HttpPostedFileBase file)
        {
            try
            {
                /*ALTER TABLE UserDataEditRequest
                  ALTER COLUMN MoreImg NVARCHAR(200) [NULL | NOT NULL]*/
                var extensionesPermitidas = new List<string>() { ".jpg", ".jpeg" };
                if (file.ContentLength == 0) throw new Exception("No content length");
                Random random = new Random();
                var extension = Path.GetExtension(file.FileName);
                if (!extensionesPermitidas.Contains(extension)) throw new Exception("Solo se admiten jpg y jpeg");
                string newName = random.Next().ToString();
                var image = Image.FromStream(file.InputStream, true, true);
                var resizedImage = ResizeImage(image, new Size(800, 600));
                var fileName = Server.MapPath("~/images/media") + "/"+ newName + extension;
                using (MemoryStream memory = new MemoryStream())
                {
                    using (FileStream fs = new FileStream(fileName, FileMode.Create, FileAccess.ReadWrite))
                    {
                        image.Save(memory, ImageFormat.Jpeg);
                        byte[] bytes = memory.ToArray();
                        fs.Write(bytes, 0, bytes.Length);
                    }
                }
                
                return fileName;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public static Image ResizeImage(Image imgToResize, Size size)
        {
            return (Image)(new Bitmap(imgToResize, size));
        }


        [HttpPost]
        public bool CambiarContrasena(string clienteId, string oldPassword, string newPassword)
        {
            try
            {
                //obtener resultado de ultima edicion
                var dataUltimaEdicion = _perfilService.ObtenerEditarMisDatos(_seguridadService.UsuarioLogueadoCodigo);
                var enabled_to_edit = true;
                var last_request = false;
                if (dataUltimaEdicion != null)
                {
                    if (dataUltimaEdicion.received == 0)
                    {
                        enabled_to_edit = false;
                    }
                    else
                    {
                        last_request = dataUltimaEdicion.received == 1;
                    }
                }
                ViewBag.LastRequest = last_request;
                ViewBag.EnableToEdit = enabled_to_edit;
                
                var url = $"{System.Web.Configuration.WebConfigurationManager.AppSettings["ApiUrlFabio"]}serv.aspx/CambioPassword";
                
                using (WebClient client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.ContentType] = "application/json";
                    var json = JsonConvert.SerializeObject(new { clienteId = clienteId, oldPassword = oldPassword.Replace("'", "\\'"), newPassword = newPassword.Replace("'", "\\'") });
                    
                    string response = client.UploadString(url, "POST", json);
                    dynamic data = JsonConvert.DeserializeObject(response);
                    var ws = data["d"][0];

                    if (ws["resultado"] == false)
                    {
                        ViewBag.Mensaje = "Error";
                        return false;
                    }
                    
                    _seguridadService.Logout();
                    ViewBag.Mensaje = "Por favor ingrese nuevamente.";
                    return true;
                }
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
                throw new Exception("Ocurrio un error al cambiar contrasena");
            }

        }

        public virtual ActionResult EditarMisDatos()
        {
            //obtener resultado de ultima edicion
            var data = _perfilService.ObtenerEditarMisDatos(_seguridadService.UsuarioLogueadoCodigo);
            var enabled_to_edit = true;
            var last_request = false;
            if (data != null)
            {
                if (data.received == 0)
                {
                    enabled_to_edit = false;
                }
                else
                {
                    last_request = data.received == 1;
                }
            }
            ViewBag.LastRequest = last_request;
            ViewBag.EnableToEdit = enabled_to_edit;
            ViewBag.ApiUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["ApiUrl"];
            return View();
        }

        [HttpPost]
        public virtual ActionResult CambiarMisDatos(CambiarMisDatosViewModel cambiarMisDatosViewModel)
        {
            var infoSerialized = JsonConvert.SerializeObject(cambiarMisDatosViewModel);
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(infoSerialized));
            var objSerialized = System.Convert.ToBase64String(plainTextBytes);
            var id = _perfilService.CambiarMisDatosInfoSerialized(_seguridadService.UsuarioLogueadoCodigo, objSerialized);
            try
            {
                EnviarEmailConfirmacion(id);
                _perfilService.ActualizarMailEnviado(id);
            }
            catch 
            {
                ViewBag.ConfirmationMessage = "No se pudo enviar el email.";
                return this.EditarMisDatos();
            }
            ViewBag.ConfirmationMessage = "Esta información queda sujeto a confirmación del área administración.";
            return this.EditarMisDatos();
        }

        private void EnviarEmailConfirmacion(decimal id)
        {
            var smtpClient = new SmtpClient("smtp.gmail.com")
            {
                Port = 587,
                Credentials = new NetworkCredential("pedidostaranto@gmail.com", "taranto2016"),
                EnableSsl = true,
            };
            var mailMessage = new MailMessage
            {
                From = new MailAddress("pedidostaranto@gmail.com"),
                Subject = "Solicitud cambios en Cliente " + _seguridadService.UsuarioLogueadoCodigo + " Informar a CEAC",
                Body = "Nueva solicitud de edici&oacute;n de datos<br> Cliente: " + _seguridadService.UsuarioLogueadoCodigo + "- " + _seguridadService.NombreUsuario + "<br>" +
                "Por favor ingresa al <a href='www.taranto.com.ar/panel_admin' target='_blank'>Panel admin</a> de la web para ver m&aacute;s< br >",
                IsBodyHtml = true,
            };
            mailMessage.To.Add("pedidosweb@taranto.com.ar");

            smtpClient.Send(mailMessage);
        }

        public virtual ActionResult CuentaCorriente(string mensaje)
        {
            ViewBag.Mensaje = mensaje;
            ViewBag.ApiUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["ApiUrl"];
            return View();
        }
        public virtual ActionResult CuentaCorrienteDetalle(string tipoMovimiento, string tipoComprobante, int numeroComprobante)
        {
            ViewBag.TipoMovimiento = tipoMovimiento;
            ViewBag.TipoComprobante = tipoComprobante;
            ViewBag.NumeroComprobante = numeroComprobante;
            ViewBag.ApiUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["ApiUrl"];
            return View();
        }

        public virtual ActionResult CuentaCorrienteInformarPago(string comentario, HttpPostedFileBase archivo)
        {
            try
            {
                var mensaje = "Nuevo comprobante Cuenta Corriente" + Environment.NewLine +
                              "Cliente: " + _seguridadService.UsuarioLogueadoCodigo + " - " +
                              _seguridadService.NombreUsuario + Environment.NewLine +
                              "Mensaje: " + comentario + Environment.NewLine;

                var attachment = new Attachment(archivo.InputStream, archivo.FileName);
                _envioMailService.SendEmail("pedidosweb@taranto.com.ar", "Nuevo comprobante CTA CTE", mensaje, attachment, false);
                return RedirectToAction(MVC.Perfil.CuentaCorriente("Mail enviado con éxito"));
            }
            catch (Exception)
            {
                return RedirectToAction(MVC.Perfil.CuentaCorriente("El mail no se pudo enviar"));
            }

        }

        public virtual ActionResult Sugerencias()
        {
            return View();
        }
    }
}