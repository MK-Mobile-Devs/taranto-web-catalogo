﻿using System.Globalization;
using System.Threading;
using System.Web.Mvc;
using System.Web.Routing;
using TarantoWebCatalogo.Data.UnitOfWork;
using TarantoWebCatalogo.Services;

namespace TarantoWebCatalogo.Web.Controllers
{
    public partial class BaseController : Controller
    {
        public readonly ServiceFactory Factory;

        public BaseController()
            : this(new UnitOfWork())
        {

        }

        public BaseController(IUnitOfWork unitOfWork)
        {
            Factory = new ServiceFactory(unitOfWork);
        }

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);

            if (System.Web.HttpContext.Current.Session["Idioma"] == null)
            {
                System.Web.HttpContext.Current.Session["Idioma"] = "es";
            }

            var idioma = System.Web.HttpContext.Current.Session["Idioma"].ToString();
            var cInf = new CultureInfo(idioma + "-" + idioma.ToUpper(), true);
            Thread.CurrentThread.CurrentCulture = cInf;
            Thread.CurrentThread.CurrentUICulture = cInf;
        }
    }
}