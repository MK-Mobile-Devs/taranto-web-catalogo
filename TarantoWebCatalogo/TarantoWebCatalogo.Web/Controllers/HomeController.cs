﻿using System.Web.Mvc;
using System.Threading;
using System.Globalization;
using TarantoWebCatalogo.Services.Core;
using TarantoWebCatalogo.Services.Novedades;
using TarantoWebCatalogo.Services.Utilidades;
using TarantoWebCatalogo.Services.BoletinLanzamiento;
using TarantoWebCatalogo.ViewModels.Home;
using TarantoWebCatalogo.ViewModels.Usuarios;
using TarantoWebCatalogo.Web.helpers;

namespace TarantoWebCatalogo.Web.Controllers
{
    public partial class HomeController : BaseController
    {
        private readonly ICarrouselService _carrouselService;
        private readonly ISeguridadService _seguridadService;
        private readonly INovedadesService _novedadesService;
        private readonly IBoletinLanzamientoService _boletinLanzamientoService;

        public HomeController()
        {
            _carrouselService = Factory.CrearCarrouselService();
            var webConfigHelper = new WebConfigHelper();
            _seguridadService = Factory.CrearSeguridadService(webConfigHelper.ApiUrl);
            _novedadesService = Factory.CrearNovedadesService();
            _boletinLanzamientoService = Factory.CrearBoletinLanzamientoService();

        }

        public virtual ActionResult Index(string idioma)
        {
            if (idioma != null)
            {
                System.Web.HttpContext.Current.Session["Idioma"] = idioma;
                ViewData["Idioma"] = System.Web.HttpContext.Current.Session["sessionString"] as string;
                var cInf = new CultureInfo(idioma + "-" + idioma.ToUpper(), true);
                Thread.CurrentThread.CurrentCulture = cInf;
                Thread.CurrentThread.CurrentUICulture = cInf;
            }
            else if(Session["Idioma"] != null)
            {
                idioma = Session["Idioma"].ToString();
            }
            else
            {
                System.Web.HttpContext.Current.Session["Idioma"] = "es";
                idioma = "es";
            }

            ViewBag.WebAdminUri = System.Web.Configuration.WebConfigurationManager.AppSettings["WebAdminUri"];
            return View(CrearViewModelIndex(idioma));

        }
        
        [HttpPost]
        public virtual ActionResult Login(Login login)
        {
            if (_seguridadService.HayUsuarioLogueado)
            {
                return RedirectToAction(MVC.Home.Perfil());
            }

            if (_seguridadService.Login(login.Usuario, login.Password))
            {
                return RedirectToAction(MVC.Home.Perfil());
            }

            ModelState.AddModelError("", "Usuario y contraseña inválidos");

            return RedirectToAction(MVC.Home.Index());
        }

        [HttpGet]
        public virtual ActionResult Perfil()
        {
            var boletinLanzamiento = _boletinLanzamientoService.ObtenerMemoLanzamiento();
            ViewBag.WebAdminUri = System.Web.Configuration.WebConfigurationManager.AppSettings["WebAdminUri"];

            ViewBag.BoletinImagen = boletinLanzamiento?.Imagen;
            ViewBag.BoletinArchivo = boletinLanzamiento?.Archivo;
            ViewBag.BoletinTitulo = boletinLanzamiento?.TituloEs;
            return View();
        }

        public virtual ActionResult Logout()
        {
            _seguridadService.Logout();

            return RedirectToAction(MVC.Home.Index());
        }

        #region Private

        private HomeIndex CrearViewModelIndex(string idioma)
        {
            var homeVm = new HomeIndex()
            {
                ContenidoCarrousel = _carrouselService.ObtenerTodosPorIdioma(idioma),
                Novedades = _novedadesService.ObtenerUltimasPorIdioma(idioma)
            };

            return homeVm;
        }

        #endregion
    }
}