﻿using System;
using System.Web.Mvc;
using TarantoWebCatalogo.Services.Core;

namespace TarantoWebCatalogo.Web.Controllers
{
    public partial class ContactoController : BaseController
    {
        private readonly IEnvioMailService _envioMailService;

        public ContactoController()
        {
            _envioMailService = Factory.CrearEnvioMailService();
        }

        public virtual ActionResult Index(string respuesta)
        {
            ViewBag.Mensaje = respuesta;
            return View();
        }

        public virtual ActionResult EnvioMailContacto(string motivo, string sede, string nombreApellido, string email, string telefono, string consulta)
        {
            try
            {
                var mensaje = "<strong>Motivo: </strong>" + motivo + "<br />" +
                              "<strong>Sede: </strong>" + sede + "<br />" +
                              "<strong>Nombre y Apellido: </strong>" + nombreApellido + "<br />" +
                              "<strong>Email: </strong>" + email + "<br />" +
                              "<strong>Teléfono: </strong>" + telefono + "<br />" +
                              "<strong>Consulta: </strong>" + consulta;
                _envioMailService.SendEmailTwilio("info@taranto.com.ar", "Contacto desde Web Taranto", mensaje).Wait();
                return RedirectToAction(MVC.Contacto.Index("Mail enviado con éxito"));
            }
            catch (Exception)
            {
                return RedirectToAction(MVC.Contacto.Index("El mail no se pudo enviar"));
            }

        }
    }
}