﻿using System.Web.Mvc;
using TarantoWebCatalogo.Data.UnitOfWork;
using TarantoWebCatalogo.Services.Productos;
using TarantoWebCatalogo.ViewModels.Productos;

namespace TarantoWebCatalogo.Web.Controllers
{
    public partial class ProductosController : BaseController
    {
        private readonly IProductoService _productoService;

        public ProductosController()
        {
            _productoService = Factory.CrearProductoService();
        }

        public virtual ActionResult Index(int? idProducto)
        {
            var idioma = Session["Idioma"].ToString();

            return idProducto != null
                ? View(new ProductoIndex
                {
                    Producto = _productoService.ObtenerPorId(idioma, idProducto.Value),
                    MenuProductos = _productoService.ObtenerTodosParaMenuPorIdioma(idioma)
                }
                    )
                : View(new ProductoIndex
                {
                    MenuProductos = _productoService.ObtenerTodosParaMenuPorIdioma(idioma)
                });
        }
    }
}