﻿namespace TarantoWebCatalogo.Web.helpers
{
    public class WebConfigHelper
    {
        public string ApiUrl => System.Web.Configuration.WebConfigurationManager.AppSettings["ApiUrl"];
    }
}