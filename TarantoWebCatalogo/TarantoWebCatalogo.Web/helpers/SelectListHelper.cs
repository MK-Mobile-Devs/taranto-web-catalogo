﻿using System.Collections.Generic;
using TarantoWebCatalogo.ViewModels;

namespace TarantoWebCatalogo.Web.helpers
{
    public class SelectListHelper
    {
        public static List<ParaDropdown> AgregarItemInicial(List<ParaDropdown> lista, string textoItemInicial, int valorItemInicial)
        {
            lista.Insert(0, new ParaDropdown { Name = textoItemInicial, Id = valorItemInicial });

            return lista;
        }
        
    }
}
