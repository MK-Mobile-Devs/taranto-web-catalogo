﻿namespace TarantoWebCatalogo.Web.helpers
{
    public static class Constantes
    {
        public static string IdiomaEspanol => "es";
        public static string IdiomaPortugues => "pr";
        public static string IdiomaIngles => "en";
    }
}