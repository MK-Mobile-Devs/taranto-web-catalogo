﻿namespace TarantoWebCatalogo.Web.helpers.Enums
{
    public enum PaginaInstitucionales
    {
        GrupoTaranto = 1,
        CalidadSeguridadMedioAmbiente = 2,
        Rrhh = 3
    }

}