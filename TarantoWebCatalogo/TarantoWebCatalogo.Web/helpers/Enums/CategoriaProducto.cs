﻿namespace TarantoWebCatalogo.Web.helpers.Enums
{
    public enum CategoriaProducto
    {
        Sellado = 9,
        Friccion = 58,
        Fijacion = 3,
        SuspensionYDireccion = 4,
        ComponentesDeMotor = 5,
        Encendido = 6,
        HerramientasTaranto = 7
    }
}