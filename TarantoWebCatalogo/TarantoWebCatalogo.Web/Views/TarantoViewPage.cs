﻿using System.Web.Mvc;
using TarantoWebCatalogo.Data.UnitOfWork;
using TarantoWebCatalogo.Services;
using TarantoWebCatalogo.Services.Novedades;
using TarantoWebCatalogo.Services.Productos;
using TarantoWebCatalogo.Services.Catalogo;
using TarantoWebCatalogo.Services.Core;
using TarantoWebCatalogo.Services.Pedidos;
using TarantoWebCatalogo.Web.helpers;
using TarantoWebCatalogo.Services.Perfil;

namespace TarantoWebCatalogo.Web.Views
{
    public abstract class TarantoViewPage : WebViewPage
    {
        public IProductoService ProductoService { get; private set; }
        public IPerfilService PerfilService { get; set; }
        public INovedadesService NovedadesService { get; private set; }
        public ICatalogoService CatalogoService { get; private set; }
        public ISeguridadService SeguridadService { get; private set; }
        public IPedidosService PedidosService { get; private set; }
        public WebConfigHelper WebConfigHelper { get; }

        public TarantoViewPage()
        {
            var factory = new ServiceFactory(new UnitOfWork());
            ProductoService = factory.CrearProductoService();
            NovedadesService = factory.CrearNovedadesService();
            WebConfigHelper = new WebConfigHelper();
            SeguridadService = factory.CrearSeguridadService(WebConfigHelper.ApiUrl);
            PedidosService = factory.CrearPedidoService();
            PerfilService = factory.CrearPerfilService();
        }
    }

    public abstract class TarantoViewPage<T> : WebViewPage<T>
    {
        public IProductoService ProductoService { get; private set; }
        public INovedadesService NovedadesService { get; private set; }
        public ICatalogoService CatalogoService { get; private set; }
        public ISeguridadService SeguridadService { get; private set; }
        public IPedidosService PedidosService { get; private set; }
        public WebConfigHelper WebConfigHelper { get; }

        public TarantoViewPage()
        {
            var factory = new ServiceFactory(new UnitOfWork());
            ProductoService = factory.CrearProductoService();
            NovedadesService = factory.CrearNovedadesService();
            WebConfigHelper = new WebConfigHelper();
            SeguridadService = factory.CrearSeguridadService(WebConfigHelper.ApiUrl);
            PedidosService = factory.CrearPedidoService();
        }
    }
}