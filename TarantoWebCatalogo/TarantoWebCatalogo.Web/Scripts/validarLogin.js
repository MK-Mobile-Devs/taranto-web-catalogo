﻿$(function () {
    $("#btnIngresar").on("click", function (evento) {
        var usuario = $("#lblUsuario").val()
        var password = $("#lblPassword").val()
        const datos = {
            Usuario: usuario,
            Password: password
        };
        $.ajax({
            url: apiUrl + "/core/validarLogin",
            type: "POST",
            data: datos,
			async: false,
            error: function () {
                $("#menuLogin").addClass("open")
                $("#loginError").removeClass("hide");
                window.stop();
            },
            success: function () {
            }
        });
    });
});

