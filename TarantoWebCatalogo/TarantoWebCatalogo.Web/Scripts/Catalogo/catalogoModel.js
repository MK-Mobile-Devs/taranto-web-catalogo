﻿var catalogoModel = (function () {
    var combosMotor = function() {
        return {
            marca: $("#cboMarcaSeccionPorMotor"),
            combustible: $("#cboCombustibleSeccionPorMotor"),
            cilindrada: $("#cboCilindradaSeccionPorMotor"),
            motor: $("#cboMotorSeccionPorMotor"),
            familia: $("#cboFamiliaSeccionPorMotor")
        }
    }

    var combosMarca = function () {
        return {
            marca: $("#cboMarcaSeccionPorMarca"),
            modelo: $("#cboModeloSeccionPorMarca"),
            familia: $("#cboFamiliaSeccionPorMarca"),
            combustible: $("#cboCombustibleSeccionPorMarca"),
            motor: $("#cboMotorSeccionPorMarca")
        }
    }

    var combosAvanzada = function() {
        return {
            marca: $("#cboMarcaSeccionPorAvanzada"),
            combustible: $("#cboCombustibleSeccionPorAvanzada")
        }
    }

    var combos = function () {
        return {
            motor: combosMotor,
            marca: combosMarca,
            avanzada: combosAvanzada
        }
    }

    var div = function () {
        return {
            catalogoCabecera: $("#catalogoCabecera"),
            catalogoDetalle: $("#catalogoDetalle"),
            catalogoArticulosRelacionados: $("#catalogoArticulosRelacionados"),
            cuadro: $("#cuadro"),
            cargaDeCombosSpin: $("#cargaDeCombosSpin"),
            cargaDeCombosSpinParaResultado: $("#cargaDeCombosSpinParaResultado"),
            alertaErrorDeApi: $("#divAlertaErrorDeApi"),
            codigosSurgeridos: $(".tt-dataset"),
            cantidadResultadoBusquedaAvanzada: $("#cantidadResultadoBusquedaAvanzada"),
            paginado: $("#paginado"),
            badgeCantidadArticulosCarrito: $("#badgeCantidadArticulosCarrito")
        }
    }

    var botones = function () {
        return {
            filtrarPorMotor: $("#btnFiltrarSeccionMotor"),
            filtrarPorMarca: $("#btnFiltrarSeccionMarca"),
            filtrarSeccionCodigo: $("#btnFiltrarSeccionCodigo"),
            filtrasBusquedaRapidaSeccionAvanzada: $("#btnFiltrarBusquedaRapidaSeccionAvanzada"),
            filtrarAvanzadaSeccionAvanzada: $("#btnFiltrarAvanzadaSeccionAvanzada")
        }
    }

    var textBoxesAvanzadas = function() {
        return {
            busquedaRapida: $("#textBoxBusquedaRapidaSeccionPorAvanzada"),
            modelo: $("#textBoxModeloSeccionPorAvanzada"),
            cilindrada: $("#textBoxCilindradaSeccionPorAvanzada"),
            motor: $("#textBoxMotorSeccionPorAvanzada"),
            cilindros: $("#textBoxCilindrosSeccionPorAvanzada"),
            valvulas: $("#textBoxValvulasSeccionPorAvanzada")
        }
    }

    var textboxes = function() {
        return {
            codigoSeccionPorCodigo: $("#textBoxCodigoSeccionPorCodigo"),
            avanzada: textBoxesAvanzadas
        }
    }

    var eventos = function () {
        return {
            cboMarcaSeleccionadoSeccionMotor: "cboMarcaSeleccionadoSeccionMotor",
            cboCombustibleSeleccionadoSeccionMotor: "cboCombustibleSeleccionadoSeccionMotor",
            cboCilindradaSeleccionadoSeccionMotor: "cboCilindradaSeleccionadoSeccionMotor",
            cboMotorSeleccionadoSeccionMotor: "cboMotorSeleccionadoSeccionMotor",
            cboMarcaSeleccionadoSeccionMarca: "cboMarcaSeleccionadoSeccionMarca",
            cboModeloSeleccionadoSeccionMarca: "cboModeloSeleccionadoSeccionMarca",
            cboFamiliaSeleccionadoSeccionMarca: "cboFamiliaSeleccionadoSeccionMarca",
            cboCombustibleSeleccionadoSeccionMarca: "cboCombustibleSeleccionadoSeccionMarca",
            seleccionoMarcaSeccionAvanzada: "seleccionoMarcaSeccionAvanzada",
            codigoAfiltrarModificado: "codigoAfiltrarModificado",
            codigoSugeridoSeleccionado: "codigoSugeridoSeleccionado",
            clickBuscarSeccionCodigo: "clickBuscarSeccionCodigo",
            clickBusquedaRapidaSeccionAvanzada: "clickBusquedaRapidaSeccionAvanzada",
            clickBusquedaAvanzadaSeccionAvanzada: "clickBusquedaAvanzadaSeccionAvanzada",
            obtenerCatalogo: "obtenerCatalogo",
            obtenerArticuloAmpliado: "obtenerArticuloAmpliado",
            clickAgregarAlCarrito: "clickAgregarAlCarrito"
        }
    }


    return {
        doc: $(document),
        combos: combos,
        botones: botones,
        textboxes: textboxes,
        div: div,
        esCliente: esCliente,
        apiUrl: apiUrl,
        usuario: usuario,
        apiLocalUrl: apiLocalUrl,
        eventos: eventos
    }
})();
