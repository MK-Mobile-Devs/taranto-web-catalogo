﻿$(function () {
    var combos = catalogoModel.combos();
    var botones = catalogoModel.botones();
    var eventos = catalogoModel.eventos();
    var divs = catalogoModel.div();
    var textboxes = catalogoModel.textboxes();
    var claseToggleTaranto = ".toggle-closed";
    const claseTtSuggestion = ".tt-suggestion";
    const btnAgregarAlCarrito = ".btncarrito";
    var buscandoAutocomplete = null;
    var paginaSeleccionada;

   //SECCION POR MOTOR 
    combos.motor().marca.on("change", function (event) {
        catalogoModel.doc.trigger(eventos.cboMarcaSeleccionadoSeccionMotor);
        event.preventDefault();
    });

    combos.motor().combustible.on("change", function (event) {
        catalogoModel.doc.trigger(eventos.cboCombustibleSeleccionadoSeccionMotor);
        event.preventDefault();
    });

    combos.motor().cilindrada.on("change", function (event) {
        catalogoModel.doc.trigger(eventos.cboCilindradaSeleccionadoSeccionMotor);
        event.preventDefault();
    });

    combos.motor().motor.on("change", function (event) {
        catalogoModel.doc.trigger(eventos.cboMotorSeleccionadoSeccionMotor);
        event.preventDefault();
    });
    
    botones.filtrarPorMotor.on("click", function (event) {
        catalogoModel.doc.trigger(eventos.obtenerCatalogo, ["cboMotorSeccionPorMotor", "cboMarcaSeccionPorMotor", "cboFamiliaSeccionPorMotor"]);
        event.preventDefault();
    });

    //SECCION POR MARCA
    combos.marca().marca.on("change", function (event) {
        catalogoModel.doc.trigger(eventos.cboMarcaSeleccionadoSeccionMarca);
        event.preventDefault();
    });

    combos.marca().modelo.on("change", function (event) {
        catalogoModel.doc.trigger(eventos.cboModeloSeleccionadoSeccionMarca);
        event.preventDefault();
    });

    combos.marca().familia.on("change", function (event) {
        catalogoModel.doc.trigger(eventos.cboFamiliaSeleccionadoSeccionMarca);
        event.preventDefault();
    });

    combos.marca().combustible.on("change", function (event) {
        catalogoModel.doc.trigger(eventos.cboCombustibleSeleccionadoSeccionMarca);
        event.preventDefault();
    });

    botones.filtrarPorMarca.on("click", function (event) {
        catalogoModel.doc.trigger(eventos.obtenerCatalogo, ["cboMotorSeccionPorMarca", "cboMarcaSeccionPorMarca","cboFamiliaSeccionPorMarca"]);
        event.preventDefault();
    });

    //SECCION POR CODIGO
    textboxes.codigoSeccionPorCodigo.on("keyup", function (event) {
        if (textboxes.codigoSeccionPorCodigo.val().length > 2) {
            if (buscandoAutocomplete) {
                clearTimeout(buscandoAutocomplete);
            }

            buscandoAutocomplete = setTimeout(function () {
                catalogoModel.doc.trigger(eventos.codigoAfiltrarModificado);
            }, 500);
            $(".tt-menu").show();
        } else {
            $(".tt-menu").hide();
        }
        event.preventDefault();
    });

    divs.codigosSurgeridos.on("click", claseTtSuggestion ,function (event) {
        var codigo = $(this).text();
        $(".tt-menu").hide();
        catalogoModel.doc.trigger(eventos.codigoSugeridoSeleccionado, codigo);
        event.preventDefault();
    });

    botones.filtrarSeccionCodigo.on("click", function (event) {
        var codigo = textboxes.codigoSeccionPorCodigo.val();
        catalogoModel.doc.trigger(eventos.clickBuscarSeccionCodigo, codigo);
        event.preventDefault();
    });

    //SECCION POR AVANZADA
    combos.avanzada().marca.on("change", function (event) {
        catalogoModel.doc.trigger(eventos.seleccionoMarcaSeccionAvanzada);
        event.preventDefault();
    });

    textboxes.avanzada().busquedaRapida.on("keyup", function (event) {
        if (textboxes.avanzada().busquedaRapida.val().length > 2) {
            botones.filtrasBusquedaRapidaSeccionAvanzada.removeClass("disabled");
        }
        event.preventDefault();
    });


    $("#textBoxBusquedaRapidaSeccionPorAvanzada").keypress(function (event) {
        if (event.keyCode == 13) {
            divs.paginado.on("click", "a", function (event) {
                paginaSeleccionada = $(this).text();
                var texto = textboxes.avanzada().busquedaRapida.val();
                catalogoModel.doc.trigger(eventos.clickBusquedaRapidaSeccionAvanzada, [texto.toUpperCase(), paginaSeleccionada]);
                event.preventDefault();
            });

            var texto = textboxes.avanzada().busquedaRapida.val();
            catalogoModel.doc.trigger(eventos.clickBusquedaRapidaSeccionAvanzada, [texto.toUpperCase(), 1]);
            event.preventDefault();
        }
    });
    

    botones.filtrasBusquedaRapidaSeccionAvanzada.on("click", function (event) {
        divs.paginado.on("click", "a", function (event) {
            paginaSeleccionada = $(this).text();
            var texto = textboxes.avanzada().busquedaRapida.val();
            catalogoModel.doc.trigger(eventos.clickBusquedaRapidaSeccionAvanzada, [texto.toUpperCase(), paginaSeleccionada]);
            event.preventDefault();
        });

        var texto = textboxes.avanzada().busquedaRapida.val();
        catalogoModel.doc.trigger(eventos.clickBusquedaRapidaSeccionAvanzada, [texto.toUpperCase(), 1]);
        event.preventDefault();
    });


    botones.filtrarAvanzadaSeccionAvanzada.on("click", function (event) {
        divs.paginado.on("click", "a", function (event) {
            paginaSeleccionada = $(this).text();
            catalogoModel.doc.trigger(eventos.clickBusquedaAvanzadaSeccionAvanzada, paginaSeleccionada);
            event.preventDefault();
        });
        catalogoModel.doc.trigger(eventos.clickBusquedaAvanzadaSeccionAvanzada);
        event.preventDefault();
    });



    //GENERAL
    divs.catalogoArticulosRelacionados.on("click", claseToggleTaranto, function () {
        var $fila = $(this);
        $fila.parent().siblings(1).toggle();
    });

    divs.catalogoArticulosRelacionados.on("click", btnAgregarAlCarrito, function() {
        var $boton = $(this);
        var usuario = $boton.data("usuario");
        var articulo = $boton.data("articulo");
        var idCantidadArticulo = "#cantidadArticulo-" + articulo
        var cantidad = parseInt($(idCantidadArticulo).val())
        catalogoModel.doc.trigger(eventos.clickAgregarAlCarrito, [usuario, articulo, cantidad]);
        event.preventDefault();
    });

    catalogoManager.inicializar();
    motorManager.inicializar();
    marcaManager.inicializar();
    codigoManager.inicializar();
    avanzadaManager.inicializar();
});
