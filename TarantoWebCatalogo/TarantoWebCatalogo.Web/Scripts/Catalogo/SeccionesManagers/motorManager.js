﻿var motorManager = (function () {
    var eventos = catalogoModel.eventos();
    var combo = catalogoModel.combos();
    var boton = catalogoModel.botones();
    var div = catalogoModel.div();

    var asignarValoresDelCombo = function(valor, descripcion) {
        return `<option value="${valor}">${descripcion}</option>`;
    }
    
    var obtenerCombustibleParaCbo = function () {
        div.cargaDeCombosSpin.show();
        const marcaSeleccionada = $("#cboMarcaSeccionPorMotor option:selected").val();
        $.ajax({
            url: catalogoModel.apiUrl + "catalogo/combustiblesPorMarca?marca=" + marcaSeleccionada,
            type: "GET",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            error: function () {
                div.alertaErrorDeApi.show();
            },
            success: function (data) {
                var html = "";
                for (let i = 0; i < data.length; i++) {
                    html += asignarValoresDelCombo(data[i].Codigo, data[i].Descripcion);
                }
                combo.motor().combustible.html(html);
                $("#cboCombustibleSeccionPorMotor").selectpicker("refresh");
                div.cargaDeCombosSpin.hide();
            }
        });
    }

    var obtenerCilindradaParaCbo = function () {
        div.cargaDeCombosSpin.show();
        const marcaSeleccionada = $("#cboMarcaSeccionPorMotor option:selected").val();
        const combustibleSeleccionado = $("#cboCombustibleSeccionPorMotor option:selected").text();
        $.ajax({
            url: catalogoModel.apiUrl + "catalogo/cilindradaPorMarcaYCombustible?marca=" + marcaSeleccionada + "&combustible=" + combustibleSeleccionado,
            type: "GET",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            error: function () {
                div.alertaErrorDeApi.show();
            },
            success: function (data) {
                var html = "";
                for (var i = 0; i < data.length; i++) {
                    html += asignarValoresDelCombo(i + '" data-cilindradaDesde="' + data[i].RangoDesde + '" data-cilindradaHasta="' + data[i].RangoHasta + '"'
                        , data[i].Descripcion + " cc");
                }
                combo.motor().cilindrada.html(html);
                $("#cboCilindradaSeccionPorMotor").selectpicker("refresh");
                div.cargaDeCombosSpin.hide();
            }
        });
    }

    var obtenerMotorParaCbo = function () {
        div.cargaDeCombosSpin.show();
        const marcaSeleccionada = $("#cboMarcaSeccionPorMotor option:selected").val();
        const combustibleSeleccionado = $("#cboCombustibleSeccionPorMotor option:selected").text();
        const cilindradaDesde = parseInt($("#cboCilindradaSeccionPorMotor option:selected").attr("data-cilindradaDesde"));
        const cilindradaHasta = parseInt($("#cboCilindradaSeccionPorMotor option:selected").attr("data-cilindradaHasta"));

        $.ajax({
            url: catalogoModel.apiUrl + "catalogo/motoresPorMarcaCombustibleYcilindradas?marca="
                + marcaSeleccionada + "&combustible=" + combustibleSeleccionado + "&cilindradaDesde=" + cilindradaDesde
                + "&cilindradaHasta=" + cilindradaHasta,
            type: "GET",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            error: function () {
                div.alertaErrorDeApi.show();
            },
            success: function (data) {
                var html = "";

                $.each(data, function (index, valor) {
                    var descripcion = [];
                    if (valor.Cilindrada.length > 0) {
                        descripcion.push(valor.Cilindrada);
                    }
                    if (valor.Potencia.length > 0) {
                        descripcion.push(valor.Potencia);
                    }
                    if (valor.MOTDICIL.length > 0) {
                        descripcion.push(valor.MOTDICIL);
                    }
                    descripcion.push(valor.Nombre),
                    html += `<option value="${valor.Nombre}" data-subtext="${descripcion.join(" - ")}" data-motcilin="${valor.Cilindrada}"
                            data-potencia="${valor.Potencia}" data-motdicil="${valor.MOTDICIL}" 
                            data-motdesc="${valor.Descripcion}">${valor.Descripcion}</option>`;
                });
                combo.motor().motor.html(html);
                $("#cboMotorSeccionPorMotor").selectpicker("refresh");
                div.cargaDeCombosSpin.hide();
            }
        });
    }

    var obtenerFamiliaParaCbo = function () {
        div.cargaDeCombosSpin.show();
        const motorSeleccionado = $("#cboMotorSeccionPorMotor option:selected").val();
        $.ajax({
            url: catalogoModel.apiUrl + "catalogo/familiasPorMotor?motor=" + motorSeleccionado,
            type: "GET",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            error: function () {
                div.alertaErrorDeApi.show();
            },
            success: function (data) {
                var html = "";
                for (let i = 0; i < data.length; i++) {
                    html += asignarValoresDelCombo(data[i].Id, data[i].Nombre);
                }
                combo.motor().familia.html(html);
                $("#cboFamiliaSeccionPorMotor").selectpicker("refresh");
                boton.filtrarPorMotor.removeClass("disabled").focus();
                div.cargaDeCombosSpin.hide();
            }
        });
    }
    
    var establecerEventos = function () {
        catalogoModel.doc.on(eventos.cboMarcaSeleccionadoSeccionMotor, obtenerCombustibleParaCbo);
        catalogoModel.doc.on(eventos.cboCombustibleSeleccionadoSeccionMotor, obtenerCilindradaParaCbo);
        catalogoModel.doc.on(eventos.cboCilindradaSeleccionadoSeccionMotor, obtenerMotorParaCbo);
        catalogoModel.doc.on(eventos.cboMotorSeleccionadoSeccionMotor, obtenerFamiliaParaCbo);
        
    }

    var inicializar = function () {
        establecerEventos();
    }

    return {
        inicializar: inicializar
    }
})();