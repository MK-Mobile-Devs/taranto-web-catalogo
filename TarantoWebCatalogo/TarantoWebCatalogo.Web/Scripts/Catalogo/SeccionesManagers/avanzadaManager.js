﻿var avanzadaManager = (function () {
    var eventos = catalogoModel.eventos();
    var div = catalogoModel.div();
    var combo = catalogoModel.combos();

    var asignarValoresDelCombo = function (valor, descripcion) {
        return `<option value="${valor}">${descripcion}</option>`;
    }

    var obtenerCombustibleParaCbo = function () {
        div.cargaDeCombosSpin.show();
        const marcaSeleccionada = $("#cboMarcaSeccionPorAvanzada option:selected").val();
        $.ajax({
            url: catalogoModel.apiUrl + "catalogo/combustiblesPorMarca?marca=" + marcaSeleccionada,
            type: "GET",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            error: function () {
                div.alertaErrorDeApi.show();
            },
            success: function (data) {
                var html = "";
                for (let i = 0; i < data.length; i++) {
                    html += asignarValoresDelCombo(data[i].Codigo, data[i].Descripcion);
                }
                combo.avanzada().combustible.html(html);
                $("#cboCombustibleSeccionPorAvanzada").selectpicker("refresh");
                div.cargaDeCombosSpin.hide();
            }
        });
    }

    var establecerEventos = function () {
        catalogoModel.doc.on(eventos.seleccionoMarcaSeccionAvanzada, obtenerCombustibleParaCbo);
    }

    var inicializar = function () {
        establecerEventos();
    }

    return {
        inicializar: inicializar
    }

})();