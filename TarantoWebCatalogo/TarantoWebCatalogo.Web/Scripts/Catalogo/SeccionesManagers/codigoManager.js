﻿var codigoManager = (function () {
    var eventos = catalogoModel.eventos();
    var boton = catalogoModel.botones();
    var div = catalogoModel.div();
    var textboxes = catalogoModel.textboxes();

    var asignarValoresDelCombo = function (valor) {
        return `<div class="tt-suggestion">${valor}</div>`;
    } 

    var obtenerCodigosFiltrados = function () {
        div.cargaDeCombosSpin.show();
        const codigoIngresado = $("#textBoxCodigoSeccionPorCodigo").val();
        $.ajax({
            url: catalogoModel.apiUrl + "catalogo/articulosPorCodigo?codigo=" + codigoIngresado,
            type: "GET",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            fail: function () {
                div.alertaErrorDeApi.show();
            },
            success: function (data) {
                var html = "";
                for (let i = 0; i < data.length; i++) {
                    html += asignarValoresDelCombo(data[i].Codigo);
                }
                $(".tt-dataset").html(html);
                div.cargaDeCombosSpin.hide();
            }
        });
    }

    var reemplazarCodigoEscritoPorElSeleccionado = function (evento, codigo) {
        textboxes.codigoSeccionPorCodigo.val(codigo);
        boton.filtrarSeccionCodigo.removeClass("disabled").focus();
    }

    var establecerEventos = function () {
        catalogoModel.doc.on(eventos.codigoAfiltrarModificado, obtenerCodigosFiltrados);
        catalogoModel.doc.on(eventos.codigoSugeridoSeleccionado, reemplazarCodigoEscritoPorElSeleccionado);
    }

    var inicializar = function () {
        establecerEventos();
    }

    return {
        inicializar: inicializar
    }

})();