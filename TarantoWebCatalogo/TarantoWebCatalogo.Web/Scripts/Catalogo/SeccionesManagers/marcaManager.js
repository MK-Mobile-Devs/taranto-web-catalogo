﻿var marcaManager = (function () {
    var eventos = catalogoModel.eventos();
    var combo = catalogoModel.combos();
    var boton = catalogoModel.botones();
    var div = catalogoModel.div();

    var asignarValoresDelCombo = function (valor, descripcion) {
        return `<option value="${valor}">${descripcion}</option>`;
    }

    var obtenerModeloParaCbo = function () {
        div.cargaDeCombosSpin.show();
        const marcaSeleccionada = $("#cboMarcaSeccionPorMarca option:selected").val();
        $.ajax({
            url: catalogoModel.apiUrl + "catalogo/modelosPorMarca?marca=" + marcaSeleccionada,
            type: "GET",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            fail: function () {
                div.alertaErrorDeApi.show();
            },
            success: function (data) {
                var html = "";
                for (let i = 0; i < data.length; i++) {
                    html += asignarValoresDelCombo(data[i].Descripcion, data[i].Descripcion);
                }
                combo.marca().modelo.html(html);
                $("#cboModeloSeccionPorMarca").selectpicker("refresh");
                div.cargaDeCombosSpin.hide();
            }
        });
    }

    var obtenerFamiliaParaCbo = function () {
        div.cargaDeCombosSpin.show();
        const marcaSeleccionada = $("#cboMarcaSeccionPorMarca option:selected").val();
        const modeloSeleccionado = $("#cboModeloSeccionPorMarca option:selected").val();
        $.ajax({
            url: catalogoModel.apiUrl + "catalogo/familiasPorMarcaYmodelo?marca=" + marcaSeleccionada + "&modelo=" + modeloSeleccionado,
            type: "GET",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            fail: function () {
                div.alertaErrorDeApi.show();
            },
            success: function (data) {
                var html = "";
                for (let i = 0; i < data.length; i++) {
                    html += asignarValoresDelCombo(data[i].Id, data[i].Nombre);
                }
                combo.marca().familia.html(html);
                $("#cboFamiliaSeccionPorMarca").selectpicker("refresh");
                div.cargaDeCombosSpin.hide();
            }
        });
    }

    var obtenerCombustibleParaCbo = function () {
        const familiaSeleccionada = $("#cboFamiliaSeccionPorMarca option:selected").val();

        if (familiaSeleccionada === "13") {
            combo.marca().combustible.attr("disabled", true);
            combo.marca().motor.attr("disabled", true);
            boton.filtrarPorMarca.removeClass("disabled").focus();
        } else {
            combo.marca().combustible.attr("disabled", false);
            combo.marca().motor.attr("disabled", false);
            boton.filtrarPorMarca.removeClass("disabled").focus();
            div.cargaDeCombosSpin.show();
            const marcaSeleccionada = $("#cboMarcaSeccionPorMarca option:selected").val();
            $.ajax({
                url: catalogoModel.apiUrl + "catalogo/combustiblesPorMarca?marca=" + marcaSeleccionada,
                type: "GET",
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                fail: function (e) {
                    div.alertaErrorDeApi.show();
                },
                success: function (data) {
                    var html = "";
                    for (let i = 0; i < data.length; i++) {
                        html += asignarValoresDelCombo(data[i].Codigo, data[i].Descripcion);
                    }
                    combo.marca().combustible.html(html);
                    $("#cboCombustibleSeccionPorMarca").selectpicker("refresh");
                    div.cargaDeCombosSpin.hide();
                }
            });
        }
    }
    
    var obtenerMotorParaCbo = function () {
        div.cargaDeCombosSpin.show();
        const marcaSeleccionada = $("#cboMarcaSeccionPorMarca option:selected").val();
        const modeloSeleccionado = $("#cboModeloSeccionPorMarca option:selected").val();
        const combustibleSeleccionado = $("#cboCombustibleSeccionPorMarca option:selected").text();

        $.ajax({
            url: catalogoModel.apiUrl + "catalogo/motoresPorMarcaModeloYcombustible?marca="
                + marcaSeleccionada + "&modelo=" + modeloSeleccionado + "&combustible=" + combustibleSeleccionado,
            type: "GET",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            fail: function () {
                div.alertaErrorDeApi.show();
            },
            success: function (data) {
                var html = "";
                $.each(data, function (index, valor) {
                    var descripcion = [];
                    if (valor.Cilindrada.length > 0) {
                        descripcion.push(valor.Cilindrada);
                    }
                    if (valor.Potencia.length > 0) {
                        descripcion.push(valor.Potencia);
                    }
                    if (valor.MOTDICIL.length > 0) {
                        descripcion.push(valor.MOTDICIL);
                    }
                    descripcion.push(valor.Nombre),
                        html += `<option value="${valor.Nombre}" data-subtext="${descripcion.join(" - ")}" data-motcilin="${valor.Cilindrada}"
                            data-potencia="${valor.Potencia}" data-motdicil="${valor.MOTDICIL}" 
                            data-motdesc="${valor.Descripcion}">${valor.Descripcion}</option>`;
                });
                combo.marca().motor.html(html);
                $("#cboMotorSeccionPorMarca").selectpicker("refresh");
                boton.filtrarPorMarca.removeClass("disabled").focus();
                div.cargaDeCombosSpin.hide();
            }
        });
    }
    

    var establecerEventos = function () {
        catalogoModel.doc.on(eventos.cboMarcaSeleccionadoSeccionMarca, obtenerModeloParaCbo);
        catalogoModel.doc.on(eventos.cboModeloSeleccionadoSeccionMarca, obtenerFamiliaParaCbo);
        catalogoModel.doc.on(eventos.cboFamiliaSeleccionadoSeccionMarca, obtenerCombustibleParaCbo);
        catalogoModel.doc.on(eventos.cboCombustibleSeleccionadoSeccionMarca, obtenerMotorParaCbo);
    }

    var inicializar = function () {
        establecerEventos();
    }

    return {
        inicializar: inicializar
    }

})();