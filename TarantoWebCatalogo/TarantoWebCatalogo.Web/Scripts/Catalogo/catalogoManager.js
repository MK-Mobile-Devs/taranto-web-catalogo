﻿var catalogoManager = (function() {
    var eventos = catalogoModel.eventos();
    var div = catalogoModel.div();
    var combo = catalogoModel.combos();
    var ajustes_descuentoEspecial;
    var ajustes_descuentoProntoPago;
    var ajustes_margen;
    var ajustes_iva;

    var asignarValoresDelCombo = function (valor, descripcion) {
        return `<option value="${valor}">${descripcion}</option>`;
    }

    var obtenerTodasLasMarcasParaCbo = function () {
        div.cargaDeCombosSpin.show();
        $.ajax({
            url: catalogoModel.apiUrl + "catalogo/marcas",
            type: "GET",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            error: function () {
                div.alertaErrorDeApi.show();
            },
            success: function (data) {
                var html = "";
                for (let i = 0; i < data.length; i++) {
                    html += asignarValoresDelCombo(data[i].Codigo, data[i].Descripcion);
                }
                combo.motor().marca.html(html);
                combo.marca().marca.html(html);
                combo.avanzada().marca.html(html);
                $("#cboMarcaSeccionPorMotor").selectpicker("refresh");
                $("#cboMarcaSeccionPorMarca").selectpicker("refresh");
                $("#cboMarcaSeccionPorAvanzada").selectpicker("refresh");
                div.cargaDeCombosSpin.hide();
            }
        });
    }

    var obtenerCabecera = function (idComboMotor) {
        $("#seccionPorMotorApliqueTodosLosFiltros").addClass("hide");
        $("#seccionPorMarcaApliqueTodosLosFiltros").addClass("hide");
        
        const motorSeleccionado = $(`#${idComboMotor} option:selected`).val();

        const ficha = `<div class="heading-block">           
                             <h3 class="color-azul">${motorSeleccionado}                   
                                <div class="ver-ficha-pdf" data-pdf="${motorSeleccionado}">
                                    <a href="${apiUrl}catalogo/documentoPdf?nombre=${motorSeleccionado}" class="color-azul" style="margin-right: 10px;">Ver ficha</a>                        
                                    <img src="images/icons/pdf.png" width="50px">
                                </div>
                            </h3>
                            <span class="linea"></span>
                        </div>`;


        return div.catalogoCabecera.html(ficha);
    }

    var generarTablaDeMarca = function (marca, descripcion) {
        return `    <table class="table table-bordered">         
                        <thead>             
                            <tr>                 
                                <th style="background-color:#f0f0f0;">                    
                                    <div class="img-logo-marca">                         
                                        <span class="parse-img" data-img="${marca}">                             
                                            <img src="${apiUrl}catalogo/logoMarcas?marca=${marca}">                         
                                        </span>
                                    </div>
                                    ${marca}                 
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>                 
                                <td>${descripcion}</td>             
                            </tr>
                        </tbody>
                    </table>`;
    }

    var obtenerCuadroDeModelosPorMarca = function (idComboMotor) {
        const motorSeleccionado = $(`#${idComboMotor} option:selected`).val();
        var ficha = "";
        $.ajax({
            url: catalogoModel.apiUrl + "catalogo/modelosPorMotor?motor=" + motorSeleccionado,
            type: "GET",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            error: function () {
                div.alertaErrorDeApi.show();
            },
            success: function (data) {
                obtenerCabecera(idComboMotor);
                ficha += '<div id="catalogo-modelos" class="postcontent nobottommargin clearfix">' +
                    '  <div class="single-post nobottommargin">' +
                    '     <div class="entry clearfix nobottommargin" style="padding-bottom: 0;">';
                if (data.length > 0) {
                    for (let i = 0; i < data.length; i++) {
                        ficha += generarTablaDeMarca(data[i].Marca, data[i].Descripcion);
                    }
                };
                ficha += "    </div>" +
                    "  </div>" +
                    "</div>";
                div.catalogoDetalle.html(ficha);
            }
        });
    };

    var crearTablaDeMotores = function (data) {
        var ficha = '<div id="ficha" class="sidebar nobottommargin col_last clearfix">' +
            '                <table class="table table-bordered nobottommargin">' +
            "                    <thead>" +
            '                        <tr style="background-color:#156099;" class="color-blanco">' +
            "                            <th>MOTOR</th>" +
            "                            <th>CC</th>" +
            "                            <th>HP</th>" +
            "                            <th>Ø</th>" +
            "                        </tr>" +
            "                    </thead>" +
            '                    <tbody style="background-color:#edf3f7;">';

        if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                ficha += "<tr>" +
                    " <td>" + data[i].Descripcion + "</td>" +
                    " <td>" + data[i].Cilindrada + "</td>" +
                    " <td>" + data[i].Potencia + "</td>" +
                    " <td>" + data[i].MOTDICIL + "</td>" +
                    "</tr>";
            }
        };
        ficha += "                    </tbody>" +
            "                </table>" +
            "   </div>";
        return ficha;
    }

    var obtenerCuadroDeDetalleDeMotores = function (idComboMotor) {
        const motorSeleccionado = $(`#${idComboMotor} option:selected`).val();
        $.ajax({
            url: catalogoModel.apiUrl + "catalogo/motoresPorMotor?motor=" + motorSeleccionado,
            type: "GET",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            error: function () {
                div.alertaErrorDeApi.show();
            },
            success: function (data) {
                div.cuadro.html(crearTablaDeMotores(data));
            }
        });
    };

    var crearTablaDeMotoresDeArticulosRelacionados = function (data) {
        var ficha = "";
        if (data.length > 0) {
            ficha += '                          <table class="table table-bordered small info-motor">' +
                "                                    <thead>" +
                '                                        <tr style="background-color:#156099;" class="color-blanco">' +
                "                                            <th>MOTOR</th>" +
                "                                            <th>CC</th>" +
                "                                            <th>HP</th>" +
                "                                            <th>Ø</th>" +
                "                                        </tr>" +
                "                                    </thead>" +
                '                                    <tbody style="background-color:#edf3f7;">';
            for (let i = 0; i < data.length; i++) {
                ficha += `<tr class="tr-clone"> 
                        <td class="info-motdesc">${data[i].Descripcion}</td> 
                        <td class="info-motcilin">${data[i].Cilindrada}</td> 
                        <td class="info-potencia">${data[i].Potencia}</td> 
                        <td class="info-motdicil">${data[i].MOTDICIL}</td></tr>`;
            }
        
        ficha += "                    </tbody>" +
                "                </table>";
        };
        
        return ficha;
    }


    var obtenerCuadroDeDetalleDeMotoresDeArticulosRelacionados = function (articuloId) {
        $.ajax({
            url: catalogoModel.apiUrl + "catalogo/motoresPorArticuloId?articuloId=" + articuloId,
            type: "GET",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            error: function () {
                div.alertaErrorDeApi.show();
            },
            success: function (data) {
                const tabla = crearTablaDeMotoresDeArticulosRelacionados(data);
                $(`#cuadroMotor-${articuloId.replace(".", "-")}`).html(tabla);
            }
        });
    };

    var crearTablaDetallesArticulosRelacionados = function (data, articuloId) {
        var ficha = "";
        if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                if (data[i].Material.length > 0) {
                    ficha += `<p class="info-material nobottommargin">Material: <b>${data[i].Material}</b></p>`;
                }
                if (data[i].NRORI.length > 0) {
                    ficha += `<p class="info-ref-noe mb-20">Referencia N° OE: <b>${data[i].NRORI}</b></p>`;
                }
                ficha += `<div class="info-atributos small" id="cuadroAtributo-${articuloId.replace(".", "-")}"></div>`;
            }
        };
        return ficha;
    }

    var crearImagenAtributosArticulosRelacionados = function (data) {
        var imagen = "";
        var url;
        if (data.length > 0) {
            for (let i = 0; i < data.length; i++) {
                if (data[i].NombreImagen.length > 0) {
                    url = `${apiUrl}catalogo/imagenesArticulosRelacionados?nombreImagen=${data[i].NombreImagen}`;
                    url2 = `${apiUrl}catalogo/imagenesArticulosRelacionados?nombreImagen=${data[i].NombreImagen.replace('.jpg', '_1.jpg')}`;
                    imagen += ` <a href="${url}&minimizado=false&esCabezaDeTornillo=false" target="_blank">        
                                <img src="${url}&minimizado=true&esCabezaDeTornillo=false" class="img-responsive" alt="">   
                            </a>
                            <a href="${url2}&minimizado=false&esCabezaDeTornillo=false" target="_blank">        
                                <img src="${url2}&minimizado=true&esCabezaDeTornillo=false" class="img-responsive" alt="">   
                            </a>`;
                }
            }
        }
        return imagen;
    }

    var obtenerDescripcionDeArticulosRelacionados = function (articuloId) {
        $.ajax({
            url: catalogoModel.apiUrl + "catalogo/articulosRelacionados?articuloId=" + articuloId,
            type: "GET",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            error: function () {
                div.alertaErrorDeApi.show();
            },
            success: function (data) {
                const tabla = crearTablaDetallesArticulosRelacionados(data, articuloId);
                $(`#cuadroDetalle-${articuloId.replace(".", "-").replace(" ", "")}`).html(tabla);
                const imagen = crearImagenAtributosArticulosRelacionados(data);
                $(`#imagenArticulo-${articuloId.replace(".", "-").replace(" ","")}`).html(imagen);
            }
        });
    };


    var crearTablaAtributosArticulosRelacionados = function (data) {
        var ficha = '<div class="info-atributos small">';
        if (data.length > 0) {
            for (let i = 0; i < data.length; i++) {
                if (data[i].Descripcion !== "FIGURA") {
                    ficha += `<p class="nobottommargin">${data[i].Descripcion}: <b> ${data[i].Valor}</b></p>`;
                } else {
                    ficha += `<div class="img-info-atributos mt-20">
                        <img src="${apiUrl}catalogo/imagenesArticulosRelacionados?nombreImagen=${data[i].Valor}
                        &minimizado=true&esCabezaDeTornillo=true" height="130">`;

                    const imagen2 = new Image();
                    imagen2.src = `${apiUrl}catalogo/imagenesArticulosRelacionados?nombreImagen=${data[i].Valor.replace('.jpg', '_1.jpg')}`;
                    if (imagen2.width !== 0) {
                        ficha += `<img src="${apiUrl}catalogo/imagenesArticulosRelacionados?nombreImagen=${data[i].Valor.replace('.jpg', '_1.jpg')}
                        &minimizado=true&esCabezaDeTornillo=true" height="130">`;
                    }
                    
                    ficha += '</div>';
                }
            }
        };
        ficha += "</div>";
        return ficha;
    }

    var obtenerDescripcionDeAtributosArticulosRelacionados = function (articuloId) {
        $.ajax({
            url: catalogoModel.apiUrl + "catalogo/atributosDeArticulosRelacionados?articuloId=" + articuloId,
            type: "GET",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            error: function () {
                div.alertaErrorDeApi.show();
            },
            success: function (data) {
                const tabla = crearTablaAtributosArticulosRelacionados(data);
                $(`#cuadroAtributo-${articuloId.replace(".", "-")}`).html(tabla);
            }
        });
    };

    var cambiarBotonAlAgregarArticulo = function() {
        return `<button type="button" class="button btn-add-to-cart added no-link fondo-azul">
					<i class="icon-cart"></i>
					<span><b>Agregado</b> <br>al carrito</span>
				</button>`;
    }


    var agregarAlCarrito = function (evento, usuario, articulo, cantidad) {
        const url = catalogoModel.apiLocalUrl + "catalogo/agregarArticuloAlCarrito";
        const datos = {
            Usuario: usuario,
            Articulo: articulo,
            Cantidad: cantidad
        };
        $.ajax({
            url: url,
            type: "post",
            data: datos,
            error: function () {
                alert("fallo");
                div.alertaErrorDeApi.show();
            },
            success: function () {
                var html = cambiarBotonAlAgregarArticulo();
                $(`#btn-${articulo.toString().replace(".", "-")}`).replaceWith(html);
                $("#badgeCantidadArticulosCarrito").text(parseInt($("#badgeCantidadArticulosCarrito").text()) || 0 + 1);
            }
        });
    }

    var obtenerValoresDelCotizador = function (articuloId) {
        $.ajax({
            url: catalogoModel.apiUrl + "perfil/parametrosParaPedido?clienteid=" + catalogoModel.usuario,
            type: "GET",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            fail: function () {
            },
            success: function (data) {
                if (data == null) {
                    ajustes_descuentoEspecial = 0;
                    ajustes_descuentoProntoPago = 0;
                    ajustes_margen = 0;
                    ajustes_iva = 0;
                } else {
                    ajustes_descuentoEspecial = data.DescuentoEspecial;
                    ajustes_descuentoProntoPago = data.DescuentoProntoPago;
                    ajustes_margen = data.Margen;
                    ajustes_iva = data.Iva;
                }

                obtenerProductoParaAgregarAlCarritoDeCompras(articuloId, ajustes_descuentoEspecial, ajustes_descuentoProntoPago, ajustes_margen, ajustes_iva)
            }
        });
    }
    var ActualizarPreciosCarrito = function (precioUnitario, descuentoEspecial, descuentoProntoPago, margen, iva) {
        if (descuentoEspecial > -1 || descuentoProntoPago > -1 || margen > -1 || iva > -1) {
            
            let nuevoValor = 0;

            if (descuentoEspecial > 0) {
                nuevoValor = (precioUnitario - ((descuentoEspecial / 100) * precioUnitario));
            } else if (descuentoEspecial == 0) {
                nuevoValor = precioUnitario;
            }

            if (descuentoProntoPago > 0) {
                if (nuevoValor > 0) {
                    nuevoValor = (nuevoValor - ((descuentoProntoPago / 100) * nuevoValor));
                } else {
                    nuevoValor = (precioUnitario - ((descuentoProntoPago / 100) * precioUnitario));
                }
            } else if (descuentoProntoPago == 0 && nuevoValor == 0) {
                nuevoValor = precioUnitario;
            }

            if (margen > 0) {
                if (nuevoValor > 0) {
                    nuevoValor = (nuevoValor + ((margen / 100) * nuevoValor));
                } else {
                    nuevoValor = (precioUnitario + ((margen / 100) * precioUnitario));
                }
            } else if (margen == 0 && nuevoValor == 0) {
                nuevoValor = precioUnitario;
            }

            if (iva > 0) {
                if (nuevoValor > 0) {
                    nuevoValor = (nuevoValor + ((iva / 100) * nuevoValor));
                } else {
                    nuevoValor = (precioUnitario + ((iva / 100) * precioUnitario));
                }
            } else if (iva == 0 && nuevoValor == 0) {
                nuevoValor = precioUnitario;
            }
            
            return nuevoValor.toFixed(2);
        }
    }

    var obtenerProductoParaAgregarAlCarritoDeCompras = function (articuloId, ajustes_descuentoEspecial, ajustes_descuentoProntoPago, ajustes_margen, ajustes_iva) {
        var html = "";
        $.ajax({
            url: catalogoModel.apiUrl + "catalogo/articulosParaCarrito?codigo=" + articuloId,
            type: "GET",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            fail: function () {
            },
            success: function (data) {
                for (let i = 0; i < data.length; i++) {
                    if (usuario === "") {
                        html = `<div class="cart-price">
		                            <div class="clear topmargin-xs"></div>
	                            </div>
	                            <div class="clear topmargin-xs"></div>													
	                            														
                            </div>`;
                    } else {
                        html = `<div class="cart-price">
		                            <b class="color-negro">Precio: $ <span>${ActualizarPreciosCarrito(data[i].PrecioFinal, ajustes_descuentoEspecial, ajustes_descuentoProntoPago, ajustes_margen, ajustes_iva)}</span></b>
		                            <div class="clear topmargin-xs"></div>
	                            </div>
	                            <div class="clear topmargin-xs"></div>													
	                            <button type="button" id="btn-${data[i].Codigo.replace(".", "-")}" class="button btn-add-to-cart btncarrito"  
                                    data-usuario="${catalogoModel.usuario}" data-articulo="${data[i].Codigo}">
		                            <i class="icon-cart"></i>
		                            <span><b>Agregar</b> <br>al carrito</span>
	                            </button>
                                <div class="clear topmargin-xs"></div>
                                <label for="quantity">Cantidad:</label>
                                <input type="number" id="cantidadArticulo-${data[i].Codigo.replace(".", "-")}" name="quantity" min="1" max="200" value="1">
                            </div>`;
                    }
                    
                    $(`#cuadroCarrito-${articuloId.replace(".", "-")}`).html(html);
                }
            }
        });
    }

    var generarDetalleDeArticuloRelacionados = function (motor, descripcion, marcaSeleccionado, contraer) {
        const logoArticulosRelacionados = `<div class="img-logo-marca">                                
                                <span class="parse-img-marca" data-img="${marcaSeleccionado}">                                   
                                    <img src="${apiUrl}catalogo/logoMarcas?marca=${marcaSeleccionado}">                                
                                </span>
                            </div>
                            <span>
                                <b>${marcaSeleccionado}</b>
                            </span>
                            <div class="clear"></div>`;

        return `<div class="toggle toggle-taranto" data-ficom="${motor}">                
                    <div class="togglet">
                        <p>${motor} <b>${descripcion}</b></p>                        
                        <i class="toggle-closed more-link-azul">Ver Detalle</i>                        
                        <i class="toggle-open more-link-red">Cerrar</i>                    
                    </div>                    
                    <div id="articulo-${motor.replace(".", "-")}" class="togglec" ${contraer ? 'style="display: none"' : ""}>                      
                        <div class="col_full tleft">    
                                ${(marcaSeleccionado.length > 0 ? logoArticulosRelacionados : "")}
                            <div class="col-sm-5 col-lg-5 topmargin-sm bottommargin-sm" id="cuadroMotor-${motor.replace(".", "-").replace(" ", "")}"></div> 
                            <div class="col-sm-4 col-lg-5 topmargin-sm bottommargin-sm">
                                <div class="img-info-detalle mb-20">                              
                                    <div id="imagenArticulo-${motor.replace(".", "-").replace(" ", "")}"></div>
                                </div>
                                <div id="cuadroDetalle-${motor.replace(".", "-").replace(" ", "")}"></div>
                                <div id="cuadroAtributo-${motor.replace(".", "-").replace(" ", "")}"></div> 
                                <div class="img-info-atributos text-center mt-20"></div>
                            </div>
                            <div class="col-sm-3 col-lg-2 topmargin-sm bottommargin-sm" id="cuadroCarrito-${motor.replace(".", "-").replace(" ", "")}"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>`;
    }

    var obtenerArticulosRelacionados = function (idComboMotor, idComboMarca, idComboFamilia) {
        div.cargaDeCombosSpinParaResultado.show();
        const motorSeleccionado = $(`#${idComboMotor} option:selected`).val();
        const marcaSeleccionado = $(`#${idComboMarca} option:selected`).val();
        const familiaSeleccionada = $(`#${idComboFamilia} option:selected`).val();
        var ficha = "";
        $.ajax({
            url: catalogoModel.apiUrl + "catalogo/fichaPorMotorYfamilaId?motor=" + motorSeleccionado + "&familiaId=" + familiaSeleccionada,
            type: "GET",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            error: function () {
                div.alertaErrorDeApi.show();
            },
            success: function (data) {
                if (data.length > 0) {
                    ficha += "<div class=\"clear\" style=\"padding-top: 40px;\"></div>";
                    for (let i = 0; i < data.length; i++) {
                        ficha += generarDetalleDeArticuloRelacionados(data[i].Motor, data[i].Descripcion, marcaSeleccionado, true);
                        obtenerCuadroDeDetalleDeMotoresDeArticulosRelacionados(data[i].Motor);
                        obtenerDescripcionDeArticulosRelacionados(data[i].Motor);
                        obtenerDescripcionDeAtributosArticulosRelacionados(data[i].Motor);
                        if(catalogoModel.esCliente) {
                            obtenerValoresDelCotizador(data[i].Motor);
                        }
                    }
                };
                div.cargaDeCombosSpinParaResultado.hide();
                div.catalogoArticulosRelacionados.html(ficha);
            }
        });
    }

    var obtenerArticulosDeSuspension = function (marca, modelo) {
        div.cargaDeCombosSpinParaResultado.show();
        
        var ficha = "";
        $.ajax({
            url: catalogoModel.apiUrl + "catalogo/articulosDeSuspension?marca=" + marca + "&modelo=" + modelo,
            type: "GET",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            error: function () {
                div.alertaErrorDeApi.show();
            },
            success: function (data) {
                if (data.length > 0) {
                    ficha += "<div class=\"clear\" style=\"padding-top: 40px;\"></div>";

                    for (let i = 0; i < data.length; i++) {
                        ficha += generarDetalleDeArticuloRelacionados(data[i].Codigo, data[i].Descripcion, marca, false);
                        obtenerCuadroDeDetalleDeMotoresDeArticulosRelacionados(data[i].Codigo);
                        obtenerDescripcionDeArticulosRelacionados(data[i].Codigo);
                        obtenerDescripcionDeAtributosArticulosRelacionados(data[i].Codigo);
                        div.catalogoArticulosRelacionados.html(ficha);
                        if (catalogoModel.esCliente) {
                            obtenerValoresDelCotizador(data[i].Codigo);
                        }
                    }
                };
                div.cargaDeCombosSpinParaResultado.hide();
                div.catalogoArticulosRelacionados.html(ficha);
            }
        });
    }

    var limpiar = function () {
        div.cantidadResultadoBusquedaAvanzada.empty();
        div.catalogoCabecera.empty();
        div.catalogoDetalle.empty();
        div.cuadro.empty();
        div.catalogoArticulosRelacionados.empty();
        div.paginado.empty();
    }

    var obtenerArticuloPorCodigo = function (evento, codigo) {
        limpiar();
        $("#seccionPorCodigoApliqueTodosLosFiltros").addClass("hide");
        const sinMarcaSeleccionada = "";
        var ficha = "";
        $.ajax({
            url: catalogoModel.apiUrl + "catalogo/articulosPorCodigo?codigo=" + codigo,
            type: "GET",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            error: function () {
                div.alertaErrorDeApi.show();
            },
            success: function (data) {
                if (data.length > 0) {
                    for (let i = 0; i < data.length; i++) {
                        ficha += generarDetalleDeArticuloRelacionados(codigo, data[i].Descripcion, sinMarcaSeleccionada, false);
                        obtenerCuadroDeDetalleDeMotoresDeArticulosRelacionados(codigo);
                        obtenerDescripcionDeArticulosRelacionados(codigo);
                        obtenerDescripcionDeAtributosArticulosRelacionados(codigo);
                        div.catalogoArticulosRelacionados.html(ficha);
                        obtenerValoresDelCotizador(codigo);
                    }
                };
                div.catalogoArticulosRelacionados.html(ficha);
            }
        });
    };

    var generarPaginado = function (cantidadDeResultados, numeroDePagina) {
        var paginas = "";

        for (var i = 0; i * 50 <= cantidadDeResultados; i++) {
            if (i === 0) {
                paginas += `<li class="active"><a href="#">1</a></li>`;
            }
            else if (i + 1 === parseInt(numeroDePagina)) {
                paginas += `<li class="active"><a href="#">${i + 1}</a></li>`;
            } 
            else {
                paginas += `<li><a href="#">${i + 1}</a></li>`;
            }
        }

        var html = `<nav aria-label="...">
                        <ul class="pagination">
                            ${paginas}
                        </ul>
                    </nav>`;

        return div.paginado.html(html);

    }

    var obtenerArticuloPorTexto = function (evento, texto, numeroDePagina) {
        limpiar();
        div.cargaDeCombosSpinParaResultado.show();
        $("#seccionPorCodigoApliqueTodosLosFiltros").addClass("hide");
        const sinMarcaSeleccionada = "";
        var ficha = "";
        $.ajax({
            url: catalogoModel.apiUrl + `catalogo/busquedaRapidaPorTexto?texto=${texto}&numeroDePagina=${numeroDePagina}`,
            type: "GET",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            error: function () {
                div.alertaErrorDeApi.show();
            },
            success: function (data) {
                if (data.length > 0) {
                    for (let i = 0; i < data.length; i++) {
                        ficha += generarDetalleDeArticuloRelacionados(data[i].Codigo, data[i].Descripcion, sinMarcaSeleccionada, true);
                        obtenerCuadroDeDetalleDeMotoresDeArticulosRelacionados(data[i].Codigo);
                        obtenerDescripcionDeArticulosRelacionados(data[i].Codigo);
                        obtenerDescripcionDeAtributosArticulosRelacionados(data[i].Codigo);
                        generarPaginado(data.length, numeroDePagina);
                        div.catalogoArticulosRelacionados.html(ficha);
                        obtenerValoresDelCotizador(data[i].Codigo);
                    }
                    var html = `<div class="cant-resultados text-left mb-30">Se han encontrado <strong>${data.length}</strong> resultados para <strong>"${texto}"</strong>.</div>`;
                    div.cantidadResultadoBusquedaAvanzada.html(html);
                };
                div.cargaDeCombosSpinParaResultado.hide();
                div.catalogoArticulosRelacionados.html(ficha);
            }
        });
    };

    var obtenerArticulosPorFiltros = function (evento, numeroDePagina) {
        limpiar();
        div.cargaDeCombosSpinParaResultado.show();
        $("#seccionPorCodigoApliqueTodosLosFiltros").addClass("hide");
        const marcaSeleccionada = $("#cboMarcaSeccionPorAvanzada option:selected").val().toUpperCase().trim();
        const modelo = $("#textBoxModeloSeccionPorAvanzada").val().trim().toUpperCase();
        const cilindrada = $("#textBoxCilindradaSeccionPorAvanzada").val().trim();
        const motor = $("#textBoxMotorSeccionPorAvanzada").val().trim().toUpperCase();
        const cilindros = $("#textBoxCilindrosSeccionPorAvanzada").val().trim();
        const valvulas = $("#textBoxValvulasSeccionPorAvanzada").val().trim();
        const combustible = $("#cboCombustibleSeccionPorAvanzada option:selected").val().toUpperCase().trim();

        var ficha = "";
        if (!numeroDePagina) {
            numeroDePagina = 1;
        }
        $.ajax({
            url: catalogoModel.apiUrl +
                `catalogo/busquedaAvanzadaPorFiltros?marca=${marcaSeleccionada}&modelo=${modelo}&cilindrada=${cilindrada}&motor=${motor}&cilindros=${cilindros}&valvulas=${valvulas}&combustible=${combustible}&numeroDePagina=${numeroDePagina}`,
            type: "GET",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            error: function () {
                div.alertaErrorDeApi.show();
            },
            success: function (data) {
                if (data.length > 0) {
                    for (let i = 0; i < data.length; i++) {
                        ficha += generarDetalleDeArticuloRelacionados(data[i].Codigo, data[i].Descripcion, marcaSeleccionada, true);
                        obtenerCuadroDeDetalleDeMotoresDeArticulosRelacionados(data[i].Codigo);
                        obtenerDescripcionDeArticulosRelacionados(data[i].Codigo);
                        obtenerDescripcionDeAtributosArticulosRelacionados(data[i].Codigo);
                        generarPaginado(data.length, numeroDePagina);
                        div.catalogoArticulosRelacionados.html(ficha);
                    }
                    var html = `<div class="cant-resultados text-left mb-30">Se han encontrado <strong>${data.length}</strong> resultados para <strong>"${marcaSeleccionada}"</strong>.</div>`;
                    div.cantidadResultadoBusquedaAvanzada.html(html);
                };
                div.cargaDeCombosSpinParaResultado.hide();
                div.catalogoArticulosRelacionados.html(ficha);
            }
        });
    };

    var obtenerCatalogo = function (evento, idComboMotor, idComboMarca, idComboFamilia) {
        limpiar();
        const familiaSeleccionada = $(`#${idComboFamilia} option:selected`).val();
        if (familiaSeleccionada === "13") {
            const modeloSeleccionado = $("#cboModeloSeccionPorMarca option:selected").val();
            const marcaSeleccionada = $(`#${idComboMarca} option:selected`).val();
            obtenerArticulosDeSuspension(marcaSeleccionada, modeloSeleccionado);
        } else {
            obtenerCuadroDeModelosPorMarca(idComboMotor);
            obtenerCuadroDeDetalleDeMotores(idComboMotor);
            obtenerArticulosRelacionados(idComboMotor, idComboMarca, idComboFamilia);
        }
    }

    var establecerEventos = function () {
        catalogoModel.doc.on(eventos.obtenerCatalogo, obtenerCatalogo);
        catalogoModel.doc.on(eventos.clickBuscarSeccionCodigo, obtenerArticuloPorCodigo);
        catalogoModel.doc.on(eventos.clickBusquedaRapidaSeccionAvanzada, obtenerArticuloPorTexto);
        catalogoModel.doc.on(eventos.clickBusquedaAvanzadaSeccionAvanzada, obtenerArticulosPorFiltros);
        catalogoModel.doc.on(eventos.clickAgregarAlCarrito, agregarAlCarrito);

    }

    var inicializar = function () {
        obtenerTodasLasMarcasParaCbo();
        div.cargaDeCombosSpin.hide();
        div.cargaDeCombosSpinParaResultado.hide();
        establecerEventos();
    }

    return {
        inicializar: inicializar
    }
})();