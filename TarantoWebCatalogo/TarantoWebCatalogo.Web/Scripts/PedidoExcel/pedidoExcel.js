﻿var eliminarPedido = function (usuario) {
    const url = apiLocalUrl + "catalogo/eliminarPedidoExcel";
    const datos = {
        Usuario: usuario
    };

    $.ajax({
        url: url,
        type: "POST",
        data: datos,
        error: function (event, response) {
            console.log(response);
        },
        success: function (event, response) {
            console.log("Eliminado: " + event + " - " + response);
        }
    });
}

$("#btnValidarExcel").on("click",
    function() {
        $("#validarExcelSpin").removeClass('hide');
    });

$("#btnConfirmarPedido").on("click", function () {
    $("#spinConfirmarExcel").removeClass('hide');
    const url = apiLocalUrl + "catalogo/crearPedidoDesdeExcel";
    const datos = {
        Usuario: usuario,
        Observaciones: $("#txtObservacionPedido").val(),
        Articulos: []
    };
    $('#tablaExcelPedidoConfirmar > tbody  > tr').each(function (index) {
        var $tds = $(this).find("td");
        var articulosDato = 
            {
                articulo: $tds.eq(0).text(),
                descripcion: $tds.eq(1).text(),
                cantidad: parseInt($tds.eq(3).text()),
                precioUnitario: parseFloat(($tds.eq(2).text().replace("$", "").replace(" ", ""))),
                stock: $tds.eq(5).text()
            };

        datos.Articulos.push(articulosDato);
    });
    JSON.stringify(datos);

    $.ajax({
        url: url,
        type: "POST",
        data: datos,
        error: function () {
            eliminarPedido(usuario);
            $("#spinConfirmarExcel").addClass('hide');
            var url = 'PedidoPorExcel?processStatus=2';
            window.location.href = url;
        },
        success: function () {
            $("#spinConfirmarExcel").addClass('hide');
            $("#modalPedidoExcelValidado").addClass('hide');
            var url = 'PedidoPorExcel?processStatus=1';
            window.location.href = url;
        }
    });
});
