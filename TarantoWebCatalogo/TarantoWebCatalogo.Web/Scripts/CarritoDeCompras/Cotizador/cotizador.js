﻿var ajustes_descuentoEspecial;
var ajustes_descuentoProntoPago;
var ajustes_margen;
var ajustes_iva;

$(function () {
    $.ajax({
        url: apiUrl + "perfil/parametrosParaPedido?clienteid=" + usuario,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        error: function () {
            div.alertaErrorDeApi.show();
        },
        success: function (data) {
            if (data == null) {
                ajustes_descuentoEspecial = 0;
                ajustes_descuentoProntoPago = 0;
                ajustes_margen = 0;
                ajustes_iva = 0;
            } else {
                ajustes_descuentoEspecial = data.DescuentoEspecial;
                ajustes_descuentoProntoPago = data.DescuentoProntoPago;
                ajustes_margen = data.Margen;
                ajustes_iva = data.Iva;
            }

            setearValoresCotizador();
        }
    });
});


var setearValoresCotizador = function () {
    if (ajustes_descuentoEspecial != undefined) {
        $("#descuentoEspecial").val(ajustes_descuentoEspecial);
    }
    if (ajustes_descuentoProntoPago != undefined) {
        $("#descuentoProntoPago").val(ajustes_descuentoProntoPago);
    }
    if (ajustes_margen != undefined) {
        $("#margen").val(ajustes_margen);
    }
    if (ajustes_iva != undefined) {
        $("#iva").val(ajustes_iva);
    }

    $("#settings_cart_modo_0").on("click", function () {
        $("#parametros-cotizador-clientes").removeClass("mb-20").addClass("mb-20 div-disabled");
    });

    $("#settings_cart_modo_1").on("click", function () {
        $("#parametros-cotizador-clientes").removeClass("mb-20 div-disabled").addClass('mb-20');
    });


    if (ajustes_descuentoEspecial != null || ajustes_descuentoProntoPago != null || ajustes_margen != null || ajustes_iva != null) {
        $("#settings_cart_modo_1")[0].checked = true
        $("#parametros-cotizador-clientes").removeClass("mb-20 div-disabled").addClass('mb-20');
    }

    if (ajustes_descuentoEspecial == 0 || ajustes_descuentoEspecial == null) { ajustes_descuentoEspecial = 0 };
    if (ajustes_descuentoProntoPago == 0 || ajustes_descuentoProntoPago == null) { ajustes_descuentoProntoPago = 0 };
    if (ajustes_margen == 0 || ajustes_margen == null) { ajustes_margen = 0 };
    if (ajustes_iva == 0 || ajustes_iva == null) { ajustes_iva = 0 };

    ActualizarPreciosCarrito(ajustes_descuentoEspecial, ajustes_descuentoProntoPago, ajustes_margen, ajustes_iva);
}

function ActualizarPreciosCarrito(descuentoEspecial, descuentoProntoPago, margen, iva) {

    if (descuentoEspecial > -1 || descuentoProntoPago > -1 || margen > -1 || iva > -1) {

        var nuevoTotal = 0;

        $('#tablaPedido > tbody  > tr').each(function (index) {
            $("#precioCliente-" + index).removeClass();
            let columnaPrecioUnitario = $("#precioUnitario-" + index);
            let precioUnitario = parseFloat(columnaPrecioUnitario.text().replace(",", "."))
            let nuevoValor = 0;

            if (descuentoEspecial > 0) {
                nuevoValor = (precioUnitario - ((descuentoEspecial / 100) * precioUnitario));
            } else if (descuentoEspecial == 0) {
                nuevoValor = precioUnitario;
            }

            if (descuentoProntoPago > 0) {
                if (nuevoValor > 0) {
                    nuevoValor = (nuevoValor - ((descuentoProntoPago / 100) * nuevoValor));
                } else {
                    nuevoValor = (precioUnitario - ((descuentoProntoPago / 100) * precioUnitario));
                }
            } else if (descuentoProntoPago == 0 && nuevoValor == 0) {
                nuevoValor = precioUnitario;
            }

            if (margen > 0) {
                if (nuevoValor > 0) {
                    nuevoValor = (nuevoValor + ((margen / 100) * nuevoValor));
                } else {
                    nuevoValor = (precioUnitario + ((margen / 100) * precioUnitario));
                }
            } else if (margen == 0 && nuevoValor == 0) {
                nuevoValor = precioUnitario;
            }

            if (iva > 0) {
                if (nuevoValor > 0) {
                    nuevoValor = (nuevoValor + ((iva / 100) * nuevoValor));
                } else {
                    nuevoValor = (precioUnitario + ((iva / 100) * precioUnitario));
                }
            } else if (iva == 0 && nuevoValor == 0) {
                nuevoValor = precioUnitario;
            }

            let columnaPrecioCliente = $("#precioCliente-" + index);
            columnaPrecioCliente.text(nuevoValor.toFixed(2));

            var cantidad = parseInt($("#cantidad-" + index).text());
            $("#subtotal-" + index).text("$" + (nuevoValor * cantidad).toFixed(2));

            if (cantidad > 0) { nuevoTotal += (nuevoValor * cantidad); }

        });
        var total = nuevoTotal.toFixed(2);
        $("#filaTotalEnDetallePedido").text("$" + total);
    }
}

$("#btnCotizadorGuardar").on("click", function () {
    var descuentoEspecial;
    var descuentoProntoPago;
    var margen;
    var iva;

    if ($("#parametros-cotizador-clientes").hasClass("mb-20 div-disabled")) {
        descuentoEspecial = 0;
        descuentoProntoPago = 0;
        margen = 0;
        iva = 0;
    } else {
        descuentoEspecial = parseFloat($("#descuentoEspecial").val());
        descuentoProntoPago = parseFloat($("#descuentoProntoPago").val());
        margen = parseFloat($("#margen").val());
        iva = parseFloat($("#iva").val());
    }

    if (descuentoEspecial == 0 || isNaN(descuentoEspecial)) { descuentoEspecial = 0 };
    if (descuentoProntoPago == 0 || isNaN(descuentoProntoPago)) { descuentoProntoPago = 0 };
    if (margen == 0 || isNaN(margen)) { margen = 0 };
    if (iva == 0 || isNaN(iva)) { iva = 0 };

    actualizarValoresCotizador(usuario, descuentoEspecial, descuentoProntoPago, margen, iva)

    ActualizarPreciosCarrito(descuentoEspecial, descuentoProntoPago, margen, iva);

    $('#modalCotizador').modal('toggle');

});

var actualizarValoresCotizador = function (usuario, descuentoEspecial, descuentoProntoPago, margen, iva) {
    const url = apiUrl + "perfil/insertarParametrosPedido";
    const datos = {
        ClienteId: usuario,
        DescuentoEspecial: descuentoEspecial,
        DescuentoProntoPago: descuentoProntoPago,
        Margen: margen,
        Iva: iva
    };
    $.ajax({
        url: url,
        type: "post",
        data: datos,
        error: function () {
        },
        success: function () {
        }
    });
}