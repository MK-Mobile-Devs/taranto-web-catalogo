﻿$(function () {
    var divs = carritoModel.divs();
    var eventos = carritoModel.eventos();
    var botones = carritoModel.botones();
    var textboxes = carritoModel.textboxes();
    var modales = carritoModel.modales();
    var articulo;
    var idPedido;

    const articulos = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: carritoModel.apiUrl + "catalogo/articulosPorCodigo?codigo=%QUERY",
            wildcard: '%QUERY'
        }
    });

    const articulosPorDescripcion = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: carritoModel.apiUrl + "catalogo/articulospordescripcion?descripcion=%QUERY",
            wildcard: '%QUERY'
        }
    });

    $('#scrollable-dropdown-menu .typeahead-codigo').typeahead(null, {
        name: 'Codigo',
        limit: 24,
        display: 'Codigo',
        source: articulos
    });

    $('#scrollable-dropdown-menu .typeahead-codigo').bind('typeahead:select', function (ev, suggestion) {
        carritoModel.doc.trigger(eventos.codigoSugeridoSeleccionado, suggestion);
        event.preventDefault();
    });

    $('#scrollable-dropdown-menu .typeahead-descripcion').typeahead(null, {
        name: 'Descripcion',
        limit: 14,
        display: 'Descripcion',
        source: articulosPorDescripcion
    });

    $('#scrollable-dropdown-menu .typeahead-descripcion').bind('typeahead:select', function (ev, suggestion) {
        carritoModel.doc.trigger(eventos.descripcionSeleccionada, suggestion);
        event.preventDefault();
    });



    divs.cantidad.on("change", function(event) {
        carritoModel.doc.trigger(eventos.modificaLaCantidadDeArticulos);
        event.preventDefault();
    });

    modales.editarCantidadArticulos.on("show.bs.modal", function (event) {
        var cantidadActual = $(event.relatedTarget).data('cantidad');
        articulo = $(event.relatedTarget).data('articulo');
        idPedido = $(event.relatedTarget).data('idpedido');
        $(this).find("#spanCantidadActual").text(cantidadActual);
        textboxes.cantidadDeArticulosEdit.focus();
    });

    botones.actualizarCantidad.on("click", function (event) {
        carritoModel.doc.trigger(eventos.clickBotonActualizarCantidad, [articulo, idPedido]);
    });
    
    botones.btnConfirmarPedido.on("click", function (event) {
        carritoModel.doc.trigger(eventos.clickBtnConfirmarPedido);
        event.preventDefault();
    });

    botones.agregarArticulo.on("click", function (event) {
        carritoModel.doc.trigger(eventos.clickBtnAgregarCarrito);
        event.preventDefault();
    });

    carritoManager.inicializar();
});
