﻿var carritoManager = (function () {
    var eventos = carritoModel.eventos();
    var textboxes = carritoModel.textboxes();
    var divs = carritoModel.divs();
    var modales = carritoModel.modales();
    var campoOculto = carritoModel.camposOcultos();
    var botones = carritoModel.botones();

    var obtenerDetallesDelArticuloSeleccionado = function (codigo) {
        botones.agregarArticulo.prop("disabled", true);
        divs.cargaDeCombosSpin.show();
        $.ajax({
            url: carritoModel.apiUrl + "catalogo/articulosParaCarrito?codigo=" + codigo,
            type: "GET",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            fail: function () {
            },
            success: function (data) {
                var descripcion = "";
                var subtotal = "";
                var stock = "";
                for (let i = 0; i < data.length; i++) {
                    descripcion = `${data[i].Descripcion}`;
                    stock = `<span class="stock-${data[i].Stock.toLowerCase()}"></span>`;
                    subtotal = `<label class="control-label" id="lblSubtotal">${formatearMoneda(data[i].PrecioFinal)}</label>`;
                    campoOculto.precioUnitario.val(data[i].PrecioFinal);
                    campoOculto.descripcion.val(data[i].Descripcion);
                    campoOculto.stock.val(data[i].Stock);
                    if (data[i].Stock === "ROJO") {
                        subtotal = `<label class="control-label" id="lblSubtotal">${formatearMoneda(0)}</label>`;
                    }
                }
                $("#descripcion").html(descripcion);
                $("#subtotal").html(subtotal);
                $("#stock").html(stock);
                botones.agregarArticulo.prop("disabled", false);
                divs.cargaDeCombosSpin.hide();

            }
        });
    }

    var editarCantidadDeArticulo = function (evento, articulo, idPedido) {
        divs.editarCantidadDeArticulosSpin.show();
        var cantidadDeArticulo = parseInt($("#txtCantidadDeArticulos").val());
        const url = carritoModel.apiLocalUrl + "catalogo/pedidosEditarCantidadArticulo";
        const data = {
            Usuario: carritoModel.usuario,
            Articulo: articulo,
            Cantidad: cantidadDeArticulo,
            IdPedido: idPedido
        };
        $.ajax({
            type: "patch",
            url: url,
            data: data
        }).done(function () {
            divs.editarCantidadDeArticulosSpin.hide();
            modales.editarCantidadArticulos.modal("hide");
            location.reload();
        });
    }

    var actualizarElPrecio = function () {
        if (campoOculto.stock.val() !== "ROJO") {
            var cantidad = $("#cantidad").val();
            var total = campoOculto.precioUnitario.val().replace(",", ".");
            var resultado = total * cantidad;
            $("#lblSubtotal").text(formatearMoneda(resultado));
        }
    }

    var procesarPedido = function () {
        $("#btnConfirmarPedido").prop('disabled', true);
        $("#realizarPedidoSpin").show();
        const url = carritoModel.apiLocalUrl + "catalogo/pedidoProcesado";
        const datos = {
            IdPedido: parseInt($("#idPedidoSql").val()),
            Observacion: $("#txtObservacionPedido").val(),
            Usuario: usuario
        }

        $.ajax({
            url: url,
            type: "Patch",
            data: datos,
            error: function () {
                alert("Ha ocurrido un error, intente mas tarde")
                $("#btnConfirmarPedido").prop('disabled', false);
                $("#realizarPedidoSpin").hide();
            },
            success: function () {
                $("#realizarPedidoSpin").hide();
                location.reload();
            }
        });
    }

    var cargarArticulo = function () {
        $("#cargaSpinAgregarCarrito").removeClass();
        const url = carritoModel.apiLocalUrl + "catalogo/agregarArticuloAlCarrito";
        const datos = {
            Usuario: usuario,
            Articulo: textboxes.articulo.val(),
            Descripcion: textboxes.descripcion.val(),
            Cantidad: parseInt($("#cantidad").val()),
            PrecioUnitario: $("#campoOcultoPrecioUnitario").val(),
            Stock: $("#campoOcultoStock").val()
        };

        $.ajax({
            url: url,
            type: "POST",
            data: datos,
            error: function (event, response) {
                console.log(response);
            },
            success: function (event, response) {
                console.log("Guardado: " + event + " - " + response);
                location.reload();
            }
        });
    }

    var reemplazarCodigoEscritoPorElSeleccionado = function (evento, articulo) {
        textboxes.articulo.val(articulo.Codigo);
        textboxes.descripcion.val(articulo.Descripcion);
        obtenerDetallesDelArticuloSeleccionado(articulo.Codigo);
    }
    var descripcionSeleccionada = function (evento, articulo) {
        textboxes.articulo.val(articulo.Codigo);
        textboxes.descripcion.val(articulo.Descripcion);
        
        obtenerDetallesDelArticuloSeleccionado(articulo.Codigo);
    }

    var verificarUsuarioBloqueado = function () {
        $.ajax({
            url: apiUrl + "perfil/cuentaCorrienteEncabezado?clienteid=" + usuario,
            type: "GET",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.length > 0) {
                    if (data[0].Estado == "Bloqueado") {
                        $("#lblUsuarioBloqueado").removeClass();
                        $("#btnConfirmarPedido").hide();
                        $("#txtObservaciones").hide();
                    }
                    
                } 
                $("#realizarPedidoSpin").hide();
                $("#divHabilitarPedido").removeClass();
            }            
        });
    };

    var establecerEventos = function () {
        carritoModel.doc.on(eventos.codigoSugeridoSeleccionado, reemplazarCodigoEscritoPorElSeleccionado);
        carritoModel.doc.on(eventos.descripcionSeleccionada, descripcionSeleccionada);
        carritoModel.doc.on(eventos.modificaLaCantidadDeArticulos, actualizarElPrecio);
        carritoModel.doc.on(eventos.clickBotonActualizarCantidad, editarCantidadDeArticulo);
        carritoModel.doc.on(eventos.clickBtnConfirmarPedido, procesarPedido);
        carritoModel.doc.on(eventos.clickBtnAgregarCarrito, cargarArticulo);
    }

    var inicializar = function () {
        botones.agregarArticulo.prop("disabled", true);
        divs.cargaDeCombosSpin.hide();
        divs.editarCantidadDeArticulosSpin.hide();
        establecerEventos();
        verificarUsuarioBloqueado();
        
    }

    return {
        inicializar: inicializar
    }

})();

function formatearMoneda(num) {
    return "$ " + (num
        .toFixed(2) // always two decimal digits
        .replace('.', ',') // replace decimal point character with ,
        .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    ) // use . as a separator
}