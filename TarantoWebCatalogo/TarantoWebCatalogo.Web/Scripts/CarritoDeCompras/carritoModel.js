﻿var carritoModel = (function () {
    var textboxes = function () {
        return {
            articulo: $("#textBoxCodigo"),
            descripcion: $("#textboxDescripcion"),
            cantidadDeArticulosEdit: $("#txtCantidadDeArticulos"),
            txtObservacionPedido: $("txtObservacionPedido")
        }
    }

    var divs = function() {
        return {
            cargaDeCombosSpin: $("#cargaDeCombosSpin"),
            codigosSurgeridos: $(".tt-dataset"),
            cantidad: $("#cantidad"),
            editarCantidadDeArticulosSpin: $("#editarCantidadDeArticulosSpin"),
            cargaSpinAgregarCarrito: $("#cargaSpinAgregarCarrito")
        }
    }
    
    var eventos = function () {
        return {
            codigoAfiltrarModificado: "codigoAfiltrarModificado",
            codigoSugeridoSeleccionado: "codigoSugeridoSeleccionado",
            descripcionSeleccionada: "descripcionSeleccionada",
            modificaLaCantidadDeArticulos: "modificaLaCantidadDeArticulos",
            mostrarModalEditarCantidadDeArticulos: "mostrarModalEditarCantidadDeArticulos",
            clickBotonActualizarCantidad: "clickBotonActualizarCantidad",
            clickBtnConfirmarPedido: "clickBtnConfirmarPedido",
            clickBtnAgregarCarrito: "clickBtnAgregarCarrito"
        }
    }

    var camposOcultos = function() {
        return {
            descripcion: $("#campoOcultoDescripcion"),
            precioUnitario: $("#campoOcultoPrecioUnitario"),
            stock: $("#campoOcultoStock")
        }
    }

    const modales = function () {
        return {
            editarCantidadArticulos: $("#modalEditarCantidadArticulos")
        }
    };

    var botones = function() {
        return {
            actualizarCantidad: $("#btnActualizarCantidad"),
            agregarArticulo: $("#btnAgregarArticulo"),
            agregaArticulo: $("#botonAgregarArticulo"),
            btnConfirmarPedido: $("#btnConfirmarPedido")
        }
    }

    return {
        doc: $(document),
        textboxes: textboxes,
        divs: divs,
        camposOcultos: camposOcultos,
        apiUrl: apiUrl,
        apiLocalUrl: apiLocalUrl,
        baseUrl: baseUrl,
        usuario: usuario,
        modales: modales,
        botones: botones,
        eventos: eventos
    }
})();