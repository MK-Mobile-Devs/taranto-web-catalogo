﻿var seccion = window.location.pathname.split("/")[2];
var li;

switch (seccion) {
    case "Catalogo":
        li = document.getElementById("seccionCatalogo");
        li.classList.add("current");
        break;
    case "Productos":
        li = document.getElementById("seccionProductos");
        li.classList.add("current");
        break;
    case "Distribuidores":
        li = document.getElementById("seccionDistribuidores");
        li.classList.add("current");
        break;
    case "Empresa":
        li = document.getElementById("seccionEmpresa");
        li.classList.add("current");
        break;
    case "Novedades":
        li = document.getElementById("seccionNovedades");
        li.classList.add("current");
        break;
    case "Contacto":
        li = document.getElementById("seccionContacto");
        li.classList.add("current");
        break;
    default:
        li = document.getElementById("seccionHome");
        li.classList.add("current");
    break;
}