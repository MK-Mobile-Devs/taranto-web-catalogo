(function($) {
  "use strict";

  // page scrolling feature
  $('a.page-scroll').on('click', function(event) {
    var $anchor = $(this);
    $('html, body').stop().animate({
      scrollTop: ($($anchor.attr('href')).offset().top - 50)
    }, 800);
    event.preventDefault();
  });

  // Closes the responsive menu on menu item click
  $('#navbar ul li a').click(function() {
    $('#navbar .navbar-toggle:visible').click();
  });

  // Highlight the top nav as scrolling occurs
  $('body').scrollspy({
    target: '#navbar',
    offset: 51
  })

  // Offset for main navigation
  $('#navbar').affix();

  // Initialize WOW.js scrolling animations
  new WOW().init();
})(jQuery);
