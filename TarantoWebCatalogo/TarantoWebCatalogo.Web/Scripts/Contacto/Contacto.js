﻿$(function () {
    $("#btnContact").on("click", function (evento) {
        $("#spinEnviarEmail").removeClass('hide');
        $.ajax({
            url: "/Contacto/EnvioMailContacto",
            type: "POST",
            data: function () {
                var data = new FormData();
                data.append("motivo", $("#contact_subject").val());
                data.append("sede", $("#contact_country").val());
                data.append("nombreApellido", $("#contact_name").val());
                data.append("email", $("#contact_email").val());
                data.append("telefono", $("#contact_tel").val());
                data.append("consulta", $("#contact_message").val());
                return data;
            }(),
            contentType: false,
            processData: false,
            success: function (response) {
                alert("Mail enviado");
                $("#spinEnviarEmail").addClass('hide');
            },
            error: function (errorMessage) {
                $("#spinEnviarEmail").addClass('hide');
            }
        });
    });
});