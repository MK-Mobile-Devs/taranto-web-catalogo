﻿$(function () {
    $("#btnUserEdit").on("click", function (evento) {
        $.ajax({
            url: "/Perfil/CargarFotosPuntoVenta",
            type: "POST",
            data: function () {
                var data = new FormData();
                data.append("clienteId", clienteId);
                data.append("Comentarios", jQuery("#user_edit_message").val());
                data.append("ImagenPrincipal", jQuery("#user_edit_img").get(0).files[0]);

                var files = jQuery("#user_edit_more_img").get(0).files;

                for (var i = 0; i < files.length; i++) {
                    data.append("MoreImages", files[i]);
                }  
                
                return data;
            }(),
            contentType: false,
            processData: false,
            success: function (response) {
                $('#modal-editar-mis-datos').modal('toggle');
            },
            error: function (errorMessage) {
                $("#errorMessage").html("Error al subir imagenes");
            }
        });
    });
});