﻿$(function () {
    $("#btnPasswordEdit").on("click", function (evento) {
        debugger;
        $.ajax({
            url: "/Perfil/CambiarContrasena",
            type: "POST",
            data: function () { 

                var data = new FormData();
                data.append("clienteId", clienteId);
                data.append("oldPassword", jQuery("#password_edit_oldpass").val());
                data.append("newPassword", jQuery("#password_edit_newpass1").val());
                return data;
            }(),
            contentType: false,
            processData: false,
            success: function (response) {
                if (response === "True") {
                    $('#modal-cambiar-mi-contrasena').modal('toggle');
                    location.reload();
                } else {
                    $("#errorMessage").html("Error al cambiar contrasena. Verifique los datos ingresados");
                }
                
            },
            error: function (errorMessage) {
                $("#errorMessage").html("Error al cambiar contrasena");
            }
        });
    });
});
