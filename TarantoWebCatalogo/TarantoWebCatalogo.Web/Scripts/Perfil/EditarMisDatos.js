﻿$(function () {
    $("#btnUserEdit").on("click", function (evento) {
        $.ajax({
            url: "/Perfil/CambiarMisDatos",
            type: "POST",
            data: function () {
                var data = new FormData();
                data.append("clienteId", clienteId);
                data.append("NombreDelComercio", jQuery("#nombreComercio").val());
                data.append("RazonSocial", jQuery("#razonSocial").val());
                data.append("DireccionEntregaTaranto", jQuery("#direccionEntrega").val());
                data.append("TelefonoContactoTaranto", jQuery("#telefonoContacto").val());
                data.append("EmailContactoAdministrativo", jQuery("#emailContacto").val());
                data.append("Sucursal", jQuery("#sucursal").val());
                data.append("DireccionDelComercio", jQuery("#direccionComercio").val());
                data.append("TelefonoDelComercio", jQuery("#telefonoComercio").val());
                data.append("Email", jQuery("#email").val());
                data.append("DiasYHorarios", jQuery("#diasHorarios").val());
                data.append("Comentarios", jQuery("#comentarios").val());
                return data;
            }(),
            contentType: false,
            processData: false,
            success: function (response) {
                $('#modal-editar-mis-datos').modal('toggle');
            },
            error: function (errorMessage) {
                $("#errorMessage").html("Error al modificar datos");
            }
        });
    });
});
