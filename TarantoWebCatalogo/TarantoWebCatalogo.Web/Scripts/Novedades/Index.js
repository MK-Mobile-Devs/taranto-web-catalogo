﻿$(function () {
    $("#IdCategoria").on("change", function (e) {
        e.preventDefault();

        const url = document.URL.replace("#", "");
        const indiceSignoPregunta = document.URL.indexOf('?');
        var reloadUrl;
        if (indiceSignoPregunta === -1) {
            reloadUrl = url + "?idCategoria=" + $("#IdCategoria option:selected").val();
        } else {
            reloadUrl = url.substring(0, indiceSignoPregunta) + "?idCategoria=" + $("#IdCategoria option:selected").val();
        }
        
        window.location = reloadUrl;
    });
});