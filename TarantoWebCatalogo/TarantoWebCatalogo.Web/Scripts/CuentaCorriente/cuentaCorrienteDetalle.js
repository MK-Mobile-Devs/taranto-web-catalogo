﻿function separadorDeUnidades(input) {
    var output = input;
    if (parseFloat(input)) {
        input = new String(input);
        var parts = input.split(".");
        parts[0] = parts[0].split("").reverse().join("").replace(/(\d{3})(?!$)/g, "$1.").split("").reverse().join("");
        output = parts.join(",");
    }

    return output;
}

document.addEventListener('DOMContentLoaded', function () {
    cargarEncabezado();
}, false);


var generarTabla = function (linea, articuloId, articuloDescripcion, cantidadPedida, subtotal) {

    return `<tr>
                <td class="tcenter"><b class="small">${separadorDeUnidades(linea)}</b></td>
                <td class="tleft"><span class="color-rojo">${articuloId}</span></td>
                <td class="tleft hidden"><i></i></td>
                <td class="tleft"><i>${articuloDescripcion}</i></td>
                <td class="tcenter"><b class="small">${cantidadPedida}</b></td>
                <td class="tright text-nowrap">$ ${separadorDeUnidades(subtotal)}</td>
            </tr>`;
};

var cargarEncabezado = function () {
    var fichaEncabezado = '';
    var tabla = '<div>';
    var fichaFooter = '';
    var cantidadDeArticulos = 0;
    var subtotalSuma = 0;
    $.ajax({
        url: apiUrl + `perfil/cuentaCorrientePedidoDetalle?clienteid=${clienteId}&tipoMovimiento=${tipoMovimiento}
                        &tipoComprobante=${tipoComprobante}&numeroComprobante=${numeroComprobante}`,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        error: function () {
            div.alertaErrorDeApi.show();
        },
        success: function (data) {
            if (data.length > 0) {
                fichaEncabezado += `<div class="col-xs-12">
                            <p class="mt-0 mb-0">
                                <strong class="small">
                                    ${data[0].Empresa}<br>
                                    Cliente: ${clienteId}, ${clienteNombre}
                                </strong>
                            </p>
                            </div>
                            <div class="col-sm-4">
                                <p class="mt-0 mb-0"><strong class="small">Tipo: ${tipoComprobante}</strong></p>
                            </div>
                            <div class="col-sm-4">
                                <p class="mt-0 mb-0"><strong class="small">Fecha: ${data[0].FechaFactura}</strong></p>
                            </div>
                            <div class="col-sm-4">
                                <p class="mt-0 mb-0"><strong class="small">Nro: ${data[0].NumeroFactura}</strong></p>
                            </div>`;

                $("#divCabecera").html(fichaEncabezado);
                $("#spinEncabezado").hide();

                for (let i = 0; i < data.length; i++) {
                    cantidadDeArticulos += data[i].CantidadPedida;
                    subtotalSuma += data[i].SubTotal;
                    tabla += generarTabla(data[i].Linea,
                        data[i].ArticuloId,
                        data[i].ArticuloDescripcion,
                        data[i].CantidadPedida,
                        data[i].SubTotal);
                }
                tabla += "</div>";
                $("#tablaPedidoCC").html(tabla);
                $("#spinTabla").hide();


                fichaFooter += `<div class="col-sm-4">
                                    <p class="small mt-0 mb-0"><strong>Cantidad de Artículos:</strong> ${
                    cantidadDeArticulos}</p>
                                </div>
                                <div class="col-sm-4">
                                    <p class="small mt-0 mb-0"><strong>Subtotal:</strong> $ ${separadorDeUnidades(
                                        subtotalSuma.toFixed(2))}</p>
                                </div>
                                <div class="col-sm-4">
                                    <p class="small mt-0 mb-0"><strong>Iva:</strong> ${(data[0].TasaIVA) * 100}%</p>
                                </div>
                                <div class="col-xs-12">
                                    <p class="mt-0 mb-0"><strong>
                                        <span class="small">TOTAL:</span> $ ${separadorDeUnidades(
                        (subtotalSuma * (1 + data[0].TasaIVA)).toFixed(2))}</strong>
                                    </p>
                                </div>`;

                $("#divFooter").html(fichaFooter);
                $("#spinFooter").hide();
            } else {
                $("#spinTabla").hide();
                $("#spinFooter").hide();
                $("#spinEncabezado").hide();
                $("#divSinInformacion").removeClass('hide');
            };
        }
    });
};


