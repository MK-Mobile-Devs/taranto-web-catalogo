﻿function separadorDeUnidades(input) {
    var output = input;
    if (parseFloat(input)) {
        input = new String(input);
        var parts = input.split(".");
        parts[0] = parts[0].split("").reverse().join("").replace(/(\d{3})(?!$)/g, "$1.").split("").reverse().join("");
        output = parts.join(",");
    }

    return output;
}

document.addEventListener('DOMContentLoaded', function () {
    cargarEncabezado();
    obtenerDatosCabecera();
}, false);

$(function() {
    $("#btnCtaCte").on("click", function (evento) {
        $("#spinEnviarEmail").removeClass('hide');
        $.ajax({
            url: "/Perfil/CuentaCorrienteInformarPago",
            type: "POST",
            data: function () {
                var data = new FormData();
                data.append("comentario", jQuery("#ctacte_message").val());
                data.append("archivo", jQuery("#ctacte_file").get(0).files[0]);
                return data;
            }(),
            contentType: false,
            processData: false,
            success: function (response) {
                $('#nuevo-pago').modal('toggle');
                $("#spinEnviarEmail").addClass('hide');
            },
            error: function (errorMessage) {
                $("#spinEnviarEmail").addClass('hide');
            }
        });
    });
});

var cargarEncabezado = function() {
    $.ajax({
        url: apiUrl + "perfil/cuentaCorrienteEncabezado?clienteid=" + clienteId,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        error: function () {
            div.alertaErrorDeApi.show();
        },
        success: function (data) {
            if (data.length > 0) {
                $("#strongCliente").text(`Nro Cliente: ${clienteId}`);
                $("#strongClienteNombre").text(`Nombre: ${clienteNombre}`);
                $("#strongBloqueo").text(`Bloqueo: ${data[0].Estado}`);
                $("#strongLimiteCredito").text(`Límite de Crédito: $  ${separadorDeUnidades(data[0].LimiteCredito)}`);
                $("#strongPromedioCompras").text(`Promedio de compras: $  ${separadorDeUnidades(data[0].PromedioCompras)}`);
                $("#strongCheques").text(`Cheques en Cartera: $  ${separadorDeUnidades(data[0].ChequesEnCartera)}`);
                $("#strongTotalDedua").text(`Total Deuda: $ ${separadorDeUnidades(data[0].DeudaTotal)}`);
                $("#strongDeudaCC").text(`Deuda Cta. Cte.: $  ${separadorDeUnidades(data[0].DeudaPendiente)}`);
            }
            $("#spinEncabezado").hide();
        }
    }); 
};

var generarTabla = function (numeroPedidoWeb, numeroPedido, fechaPedido, tipoDePedido, pedidoWeb, tipoMovimiento
    , numeroDeComprobante, fechaComprobante, fechaDespacho, total, saldo, antiguedadDeuda, diasParaBloqueo, tipoMovimientoDescripcion) {

    if (numeroPedidoWeb == null) { numeroPedidoWeb = '' };
    if (numeroPedido == null) { numeroPedido = '' };
    if (fechaPedido == null) { fechaPedido = '' };
    if (tipoDePedido == null) { tipoDePedido = '' };
    if (fechaDespacho == null) { fechaDespacho = '' };
    if (antiguedadDeuda == null) { antiguedadDeuda = '' };
    if (diasParaBloqueo == null) { diasParaBloqueo = '' };

    return `<tr>
                <td class="tright"><span class="color-rojo">${numeroPedidoWeb}</span></td>
                <td class="tright"><span class="color-rojo">${numeroPedido}</span></td>
                <td class="tcenter"><b class="small">${fechaPedido}</b></td>
                <td class="tcenter">${tipoDePedido}</td>
                <td class="tcenter">${pedidoWeb}</td>
                <td class="tcenter">${tipoMovimientoDescripcion}</td>
                <td class="tright"><span class="color-rojo">${Number(numeroDeComprobante).toString()}</span></td>
                <td class="tcenter"><b class="small">${fechaComprobante}</b></td>
                <td class="tcenter"><b class="small">${fechaDespacho}</b></td>
                <td class="tright text-nowrap">$ ${total}</td>
                <td class="tright text-nowrap">$ ${saldo}</td>
                <td class="tcenter"><b class="small">${antiguedadDeuda}</b></td>
                <td class="tcenter"><b class="small">${diasParaBloqueo}</b></td>
                <td class="tright"><a href="/Perfil/CuentaCorrienteDetalle?tipoMovimiento=${tipoMovimiento}&amp;tipoComprobante=${pedidoWeb}&amp;numeroComprobante=${numeroDeComprobante}" class="color-azul">Ver&nbsp;más</a></td>
            </tr>`;
    }

var obtenerDatosCabecera = function () {
    var ficha = "<div>";
    $.ajax({
        url: apiUrl + "perfil/cuentaCorrientePedidoEncabezado?clienteid=" + clienteId,
        type: "GET",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        error: function () {
            div.alertaErrorDeApi.show();
        },
        success: function (data) {
            if (data.length > 0) {
                for (let i = 0; i < data.length; i++) {
                    ficha += generarTabla(data[i].NumeroPedidoWeb,
                        data[i].NumeroPedido,
                        data[i].FechaPedido,
                        data[i].TipoDePedido,
                        data[i].PedidoWeb,
                        data[i].TipoMovimiento,
                        data[i].NumeroDeComprobante,
                        data[i].FechaComprobante,
                        data[i].FechaDespacho,
                        data[i].Total,
                        data[i].Saldo,
                        data[i].AntiguedadDeuda,
                        data[i].DiasParaBloqueo,
                        data[i].TipoMovimientoDescripcion);
                }
                ficha += "</div>";
                $("#tablaPedidoCC").html(ficha);

            } else {
                $("#divSinInformacion").removeClass('hide');
            };

            $("#spinTabla").hide();
        }
    }); 
};


