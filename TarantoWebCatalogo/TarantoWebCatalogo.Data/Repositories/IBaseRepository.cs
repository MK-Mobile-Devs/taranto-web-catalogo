﻿namespace TarantoWebCatalogo.Data.Repositories
{
    public interface IBaseRepository
    {
        TarantoCatalogoEntities Contexto { get; set; }

        void LogDeAccion(string usuario, string accion);
    }
}
