﻿using System.Collections.Generic;
using System.Linq;

namespace TarantoWebCatalogo.Data.Repositories.ListasDePrecios
{
    public class ListaDePrecioRepository : BaseRepository, IListaDePrecioRepository
    {
        public ListaDePrecioRepository(TarantoCatalogoEntities contexto) : base(contexto)
        {
        }

        public List<MVC_ListaDePrecios_ObtenerTodas_Result> ObtenerTodas()
        {
            return Contexto.MVC_ListaDePrecios_ObtenerTodas().ToList();
        }
    }
}