﻿using System.Collections.Generic;

namespace TarantoWebCatalogo.Data.Repositories.ListasDePrecios
{
    public interface IListaDePrecioRepository
    {
        List<MVC_ListaDePrecios_ObtenerTodas_Result> ObtenerTodas();
    }
}