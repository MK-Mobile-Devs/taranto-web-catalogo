﻿using System.Collections.Generic;

namespace TarantoWebCatalogo.Data.Repositories.Utilidades
{
    public interface ICarrouselRepository
    {
        List<MVC_CarrouselContenidos_ObtenerTodosPorIdioma_Result> ObtenerTodosPorIdioma(string idioma);
        
    }
}
