﻿using System.Collections.Generic;
using System.Linq;

namespace TarantoWebCatalogo.Data.Repositories.Utilidades
{
    public class CarrouselRepository : BaseRepository, ICarrouselRepository
    {
        public CarrouselRepository(TarantoCatalogoEntities contexto) : base(contexto)
        {
        }
        

        public List<MVC_CarrouselContenidos_ObtenerTodosPorIdioma_Result> ObtenerTodosPorIdioma(string idioma)
        {
            return Contexto.MVC_CarrouselContenidos_ObtenerTodosPorIdioma(idioma).ToList();
        }
    }
}
