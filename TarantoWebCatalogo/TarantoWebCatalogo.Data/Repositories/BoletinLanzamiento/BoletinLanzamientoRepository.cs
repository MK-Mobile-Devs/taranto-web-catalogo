﻿using System.Collections.Generic;
using System.Linq;

namespace TarantoWebCatalogo.Data.Repositories.BoletinLanzamiento
{
    public class BoletinLanzamientoRepository : BaseRepository, IBoletinLanzamientoRepository
    {
        public BoletinLanzamientoRepository(TarantoCatalogoEntities contexto) : base(contexto)
        {
        }

        public MVC_BoletinLanzamiento_ObtenerTopParaMemoPerfil_Result ObtenerMemoLanzamiento()
        {
            return Contexto.MVC_BoletinLanzamiento_ObtenerTopParaMemoPerfil().FirstOrDefault();
        }

        public List<MVC_BoletinLanzamiento_ObtenerTodos_Result> ObtenerTodos()
        {
            return Contexto.MVC_BoletinLanzamiento_ObtenerTodos().ToList();
        }
    }
}