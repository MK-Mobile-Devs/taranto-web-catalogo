﻿using System.Collections.Generic;

namespace TarantoWebCatalogo.Data.Repositories.BoletinLanzamiento
{
    public interface IBoletinLanzamientoRepository
    {
        List<MVC_BoletinLanzamiento_ObtenerTodos_Result> ObtenerTodos();
        MVC_BoletinLanzamiento_ObtenerTopParaMemoPerfil_Result ObtenerMemoLanzamiento();
    }
}