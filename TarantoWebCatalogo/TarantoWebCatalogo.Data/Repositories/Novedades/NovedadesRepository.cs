﻿using System.Collections.Generic;
using System.Linq;

namespace TarantoWebCatalogo.Data.Repositories.Novedades
{
    public class NovedadesRepository : BaseRepository ,INovedadesRepository
    {
        public NovedadesRepository(TarantoCatalogoEntities contexto) : base(contexto)
        {
        }

        public List<MVC_Novedades_ObtenerCategoriasParaDropdownPorIdioma_Result> ObtenerCategoriasParaDropdownPorIdioma(string idioma)
        {
            return Contexto.MVC_Novedades_ObtenerCategoriasParaDropdownPorIdioma(idioma).ToList();
        }

        public List<MVC_Novedades_ObtenerUltimasPorCategoria_Result> ObtenerUltimasPorCategoria(
            string idioma, int idCategoria, int idNovedadAFiltrar)
        {
            return Contexto.MVC_Novedades_ObtenerUltimasPorCategoria(idioma, idCategoria, idNovedadAFiltrar).ToList();
        }

        public MVC_Novedades_ObtenerPorId_Result ObtenerPorId(string idioma, int id)
        {
            return Contexto.MVC_Novedades_ObtenerPorId(idioma, id).FirstOrDefault();
        }

        public List<MVC_Novedades_ObtenerUltimasPorIdioma_Result> ObtenerUltimasPorIdioma(string idioma)
        {
            return Contexto.MVC_Novedades_ObtenerUltimasPorIdioma(idioma).ToList();
        }
    }
}
