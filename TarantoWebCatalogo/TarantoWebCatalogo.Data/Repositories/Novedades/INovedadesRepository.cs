﻿using System.Collections.Generic;

namespace TarantoWebCatalogo.Data.Repositories.Novedades
{
    public interface INovedadesRepository
    {
        List<MVC_Novedades_ObtenerUltimasPorIdioma_Result> ObtenerUltimasPorIdioma(string idioma);
        List<MVC_Novedades_ObtenerCategoriasParaDropdownPorIdioma_Result> ObtenerCategoriasParaDropdownPorIdioma(string idioma);
        MVC_Novedades_ObtenerPorId_Result ObtenerPorId(string idioma, int id);
        List<MVC_Novedades_ObtenerUltimasPorCategoria_Result> ObtenerUltimasPorCategoria(
            string idioma, int idCategoria, int idNovedadAFiltrar);
    }
}
