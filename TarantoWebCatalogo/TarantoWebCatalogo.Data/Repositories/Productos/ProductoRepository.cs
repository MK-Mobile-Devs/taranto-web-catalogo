﻿using System.Collections.Generic;
using System.Linq;

namespace TarantoWebCatalogo.Data.Repositories.Productos
{
    public class ProductoRepository : BaseRepository, IProductoRepository
    {
        public ProductoRepository(TarantoCatalogoEntities contexto) : base(contexto)
        {
        }

        public MVC_Productos_ObtenerPorId_Result ObtenerPorId(string idioma, int idProducto)
        {
            return Contexto.MVC_Productos_ObtenerPorId(idioma, idProducto).FirstOrDefault();
        }

        public List<MVC_Productos_ObtenerTodosParaMenuPorIdioma_Result> ObtenerTodosParaMenuPorIdioma(string idioma)
        {
            return Contexto.MVC_Productos_ObtenerTodosParaMenuPorIdioma(idioma).ToList();
        }
    }
}
