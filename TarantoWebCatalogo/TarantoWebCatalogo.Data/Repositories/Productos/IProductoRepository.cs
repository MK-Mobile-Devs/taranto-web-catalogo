﻿using System.Collections.Generic;

namespace TarantoWebCatalogo.Data.Repositories.Productos
{
    public interface IProductoRepository
    {
        List<MVC_Productos_ObtenerTodosParaMenuPorIdioma_Result> ObtenerTodosParaMenuPorIdioma(string idioma);
        MVC_Productos_ObtenerPorId_Result ObtenerPorId(string idioma, int idProducto);
    }
}