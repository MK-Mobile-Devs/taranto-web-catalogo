﻿using System.Collections.Generic;
using System.Linq;

namespace TarantoWebCatalogo.Data.Repositories.RecursosHumanos
{
    public class RecursosHumanosRepository : BaseRepository, IRecursosHumanosRepository
    {
        public RecursosHumanosRepository(TarantoCatalogoEntities contexto) : base(contexto)
        {
            
        }

        public void Agregar(int idRrhh, string nombre, string dni, string nacionalidad, string provincia, string ciudad, string direccion
            ,string email, string telefono, string fechaDeNacimiento, string sexo, string mensaje, string nombreArchivo)
        {
            Contexto.MVC_RrhhPostulate_Agregar(idRrhh, nombre, dni, nacionalidad, provincia, ciudad, 
                direccion, email, telefono, fechaDeNacimiento, sexo, mensaje, nombreArchivo);
        }

        public List<MVC_RecursosHumanosBusquedas_ObtenerTodasPorIdioma_Result> ObtenerTodasPorIdioma(string idioma)
        {
            return Contexto.MVC_RecursosHumanosBusquedas_ObtenerTodasPorIdioma(idioma).ToList();
        }


    }
}
