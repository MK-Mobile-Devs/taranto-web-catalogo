﻿using System.Collections.Generic;

namespace TarantoWebCatalogo.Data.Repositories.RecursosHumanos
{
    public interface IRecursosHumanosRepository
    {
        List<MVC_RecursosHumanosBusquedas_ObtenerTodasPorIdioma_Result> ObtenerTodasPorIdioma(string idioma);
        void Agregar(int idRrhh, string nombre, string dni, string nacionalidad,
            string provincia, string ciudad, string direccion, string email,
            string telefono, string fechaDeNacimiento, string sexo, string mensaje, string nombreArchivo);
    }
}