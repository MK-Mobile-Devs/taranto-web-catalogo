﻿using System;
using System.Linq;

namespace TarantoWebCatalogo.Data.Repositories
{
    public abstract class BaseRepository : IBaseRepository
    {
        public TarantoCatalogoEntities Contexto { get; set; }
        public void LogDeAccion(string usuario, string accion)
        {
            Contexto.AccionesLog_Guardar(usuario, accion);
        }

        public DateTime FechaActualBaseDeDatos => Contexto.MVC_SQL_FechaHoraActual().FirstOrDefault().Value;

        protected BaseRepository(TarantoCatalogoEntities contexto)
        {
            Contexto = contexto;
        }
    }
}
