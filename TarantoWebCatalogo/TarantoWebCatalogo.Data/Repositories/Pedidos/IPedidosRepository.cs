﻿using System.Collections.Generic;
using TarantoWebCatalogo.ViewModels.Pedidos;

namespace TarantoWebCatalogo.Data.Repositories.Pedidos
{
    public interface IPedidosRepository : IBaseRepository
    {
        List<MVC_Pedidos_ObtenerPorUsuario_Result> ObtenerPorUsuario(string usuario);

        void Agregar(string usuario, string articulo, string descripcion, int cantidad,
            decimal subtotal, string stock, bool excel, string observacion);

        void Eliminar(string usuario, string articulo, int idPedido);
        void EliminarUltimoPedidoExcel(string usuario);

        void EditarCantidad(string usuario, string articulo, int cantidad, int idPedido);
        void ActualizarPreciosDelPedidoPendiente(string articulo, decimal precioUnitario, int idPedido, string stock);
        List<MVC_Pedidos_ObtenerArticulosParaIconoCarrito_Result> ObtenerArticulosParaIconoCarrito(string usuario);
        List<MVC_Pedidos_ObtenerTodosLosArticulosDePedidosPendientes_Result> ObtenerTodosLosArticulosDePedidosPendientes();
        void ProcesarPedido(int idPedido, string observacion, string usuario);
        void ProcesarPedidoExcel(string usuario);
        List<MVC_Pedidos_ObtenerPedidosParaExportar_Result> ObtenerPedidosParaExportar();
        void ExportarPedido(string usuario, int idPedido, int oracleId);
        List<MVC_Pedido_ObtenerPedidosProcesadosPorUsuario_Result> ObtenerPedidosProcesadosPorUsuario(string usuario);

        List<MVC_Pedido_ObtenerDetallePedidosProcesadosPorIdPedido_Result> ObtenerDetallePedidosProcesadosPorIdPedido(
            int idPedido);

        void Eliminar(string usuario, int pedido);
        void InsertarDesdeExcel(PedidoDesdeExcel pedido);
        List<MVC_Pedidos_ObtenerDetallePedidosParaExportar_Result> ObtenerDetallePedidosParaExportar(int idPedido);
    }
}