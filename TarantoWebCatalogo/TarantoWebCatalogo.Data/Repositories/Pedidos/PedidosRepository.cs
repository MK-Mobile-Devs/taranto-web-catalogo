﻿using System.Collections.Generic;
using System.Linq;
using TarantoWebCatalogo.ViewModels.Pedidos;

namespace TarantoWebCatalogo.Data.Repositories.Pedidos
{
    public class PedidosRepository : BaseRepository, IPedidosRepository
    {
        public PedidosRepository(TarantoCatalogoEntities contexto) : base(contexto)
        {
        }

        public void Eliminar(string usuario, int pedido)
        {
            Contexto.MVC_Pedidos_Eliminar(usuario, pedido);
        }

        public void InsertarDesdeExcel(PedidoDesdeExcel pedido)
        {
            var unPedido = new PedidosCabecera
            {
                Usuario = pedido.Usuario,
                Observacion = pedido.Observaciones,
                Excel = true,
                Exportar = true,
                FechaCreacion = FechaActualBaseDeDatos
            };

            foreach (var art in pedido.Articulos)
            {
                unPedido.PedidosDetalle.Add(new PedidosDetalle
                {
                    Articulo = art.Articulo.ToUpper(),
                    Descripcion = art.Descripcion,
                    PrecioUnitario = art.PrecioUnitario,
                    Stock = art.Stock,
                    Cantidad = art.Cantidad
                });
            };

            Contexto.Set<PedidosCabecera>().Add(unPedido);
        }

        public List<MVC_Pedidos_ObtenerPorUsuario_Result> ObtenerPorUsuario(string usuario)
        {
            return Contexto.MVC_Pedidos_ObtenerPorUsuario(usuario).ToList();
        }

        public void Agregar(string usuario, string articulo, string descripcion, int cantidad, decimal subtotal, string stock, bool excel, string observacion)
        {
            Contexto.MVC_Pedidos_Agregar(usuario, articulo, descripcion, cantidad, subtotal, stock, excel, observacion);
        }

        public void Eliminar(string usuario, string articulo, int idPedido)
        {
            Contexto.MVC_Pedidos_EliminarPorFiltros(usuario, articulo, idPedido);
        }

        public void EditarCantidad(string usuario, string articulo, int cantidad, int idPedido)
        {
            Contexto.MVC_Pedidos_EditarCantidadDeProducto(usuario, articulo, cantidad, idPedido);
        }

        public void ActualizarPreciosDelPedidoPendiente(string articulo, decimal precioUnitario, int idPedido, string stock)
        {
            Contexto.MVC_Pedidos_ActualizarPreciosDelPedidoPendiente(articulo, precioUnitario, idPedido, stock);
        }

        public List<MVC_Pedidos_ObtenerArticulosParaIconoCarrito_Result> ObtenerArticulosParaIconoCarrito(string usuario)
        {
            return Contexto.MVC_Pedidos_ObtenerArticulosParaIconoCarrito(usuario).ToList();
        }
        
        public List<MVC_Pedidos_ObtenerTodosLosArticulosDePedidosPendientes_Result>
            ObtenerTodosLosArticulosDePedidosPendientes()
        {
            return Contexto.MVC_Pedidos_ObtenerTodosLosArticulosDePedidosPendientes().ToList();
        }

        public void ProcesarPedido(int idPedido, string observacion, string usuario)
        {
            Contexto.MVC_Pedidos_ProcesarPedido(idPedido, observacion, usuario);
        }

        public void ProcesarPedidoExcel(string usuario)
        {
            Contexto.MVC_Pedidos_ProcesarPedidoExcel(usuario);
        }

        public List<MVC_Pedidos_ObtenerPedidosParaExportar_Result> ObtenerPedidosParaExportar()
        {
            return Contexto.MVC_Pedidos_ObtenerPedidosParaExportar().ToList();
        }

        public List<MVC_Pedidos_ObtenerDetallePedidosParaExportar_Result> ObtenerDetallePedidosParaExportar(int idPedido)
        {
            return Contexto.MVC_Pedidos_ObtenerDetallePedidosParaExportar(idPedido).ToList();
        }

        public void ExportarPedido(string usuario, int idPedido, int oracleId)
        {
            Contexto.MVC_Pedidos_PedidoExportado(usuario, idPedido, oracleId);
        }

        public List<MVC_Pedido_ObtenerPedidosProcesadosPorUsuario_Result> ObtenerPedidosProcesadosPorUsuario(string usuario)
        {
            return Contexto.MVC_Pedido_ObtenerPedidosProcesadosPorUsuario(usuario).ToList();
        }

        public List<MVC_Pedido_ObtenerDetallePedidosProcesadosPorIdPedido_Result> ObtenerDetallePedidosProcesadosPorIdPedido(int idPedido)
        {
            return Contexto.MVC_Pedido_ObtenerDetallePedidosProcesadosPorIdPedido(idPedido).ToList();
        }

        public void EliminarUltimoPedidoExcel(string usuario)
        {
            Contexto.MVC_Pedidos_EliminarUltimoExcel(usuario);
        }
    }
}