﻿using System;

namespace TarantoWebCatalogo.Data.Repositories.Cookies
{
    public interface ICookieRepository
    {
        void Guardar(string usuarioLogueadoCodigo, string token);
        void EliminarTodasPorCodigoUsuario(string usuarioLogueadoCodigo);
        MVC_Cookies_ObtenerPorUsuarioToken_Result ObtenerPorUsuarioToken(string usuarioLogueadoCodigo, string token);
        void Actualizar(string usuarioLogueadoCodigo, string token);
        DateTime FechaActualBaseDeDatos { get; }
    }
}
