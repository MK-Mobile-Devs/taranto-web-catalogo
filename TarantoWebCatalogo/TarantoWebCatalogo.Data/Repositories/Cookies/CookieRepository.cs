﻿using System.Linq;

namespace TarantoWebCatalogo.Data.Repositories.Cookies
{
    public class CookieRepository : BaseRepository, ICookieRepository
    {
        public CookieRepository(TarantoCatalogoEntities contexto) : base(contexto)
        {
        }

        public void Guardar(string usuarioLogueadoCodigo, string token)
        {
            Contexto.MVC_Cookies_Agregar(usuarioLogueadoCodigo, token);
        }

        public void EliminarTodasPorCodigoUsuario(string usuarioLogueadoCodigo)
        {
            Contexto.MVC_Cookies_EliminarTodasDeUsuario(usuarioLogueadoCodigo);
        }

        public MVC_Cookies_ObtenerPorUsuarioToken_Result ObtenerPorUsuarioToken(string usuarioLogueadoCodigo, string token)
        {
            return Contexto.MVC_Cookies_ObtenerPorUsuarioToken(usuarioLogueadoCodigo, token).FirstOrDefault();
        }

        public void Actualizar(string usuarioLogueadoCodigo, string token)
        {
            Contexto.MVC_Cookies_Actualizar(usuarioLogueadoCodigo, token);
        }
    }
}
