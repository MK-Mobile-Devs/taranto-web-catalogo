﻿namespace TarantoWebCatalogo.Data.Repositories.PaginasInstitucionales
{
    public interface IPaginasInstitucionalesRepository
    {
        MVC_PaginasInstitucionales_ObtenerPorIdEIdioma_Result ObtenerPorIdEIdioma(string idioma, int id);
        //List<MVC_Empresa_ObtenerBusquedasRrhhPorIdioma_Result> ObtenerBusquedasRrhhPorIdioma(string idioma);
    }
}
