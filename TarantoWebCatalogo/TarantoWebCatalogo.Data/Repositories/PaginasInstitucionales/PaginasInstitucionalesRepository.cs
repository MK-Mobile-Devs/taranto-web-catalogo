﻿using System.Collections.Generic;
using System.Linq;

namespace TarantoWebCatalogo.Data.Repositories.PaginasInstitucionales
{
    public class PaginasInstitucionalesRepository : BaseRepository, IPaginasInstitucionalesRepository
    {
        public PaginasInstitucionalesRepository(TarantoCatalogoEntities contexto) : base(contexto)
        {
        }

        //public List<MVC_PaginasInstitucionales_ObtenerPorIdEIdioma_Result> ObtenerBusquedasRrhhPorIdioma(string idioma)
        //{
        //    return Contexto.MVC_Empresa_ObtenerBusquedasRrhhPorIdioma(idioma).ToList();
        //}

        public MVC_PaginasInstitucionales_ObtenerPorIdEIdioma_Result ObtenerPorIdEIdioma(string idioma, int id)
        {
            return Contexto.MVC_PaginasInstitucionales_ObtenerPorIdEIdioma(idioma, id).FirstOrDefault();
        }

    }
}
