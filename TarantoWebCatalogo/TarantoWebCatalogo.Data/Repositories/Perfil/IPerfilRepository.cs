﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TarantoWebCatalogo.Data.Entities;

namespace TarantoWebCatalogo.Data.Repositories.Perfil
{
    public interface IPerfilRepository
    {
        EdicionDatos ObtenerEdicionDatos(string clienteId);
        void CambiarMisDatos(string data);
        decimal CargarFotoPuntoVenta(TarantoWebCatalogo.ViewModels.Perfil.CargarFotosPuntoVentaViewModel viewModel);
        decimal CambiarMisDatosInfoSerialized(string clientId, string infoSerialized);
        void ActualizarMailEnviado(decimal id);
    }
}
