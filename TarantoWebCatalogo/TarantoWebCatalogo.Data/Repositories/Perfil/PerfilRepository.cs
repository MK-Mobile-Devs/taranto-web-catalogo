﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TarantoWebCatalogo.Data.Entities;
using TarantoWebCatalogo.ViewModels.Perfil;

namespace TarantoWebCatalogo.Data.Repositories.Perfil
{
    public class PerfilRepository : BaseRepository, IPerfilRepository
    {
        public PerfilRepository(TarantoCatalogoEntities contexto) : base(contexto)
        {
        }

        public void ActualizarMailEnviado(decimal id)
        {
            Contexto.Database.SqlQuery<decimal>(@"UPDATE user_data_edit_request SET sent_mail=1 WHERE id = {0}",
                new object[] { id }).First();
        }

        public void CambiarMisDatos(string clienteId)
        {
            var result = Contexto.Database.SqlQuery<EdicionDatos>("SELECT received FROM user_data_edit_request WHERE id_user = '" + clienteId + "' AND img = ''").ToList();
        }
        public decimal CambiarMisDatosInfoSerialized(string clientId, string infoSerialized)
        {
            //Contexto.Database.ExecuteSqlCommand(@"INSERT INTO UserDataEditRequest (Fecha, IdUser, InfoSerialized, Img, MoreImg) VALUES ('{0}', '{1}', '{2}', 'CONVERT(varbinary(30), {3}, 0)', '{4}')",
            //    new object[] { DateTime.Now.ToString("yyyy-MM-dd"), clientId, infoSerialized, "", ";" });

            var parameter = new SqlParameter("@Img", SqlDbType.VarBinary, -1);
            parameter.Value = DBNull.Value;
            var parameterDate = new SqlParameter("@Fecha", SqlDbType.DateTime);
            parameterDate.Value = DateTime.Now;
            decimal id = Contexto.Database.SqlQuery<decimal>(@"INSERT INTO UserDataEditRequest (Fecha, IdUser, InfoSerialized, Img, MoreImg) VALUES (@Fecha, '@IdUser', '@InfoSerialized', @Img, @MoreImg)",
                new object[] {
                    parameterDate,
                    new SqlParameter("@IdUser", clientId),
                    new SqlParameter("@InfoSerialized",  infoSerialized),
                    parameter,
                    new SqlParameter("@MoreImg", DBNull.Value),
                }).First();

            return id;
        }
        public decimal CargarFotoPuntoVenta(CargarFotosPuntoVentaViewModel viewModel)
        {
            decimal id = Contexto.Database.SqlQuery<decimal>(@"INSERT INTO UserDataEditRequest (Fecha, IdUser, InfoSerialized, Img, MoreImg) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}')",
                 new object[] { DateTime.Now.ToString("yyyy-MM-dd"), viewModel.ClienteId, viewModel.ToString(), viewModel.ImagenPrincipal, string.Join(";", viewModel.MoreImages) }).First();
            return id;
        }

        public EdicionDatos ObtenerEdicionDatos(string clienteId)
        {
            var result = Contexto.Database.SqlQuery<EdicionDatos>("SELECT Received, InfoSerialized FROM UserDataEditRequest WHERE IdUser = '" + clienteId+"' and img = ''").ToList();
            return result.FirstOrDefault();
        }
    }
}
