//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TarantoWebCatalogo.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class PedidosDetalle
    {
        public string Articulo { get; set; }
        public string Descripcion { get; set; }
        public int Cantidad { get; set; }
        public decimal PrecioUnitario { get; set; }
        public int IdPedido { get; set; }
        public string Stock { get; set; }
        public int Id { get; set; }
    
        public virtual PedidosCabecera PedidosCabecera { get; set; }
    }
}
