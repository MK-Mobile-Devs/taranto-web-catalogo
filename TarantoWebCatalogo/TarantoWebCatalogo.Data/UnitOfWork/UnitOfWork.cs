﻿using System;
using System.Data.Entity.Validation;
using TarantoWebCatalogo.Data.Repositories.BoletinLanzamiento;
using TarantoWebCatalogo.Data.Repositories.Cookies;
using TarantoWebCatalogo.Data.Repositories.ListasDePrecios;
using TarantoWebCatalogo.Data.Repositories.Novedades;
using TarantoWebCatalogo.Data.Repositories.PaginasInstitucionales;
using TarantoWebCatalogo.Data.Repositories.Pedidos;
using TarantoWebCatalogo.Data.Repositories.Perfil;
using TarantoWebCatalogo.Data.Repositories.Productos;
using TarantoWebCatalogo.Data.Repositories.RecursosHumanos;
using TarantoWebCatalogo.Data.Repositories.Utilidades;

namespace TarantoWebCatalogo.Data.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        #region private members

        private ICarrouselRepository _carrouselRepository;
        private INovedadesRepository _novedadesRepository;
        private IPaginasInstitucionalesRepository _paginasInstitucionalRepository;
        private IProductoRepository _productoRepository;
        private ICookieRepository _cookieRepository;
        private IRecursosHumanosRepository _recursosHumanosRepository;
        private IPedidosRepository _pedidosRepository;
        private IListaDePrecioRepository _listaDePrecioRepository;
        private IBoletinLanzamientoRepository _boletinLanzamientoRepository;
        private IPerfilRepository _perfilRepository;


        private TarantoCatalogoEntities _context = null;

        #endregion

        #region public members

        public UnitOfWork()
        {
            _context = new TarantoCatalogoEntities();
            _context.Database.CommandTimeout = 1000;
        }

        public IPerfilRepository PerfilRepository
          => _perfilRepository ?? (_perfilRepository = new PerfilRepository(_context));

        public ICarrouselRepository CarrouselRepository
            => _carrouselRepository ?? (_carrouselRepository = new CarrouselRepository(_context));

        public INovedadesRepository NovedadesRepository
            => _novedadesRepository ?? (_novedadesRepository = new NovedadesRepository(_context));

        public IPaginasInstitucionalesRepository PaginasInstitucionalRepository
            => _paginasInstitucionalRepository ?? (_paginasInstitucionalRepository = new PaginasInstitucionalesRepository(_context));

        public IProductoRepository ProductoRepository
            => _productoRepository ?? (_productoRepository = new ProductoRepository(_context));

        public ICookieRepository CookieRepository
            => _cookieRepository ?? (_cookieRepository = new CookieRepository(_context));

        public IRecursosHumanosRepository RecursosHumanosRepository
            => _recursosHumanosRepository ?? (_recursosHumanosRepository = new RecursosHumanosRepository(_context));

        public IPedidosRepository PedidosRepository
            => _pedidosRepository ?? (_pedidosRepository = new PedidosRepository(_context));

        public IListaDePrecioRepository ListaDePrecioRepository
            => _listaDePrecioRepository ?? (_listaDePrecioRepository = new ListaDePrecioRepository(_context));

        public IBoletinLanzamientoRepository BoletinLanzamientoRepository
            => _boletinLanzamientoRepository ??
               (_boletinLanzamientoRepository = new BoletinLanzamientoRepository(_context));

        public void Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                //_logger.Error("Error en Validaciones: DbEntityValidationException-->");
                foreach (var eve in e.EntityValidationErrors)
                {
                    var line =
                        $"{DateTime.Now}: Entity of type \"{eve.Entry.Entity.GetType().Name}\" in state \"{eve.Entry.State}\" has the following validation errors:";
                    //_logger.Info(line);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        var validation = $"- Property: \"{ve.PropertyName}\", Error: \"{ve.ErrorMessage}\"";
                        //_logger.Info(validation);
                    }
                }

                //_logger.Error("<--End Error en Validaciones: DbEntityValidationException");

                throw;
            }
        }
        #endregion
        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
