﻿using TarantoWebCatalogo.Data.Repositories.BoletinLanzamiento;
using TarantoWebCatalogo.Data.Repositories.Productos;
using TarantoWebCatalogo.Data.Repositories.Novedades;
using TarantoWebCatalogo.Data.Repositories.Cookies;
using TarantoWebCatalogo.Data.Repositories.ListasDePrecios;
using TarantoWebCatalogo.Data.Repositories.PaginasInstitucionales;
using TarantoWebCatalogo.Data.Repositories.Pedidos;
using TarantoWebCatalogo.Data.Repositories.RecursosHumanos;
using TarantoWebCatalogo.Data.Repositories.Utilidades;
using TarantoWebCatalogo.Data.Repositories.Perfil;

namespace TarantoWebCatalogo.Data.UnitOfWork
{
    public interface IUnitOfWork
    {
        ICarrouselRepository CarrouselRepository { get; }
        IPerfilRepository PerfilRepository { get; }
        INovedadesRepository NovedadesRepository { get; }
        IPaginasInstitucionalesRepository PaginasInstitucionalRepository { get; }
        IProductoRepository ProductoRepository { get; }
        ICookieRepository CookieRepository { get; }
        IRecursosHumanosRepository RecursosHumanosRepository { get; }
        IPedidosRepository PedidosRepository { get; }
        IListaDePrecioRepository ListaDePrecioRepository { get; }
        IBoletinLanzamientoRepository BoletinLanzamientoRepository { get; }
        void Save();
    }
}
