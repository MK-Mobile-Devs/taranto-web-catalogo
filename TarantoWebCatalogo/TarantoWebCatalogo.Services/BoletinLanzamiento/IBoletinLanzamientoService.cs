﻿using System.Collections.Generic;

namespace TarantoWebCatalogo.Services.BoletinLanzamiento
{
    public interface IBoletinLanzamientoService
    {
        List<ViewModels.BoletinLanzamiento.BoletinLanzamiento> ObtenerTodos();
        ViewModels.BoletinLanzamiento.BoletinLanzamiento ObtenerMemoLanzamiento();
    }
}