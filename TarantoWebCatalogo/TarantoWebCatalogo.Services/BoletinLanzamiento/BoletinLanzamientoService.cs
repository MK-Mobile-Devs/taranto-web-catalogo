﻿using System.Collections.Generic;
using AutoMapper;
using TarantoWebCatalogo.Data;
using TarantoWebCatalogo.Data.Repositories.BoletinLanzamiento;
using TarantoWebCatalogo.ViewModels.BoletinLanzamiento;

namespace TarantoWebCatalogo.Services.BoletinLanzamiento
{
    public class BoletinLanzamientoService : IBoletinLanzamientoService
    {
        private readonly IBoletinLanzamientoRepository _boletinLanzamientoRepository;

        public BoletinLanzamientoService(IBoletinLanzamientoRepository boletinLanzamientoRepository)
        {
            _boletinLanzamientoRepository = boletinLanzamientoRepository;
        }

        public ViewModels.BoletinLanzamiento.BoletinLanzamiento ObtenerMemoLanzamiento()
        {
            var boletin = _boletinLanzamientoRepository.ObtenerMemoLanzamiento();
            return Mapper.Map<MVC_BoletinLanzamiento_ObtenerTopParaMemoPerfil_Result, ViewModels.BoletinLanzamiento.BoletinLanzamiento>(boletin);
        }

        public List<ViewModels.BoletinLanzamiento.BoletinLanzamiento> ObtenerTodos()
        {
            var boletines = _boletinLanzamientoRepository.ObtenerTodos();
            return Mapper
                .Map<List<MVC_BoletinLanzamiento_ObtenerTodos_Result>,
                    List<ViewModels.BoletinLanzamiento.BoletinLanzamiento>>(boletines);
        }
    }
}