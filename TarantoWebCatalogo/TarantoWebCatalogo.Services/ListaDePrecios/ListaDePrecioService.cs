﻿using System.Collections.Generic;
using AutoMapper;
using TarantoWebCatalogo.Data;
using TarantoWebCatalogo.Data.Repositories.ListasDePrecios;
using TarantoWebCatalogo.ViewModels.ListaDePrecios;

namespace TarantoWebCatalogo.Services.ListaDePrecios
{
    public class ListaDePrecioService : IListaDePrecioService
    {
        private readonly IListaDePrecioRepository _listaDePrecioRepository;

        public ListaDePrecioService(IListaDePrecioRepository listaDePrecioRepository)
        {
            _listaDePrecioRepository = listaDePrecioRepository;
        }
        
        public List<ListaDePrecio> ObtenerTodas()
        {
            var listasDePrecios = _listaDePrecioRepository.ObtenerTodas();
            return Mapper.Map<List<MVC_ListaDePrecios_ObtenerTodas_Result>, List<ListaDePrecio>>(listasDePrecios);
        }
    }
}