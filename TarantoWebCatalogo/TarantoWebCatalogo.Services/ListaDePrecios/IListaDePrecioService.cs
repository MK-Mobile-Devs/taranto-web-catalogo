﻿using System.Collections.Generic;
using TarantoWebCatalogo.ViewModels.ListaDePrecios;

namespace TarantoWebCatalogo.Services.ListaDePrecios
{
    public interface IListaDePrecioService
    {
        List<ListaDePrecio> ObtenerTodas();
    }
}