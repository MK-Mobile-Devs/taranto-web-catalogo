﻿namespace TarantoWebCatalogo.Services.Core
{
    public interface ICookieService
    {
        void Guardar(string usuarioLogueadoCodigo, string token);
        void EliminarTodasPorCodigoUsuario(string usuarioLogueadoCodigo);
        bool EsValida(string codigoUsuario, string token);
    }
}
