﻿using System;
using System.Web;
using TarantoWebCatalogo.Services.Helpers;

namespace TarantoWebCatalogo.Services.Core
{
    public class SeguridadService : ISeguridadService
    {
        private readonly ICookieService _cookieService;
        private readonly HttpSessionStateBase _session;
        private readonly HttpRequestBase _request;
        private readonly HttpResponseBase _response;
        private readonly IApiService _apiService;

        public SeguridadService(ICookieService cookieService, IApiService apiService)
        {
            _cookieService = cookieService;
            _session = new HttpSessionStateWrapper(HttpContext.Current.Session);
            _request = new HttpRequestWrapper(HttpContext.Current.Request);
            _response = new HttpResponseWrapper(HttpContext.Current.Response);
            _apiService = apiService;
        }

        public bool HayUsuarioLogueado => VerificarLogin();

        private bool VerificarLogin()
        {
            if (!string.IsNullOrEmpty(UsuarioLogueadoCodigo))
            {
                return true;
            }

            var cookie = _request.Cookies["usuario"];
            if (cookie == null)
            {
                return false;
            }

            if (_cookieService.EsValida(cookie["C"], cookie["Token"]))
            {
                UsuarioLogueadoCodigo = cookie["C"];
                UsuarioLogueadoEsProveedor = Convert.ToBoolean(cookie["P"]);
                NombreUsuario = cookie["N"];
                return true;
            }

            return false;
        }

        public string UsuarioLogueadoCodigo
        {
            get => _session["UsuarioLogueadoCodigo"]?.ToString();
            private set => _session["UsuarioLogueadoCodigo"] = value;
        }

        public bool UsuarioLogueadoEsProveedor
        {
            get => Convert.ToBoolean(_session["UsuarioLogueadoEsProveedor"]);
            private set => _session["UsuarioLogueadoEsProveedor"] = value;
        }

        public string NombreUsuario
        {
            get => _session["NombreUsuarioLogueado"]?.ToString();
            private set => _session["NombreUsuarioLogueado"] = value;
        }
        
        public bool Login(string usuario, string password)
        {
            var usuarioLogueado = _apiService.Login(usuario, password);
            if (usuarioLogueado == null) return false;
            UsuarioLogueadoCodigo = usuarioLogueado.Usuario;
            UsuarioLogueadoEsProveedor = usuarioLogueado.EsProveedor;
            NombreUsuario = usuarioLogueado.Descripcion;

            EstablecerCookie();
            return true;
        }

        private void EstablecerCookie()
        {
            var usuario = new HttpCookie("usuario")
            {
                ["C"] = UsuarioLogueadoCodigo,
                ["Token"] = StringHelper.Sha256(DateTime.Now.Millisecond.ToString()),
                ["N"] = NombreUsuario,
                ["P"] = UsuarioLogueadoEsProveedor.ToString(),
                Expires = DateTime.Now.AddDays(1),
                HttpOnly = true
            };
            _response.SetCookie(usuario);
            GuardarCookie(usuario["Token"]);
        }

        private void GuardarCookie(string token)
        {
            _cookieService.EliminarTodasPorCodigoUsuario(UsuarioLogueadoCodigo);
            _cookieService.Guardar(UsuarioLogueadoCodigo, token);
        }

        public void Logout()
        {
            _cookieService.EliminarTodasPorCodigoUsuario(UsuarioLogueadoCodigo);
            UsuarioLogueadoCodigo = string.Empty;
            _session.Clear();
            _session.Abandon();
            HacerExpirarALaCookie();
        }

        private void HacerExpirarALaCookie()
        {
            var cookie = _request.Cookies["usuario"];
            cookie.Expires = DateTime.Today.AddDays(-5);
            _response.SetCookie(cookie);
        }
        
    }
}
