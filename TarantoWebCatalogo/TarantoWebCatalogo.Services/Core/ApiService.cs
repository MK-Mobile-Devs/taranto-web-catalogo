﻿using System;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using TarantoWebCatalogo.ViewModels.Usuarios.Api;

namespace TarantoWebCatalogo.Services.Core
{
    public class ApiService : IApiService
    {
        private readonly string _apiUrl;

        public ApiService(string apiUrl)
        {
            _apiUrl = apiUrl;
        }

        public UsuarioLogeado Login(string usuario, string password)
        {
            using (var client = new WebClient())
            {
                var postData =
                    new System.Collections.Specialized.NameValueCollection()
                    {
                        { "usuario", usuario },
                        { "password", password }
                    };
                try
                {
                    var response = Encoding.UTF8.GetString(client.UploadValues(_apiUrl + "core/validarLogin", postData));
                    return JsonConvert.DeserializeObject<UsuarioLogeado>(response);
                }
                catch (Exception)
                {
                    return null;
                }
                
            }
        }
    }
}