﻿using System;
using TarantoWebCatalogo.Data.Repositories.Cookies;

namespace TarantoWebCatalogo.Services.Core
{
    public class CookieService : ICookieService
    {
        private readonly ICookieRepository _cookieRepository;

        public CookieService(ICookieRepository cookieRepository)
        {
            _cookieRepository = cookieRepository;
        }

        public void Guardar(string usuarioLogueadoCodigo, string token)
        {
            _cookieRepository.Guardar(usuarioLogueadoCodigo, token);
        }

        public void EliminarTodasPorCodigoUsuario(string usuarioLogueadoCodigo)
        {
            _cookieRepository.EliminarTodasPorCodigoUsuario(usuarioLogueadoCodigo);
        }

        public bool EsValida(string usuarioLogueadoCodigo, string token)
        {
            var cookie = _cookieRepository.ObtenerPorUsuarioToken(usuarioLogueadoCodigo, token);

            if (cookie != null)
            {
                TimeSpan ts = _cookieRepository.FechaActualBaseDeDatos - cookie.FechaCreacion;

                if (ts.Minutes <= 60)
                {
                    _cookieRepository.Actualizar(usuarioLogueadoCodigo, token);
                    return true;
                }
            }

            return false;
        }
    }
}
