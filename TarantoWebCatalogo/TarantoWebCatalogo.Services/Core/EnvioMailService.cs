﻿using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace TarantoWebCatalogo.Services.Core
{
    public class EnvioMailService : IEnvioMailService
    {
        public void SendEmail(string address, string subject, string body, System.Net.Mail.Attachment adjuntar, bool IsBodyHTML)
        {

            MailAddress FromAddr = new MailAddress("pedidostaranto@gmail.com", "Taranto", Encoding.UTF8);
            MailAddress ToAddr = new MailAddress(address, "", Encoding.UTF8);
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("pedidostaranto@gmail.com", "taranto2016")
            };

            using (MailMessage message = new MailMessage(FromAddr, ToAddr)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = IsBodyHTML,
                BodyEncoding = Encoding.UTF8

            })
            {
                if (adjuntar != null) { message.Attachments.Add(adjuntar); }
                smtp.Send(message);
            }
        }

        public async Task SendEmailTwilio(string address, string subject, string body)
        {
            var apiKey = System.Web.Configuration.WebConfigurationManager.AppSettings["SendGripApiKey"];
            var client = new SendGridClient(apiKey);

            var msg = new SendGridMessage()
            {
                From = new EmailAddress(address, "Taranto"),
                Subject = subject,
                HtmlContent = body
            };

            msg.AddTo(new EmailAddress(address, "Taranto contactos web"));
            var response = await client.SendEmailAsync(msg).ConfigureAwait(false);
        }
    }
}
