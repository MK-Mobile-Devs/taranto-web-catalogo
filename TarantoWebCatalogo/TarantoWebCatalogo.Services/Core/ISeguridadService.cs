﻿namespace TarantoWebCatalogo.Services.Core
{
    public interface ISeguridadService
    {
        bool HayUsuarioLogueado { get; }
        bool Login(string usuario, string password);
        string UsuarioLogueadoCodigo { get; }
        void Logout();
        bool UsuarioLogueadoEsProveedor { get; }
        string NombreUsuario { get; }
    }
}
