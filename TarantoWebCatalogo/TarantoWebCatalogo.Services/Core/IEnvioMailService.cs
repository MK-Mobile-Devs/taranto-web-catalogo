﻿using System.Net.Mail;
using System.Threading.Tasks;

namespace TarantoWebCatalogo.Services.Core
{
    public interface IEnvioMailService
    {
        void SendEmail(string address, string subject, string body, Attachment adjuntar, bool IsBodyHTML);
        Task SendEmailTwilio(string address, string subject, string body);
    }
}