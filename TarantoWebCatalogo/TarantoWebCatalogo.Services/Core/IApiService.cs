﻿using TarantoWebCatalogo.ViewModels.Usuarios.Api;

namespace TarantoWebCatalogo.Services.Core
{
    public interface IApiService
    {
        UsuarioLogeado Login(string usuario, string password);
    }
}