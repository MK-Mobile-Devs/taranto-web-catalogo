﻿using System.Collections.Generic;
using TarantoWebCatalogo.ViewModels.Pedidos;

namespace TarantoWebCatalogo.Services.Pedidos
{
    public interface IPedidosService
    {
        List<PedidoDetail> ObtenerPorUsuario(string usuario);
        void Agregar(string usuario, string articulo, string descripcion, int cantidad,
            decimal subtotal, string stock, bool excel, string observacion);
        void Eliminar(string usuario, string articulo, int idPedido);
        void EliminarUltimoPedidoExcel(string usuario);
        void EditarCantidad(string usuario, string articulo, int cantidad, int idPedido);
        void ActualizarPreciosDelPedidoPendiente(string articulo, decimal precioUnitario, int idPedido, string stock);
        List<PedidoDetail> ObtenerArticulosParaIconoCarrito(string usuario);
        List<PedidoDetail> ObtenerTodosLosArticulosDePedidosPendiente();
        void ProcesarPedido(int idPedido, string observacion, string usuario);
        void ProcesarPedidoExcel(string usuario);
        List<PedidoProcesado> ObtenerPedidosParaExportar();
        void ExportarPedido(string usuario, int idPedido, int oracleId);
        List<PedidoProcesadoDetail> ObtenerPedidosProcesadosPorUsuario(string usuario);
        List<PedidoProcesadoItemDetail> ObtenerDetallePedidosProcesadosPorIdPedido(int idPedido);
        void Eliminar(string usuario, int pedido);
        void InsertarPedidoDesdeExcel(PedidoDesdeExcel pedido);

        void LogDeAccion(string usuario, string message);
    }
}