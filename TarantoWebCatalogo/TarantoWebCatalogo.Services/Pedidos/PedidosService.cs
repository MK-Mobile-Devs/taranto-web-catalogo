﻿using System.Collections.Generic;
using AutoMapper;
using TarantoWebCatalogo.Data;
using TarantoWebCatalogo.Data.UnitOfWork;
using TarantoWebCatalogo.ViewModels.Pedidos;

namespace TarantoWebCatalogo.Services.Pedidos
{
    public class PedidosService : IPedidosService
    {
        private readonly IUnitOfWork _unitOfWork;

        public PedidosService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public List<PedidoDetail> ObtenerPorUsuario(string usuario)
        {
            var pedido = _unitOfWork.PedidosRepository.ObtenerPorUsuario(usuario);
            return Mapper.Map<List<MVC_Pedidos_ObtenerPorUsuario_Result>, List<PedidoDetail>>(pedido);
        }

        public void Agregar(string usuario, string articulo, string descripcion, int cantidad, decimal subtotal, string stock, bool excel, string observacion)
        {
            _unitOfWork.PedidosRepository.Agregar(usuario, articulo, descripcion, cantidad, subtotal, stock, excel, observacion);
        }

        public void Eliminar(string usuario, string articulo, int idPedido)
        {
            _unitOfWork.PedidosRepository.Eliminar(usuario,articulo,idPedido);
        }

        public void EliminarUltimoPedidoExcel(string usuario)
        {
            _unitOfWork.PedidosRepository.EliminarUltimoPedidoExcel(usuario);
        }

        public void EditarCantidad(string usuario, string articulo, int cantidad, int idPedido)
        {
            _unitOfWork.PedidosRepository.EditarCantidad(usuario, articulo, cantidad, idPedido);
        }

        public void Eliminar(string usuario, int pedido)
        {
            _unitOfWork.PedidosRepository.Eliminar(usuario, pedido);
        }

        public void InsertarPedidoDesdeExcel(PedidoDesdeExcel pedido)
        {
            _unitOfWork.PedidosRepository.InsertarDesdeExcel(pedido);
            _unitOfWork.Save();
        }

        public void LogDeAccion(string usuario, string message)
        {
            _unitOfWork.PedidosRepository.LogDeAccion(usuario, message);
        }

        public void ActualizarPreciosDelPedidoPendiente(string articulo, decimal precioUnitario, int idPedido, string stock)
        {
            _unitOfWork.PedidosRepository.ActualizarPreciosDelPedidoPendiente(articulo, precioUnitario, idPedido, stock);
        }

        public List<PedidoDetail> ObtenerArticulosParaIconoCarrito(string usuario)
        {
            var listaPedido = _unitOfWork.PedidosRepository.ObtenerArticulosParaIconoCarrito(usuario);
            return Mapper.Map<List<MVC_Pedidos_ObtenerArticulosParaIconoCarrito_Result>, List<PedidoDetail>>(listaPedido);
        }

        public List<PedidoDetail> ObtenerTodosLosArticulosDePedidosPendiente()
        {
            var listaPedido = _unitOfWork.PedidosRepository.ObtenerTodosLosArticulosDePedidosPendientes();
            return Mapper.Map<List<MVC_Pedidos_ObtenerTodosLosArticulosDePedidosPendientes_Result>, List<PedidoDetail>>(
                listaPedido);
        }

        public void ProcesarPedido(int idPedido, string observacion, string usuario)
        {
            _unitOfWork.PedidosRepository.ProcesarPedido(idPedido, observacion, usuario);
        }

        public void ProcesarPedidoExcel(string usuario)
        {
            _unitOfWork.PedidosRepository.ProcesarPedidoExcel(usuario);
        }

        public List<PedidoProcesado> ObtenerPedidosParaExportar()
        {
            var pedido = _unitOfWork.PedidosRepository.ObtenerPedidosParaExportar();
            return Mapper.Map<List<MVC_Pedidos_ObtenerPedidosParaExportar_Result>, List<PedidoProcesado>>(
                pedido);
        }

        public void ExportarPedido(string usuario, int idPedido, int oracleId)
        {
            _unitOfWork.PedidosRepository.ExportarPedido(usuario, idPedido, oracleId);
        }

        public List<PedidoProcesadoDetail> ObtenerPedidosProcesadosPorUsuario(string usuario)
        {
            var pedidos = _unitOfWork.PedidosRepository.ObtenerPedidosProcesadosPorUsuario(usuario);
            return Mapper
                .Map<List<MVC_Pedido_ObtenerPedidosProcesadosPorUsuario_Result>, List<PedidoProcesadoDetail>>(pedidos);
        }

        public List<PedidoProcesadoItemDetail> ObtenerDetallePedidosProcesadosPorIdPedido(int idPedido)
        {
            var aritculos = _unitOfWork.PedidosRepository.ObtenerDetallePedidosProcesadosPorIdPedido(idPedido);
            return Mapper
                .Map<List<MVC_Pedido_ObtenerDetallePedidosProcesadosPorIdPedido_Result>, List<PedidoProcesadoItemDetail>
                >(aritculos);
        }
    }
}