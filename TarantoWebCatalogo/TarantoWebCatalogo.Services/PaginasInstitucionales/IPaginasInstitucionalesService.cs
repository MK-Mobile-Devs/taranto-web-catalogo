﻿using TarantoWebCatalogo.ViewModels.PaginasInstitucionales;

namespace TarantoWebCatalogo.Services.PaginasInstitucionales
{
    public interface IPaginasInstitucionalesService
    {
        PaginaInstitucional ObtenerPaginaPorIdiomaySeccion(string idioma, int id);
    }
}
