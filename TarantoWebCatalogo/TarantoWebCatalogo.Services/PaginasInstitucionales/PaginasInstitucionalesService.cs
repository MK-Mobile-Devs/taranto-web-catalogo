﻿using AutoMapper;
using TarantoWebCatalogo.Data;
using TarantoWebCatalogo.Data.Repositories.PaginasInstitucionales;
using TarantoWebCatalogo.ViewModels.PaginasInstitucionales;

namespace TarantoWebCatalogo.Services.PaginasInstitucionales
{
    public class PaginasInstitucionalesService : IPaginasInstitucionalesService
    {
        private readonly IPaginasInstitucionalesRepository _paginasInstitucionalRepository;

        public PaginasInstitucionalesService(IPaginasInstitucionalesRepository paginasInstitucionalRepository)
        {
            _paginasInstitucionalRepository = paginasInstitucionalRepository;
        }

        public PaginaInstitucional ObtenerPaginaPorIdiomaySeccion(string idioma, int id)
        {
            var paginaInstitucional = _paginasInstitucionalRepository.ObtenerPorIdEIdioma(idioma, id);
            return Mapper.Map<MVC_PaginasInstitucionales_ObtenerPorIdEIdioma_Result, PaginaInstitucional>(paginaInstitucional);
        }

    }
}
