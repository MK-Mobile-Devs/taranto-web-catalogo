﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TarantoWebCatalogo.Data.Entities;
using TarantoWebCatalogo.ViewModels.Perfil;

namespace TarantoWebCatalogo.Services.Perfil
{
    public interface IPerfilService
    {
        EdicionDatos ObtenerEditarMisDatos(string clientId);
        void CambiarMisDatos(string data);
        decimal CargarFotoPuntoVenta(CargarFotosPuntoVentaViewModel viewModel);
        decimal CambiarMisDatosInfoSerialized(string clientId, string infoSerialized);
        void ActualizarMailEnviado(decimal id);
    }
}
