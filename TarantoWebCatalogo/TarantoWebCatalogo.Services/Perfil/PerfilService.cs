﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TarantoWebCatalogo.Data.Entities;
using TarantoWebCatalogo.Data.Repositories.Perfil;
using TarantoWebCatalogo.Data.Repositories.Utilidades;
using TarantoWebCatalogo.ViewModels.Perfil;

namespace TarantoWebCatalogo.Services.Perfil
{
    public class PerfilService : IPerfilService
    {
        private IPerfilRepository perfilRepository;

        public PerfilService(IPerfilRepository perfilRepository)
        {
            this.perfilRepository = perfilRepository;
        }

        public void CambiarMisDatos(string data)
        {
            perfilRepository.CambiarMisDatos(data);
        }

        public decimal CargarFotoPuntoVenta(CargarFotosPuntoVentaViewModel viewModel)
        {
            return perfilRepository.CargarFotoPuntoVenta(viewModel);
        }
        public decimal CambiarMisDatosInfoSerialized(string clientId, string infoSerialized)
        {
            return perfilRepository.CambiarMisDatosInfoSerialized(clientId, infoSerialized);

        }

        public EdicionDatos ObtenerEditarMisDatos(string clientId)
        {
            return perfilRepository.ObtenerEdicionDatos(clientId);
        }

        public void ActualizarMailEnviado(decimal id)
        {
            perfilRepository.ActualizarMailEnviado(id);
        }
    }
}
