﻿using System.Collections.Generic;
using TarantoWebCatalogo.ViewModels.Utilidades;

namespace TarantoWebCatalogo.Services.Utilidades
{
    public interface ICarrouselService
    {
        List<ContenidoCarrousel> ObtenerTodosPorIdioma(string idioma);
    }
}