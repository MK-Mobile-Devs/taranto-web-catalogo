﻿using System.Collections.Generic;
using AutoMapper;
using TarantoWebCatalogo.Data;
using TarantoWebCatalogo.Data.Repositories.Utilidades;
using TarantoWebCatalogo.ViewModels.Utilidades;

namespace TarantoWebCatalogo.Services.Utilidades
{
    public class CarrouselService : ICarrouselService
    {
        private readonly ICarrouselRepository _carrouselRepository;

        public CarrouselService(ICarrouselRepository carrouselRepository)
        {
            _carrouselRepository = carrouselRepository;
        }

        public List<ContenidoCarrousel> ObtenerTodosPorIdioma(string idioma)
        {
            var contenidos = _carrouselRepository.ObtenerTodosPorIdioma(idioma);
            return Mapper.Map<List<MVC_CarrouselContenidos_ObtenerTodosPorIdioma_Result>, List<ContenidoCarrousel>>(contenidos);
        }
    }
}