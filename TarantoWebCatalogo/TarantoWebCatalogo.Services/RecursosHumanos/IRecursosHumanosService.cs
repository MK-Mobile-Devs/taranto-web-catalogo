﻿using System.Collections.Generic;
using TarantoWebCatalogo.ViewModels.RecursosHumanos;

namespace TarantoWebCatalogo.Services.RecursosHumanos
{
    public interface IRecursosHumanosService
    {
        List<Busqueda> ObtenerTodasPorIdioma(string idioma);

        void Agregar(int idRrhh, string nombre, string dni, string nacionalidad, string provincia, string ciudad, string direccion
            , string email, string telefono, string fechaDeNacimiento, string sexo, string mensaje, string nombreArchivo);
    }
}