﻿using System.Collections.Generic;
using AutoMapper;
using TarantoWebCatalogo.Data;
using TarantoWebCatalogo.Data.Repositories.RecursosHumanos;
using TarantoWebCatalogo.ViewModels.RecursosHumanos;

namespace TarantoWebCatalogo.Services.RecursosHumanos
{
    public class RecursosHumanosService : IRecursosHumanosService
    {
        private readonly IRecursosHumanosRepository _recursosHumanosRepository;

        public RecursosHumanosService(IRecursosHumanosRepository recursosHumanosRepository)
        {
            _recursosHumanosRepository = recursosHumanosRepository;
        }

        public void Agregar(int idRrhh, string nombre, string dni, string nacionalidad, string provincia, string ciudad, 
            string direccion, string email, string telefono, string fechaDeNacimiento, string sexo, string mensaje, string nombreArchivo)
        {
            _recursosHumanosRepository.Agregar(idRrhh, nombre, dni, nacionalidad, provincia,
                ciudad, direccion, email, telefono, fechaDeNacimiento, sexo, mensaje, nombreArchivo);
        }

        public List<Busqueda> ObtenerTodasPorIdioma(string idioma)
        {
            var busquedas = _recursosHumanosRepository.ObtenerTodasPorIdioma(idioma);
            return Mapper.Map<List<MVC_RecursosHumanosBusquedas_ObtenerTodasPorIdioma_Result>, List<Busqueda>>(
                busquedas);
        }
    }
}
