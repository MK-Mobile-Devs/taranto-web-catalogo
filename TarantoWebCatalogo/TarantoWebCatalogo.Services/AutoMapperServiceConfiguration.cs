﻿using AutoMapper;
using TarantoWebCatalogo.Data;
using TarantoWebCatalogo.ViewModels.ListaDePrecios;
using TarantoWebCatalogo.ViewModels.Novedades;
using TarantoWebCatalogo.ViewModels.PaginasInstitucionales;
using TarantoWebCatalogo.ViewModels.Pedidos;
using TarantoWebCatalogo.ViewModels.Productos;
using TarantoWebCatalogo.ViewModels.RecursosHumanos;
using TarantoWebCatalogo.ViewModels.Utilidades;

namespace TarantoWebCatalogo.Services
{
    public class AutoMapperServiceConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile(new UtilidadesProfile());
                cfg.AddProfile(new NovedadProfile());
                cfg.AddProfile(new ProductosProfile());
                cfg.AddProfile(new PaginaInstitucionalProfile());
                cfg.AddProfile(new RecursosHumanosProfile());
                cfg.AddProfile(new PedidoProfile());
                cfg.AddProfile(new ListaDePreciosProfile());
            });
        }
    }

    public class PedidoProfile : Profile
    {
        public PedidoProfile()
        {
            CreateMap<MVC_Pedidos_ObtenerPorUsuario_Result, PedidoDetail>();
            CreateMap<MVC_Pedidos_ObtenerArticulosParaIconoCarrito_Result, PedidoDetail>();
            CreateMap<MVC_Pedidos_ObtenerTodosLosArticulosDePedidosPendientes_Result, PedidoDetail>();
            CreateMap<MVC_Pedidos_ObtenerPedidosParaExportar_Result, PedidoProcesado>();
        }
    }

    public class RecursosHumanosProfile : Profile
    {
        public RecursosHumanosProfile()
        {
            CreateMap<MVC_RecursosHumanosBusquedas_ObtenerTodasPorIdioma_Result, Busqueda>();
        }
    }

    public class PaginaInstitucionalProfile : Profile
    {
        public PaginaInstitucionalProfile()
        {
            CreateMap<MVC_PaginasInstitucionales_ObtenerPorIdEIdioma_Result, PaginaInstitucional>();
        }
    }

    public class UtilidadesProfile : Profile
    {
        public UtilidadesProfile()
        {
            CreateMap<MVC_CarrouselContenidos_ObtenerTodosPorIdioma_Result, ContenidoCarrousel>();
        }
    }

    public class NovedadProfile : Profile
    {
        public NovedadProfile()
        {
            CreateMap<MVC_Novedades_ObtenerCategoriasParaDropdownPorIdioma_Result, ViewModels.ParaDropdown>();
            CreateMap<MVC_Novedades_ObtenerUltimasPorIdioma_Result, Novedad>();
            CreateMap<MVC_Novedades_ObtenerPorId_Result, Novedad>();
            CreateMap<MVC_Novedades_ObtenerUltimasPorCategoria_Result, Novedad>();
        }
    }

    public class ProductosProfile : Profile
    {
        public ProductosProfile()
        {
            CreateMap<MVC_Productos_ObtenerTodosParaMenuPorIdioma_Result, MenuProducto>();
            CreateMap<MVC_Productos_ObtenerPorId_Result, ProductoDetalle>();
        }
    }

    public class ListaDePreciosProfile : Profile
    {
        public ListaDePreciosProfile()
        {
            CreateMap<MVC_ListaDePrecios_ObtenerTodas_Result, ListaDePrecio>();
        }
    }
}
