﻿using System.Collections.Generic;
using TarantoWebCatalogo.ViewModels.Novedades;

namespace TarantoWebCatalogo.Services.Novedades
{
    public interface INovedadesService
    {
        List<Novedad> ObtenerUltimasPorIdioma(string idioma);
        List<ViewModels.ParaDropdown> ObtenerCategoriasParaDropdownPorIdioma(string idioma);
        Novedad ObtenerPorId(string idioma, int id);
        List<Novedad> ObtenerUltimasPorCategoria(string idioma, int idCategoria, int idNovedadAFiltrar);
    }
}
