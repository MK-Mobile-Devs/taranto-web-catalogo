﻿using AutoMapper;
using TarantoWebCatalogo.Data;
using System.Collections.Generic;
using TarantoWebCatalogo.ViewModels.Novedades;
using TarantoWebCatalogo.Data.Repositories.Novedades;
using TarantoWebCatalogo.ViewModels;

namespace TarantoWebCatalogo.Services.Novedades
{
    public class NovedadesService : INovedadesService
    {
        private readonly INovedadesRepository _novedadesRepository;

        public NovedadesService(INovedadesRepository novedadesRepository)
        {
            _novedadesRepository = novedadesRepository;
        }

        public List<ParaDropdown> ObtenerCategoriasParaDropdownPorIdioma(string idioma)
        {
            var categorias = _novedadesRepository.ObtenerCategoriasParaDropdownPorIdioma(idioma);
            return Mapper.Map<List<MVC_Novedades_ObtenerCategoriasParaDropdownPorIdioma_Result>, List<ParaDropdown>>(categorias);
        }

        public List<Novedad> ObtenerUltimasPorCategoria(string idioma, int idCategoria, int idNovedadAFiltrar)
        {
            var novedades = _novedadesRepository.ObtenerUltimasPorCategoria(idioma, idCategoria, idNovedadAFiltrar);
            return Mapper.Map<List<MVC_Novedades_ObtenerUltimasPorCategoria_Result>, List<Novedad>>(novedades);
        }

        public Novedad ObtenerPorId(string idioma, int id)
        {
            var novedad = _novedadesRepository.ObtenerPorId(idioma, id);
            return Mapper.Map<MVC_Novedades_ObtenerPorId_Result, Novedad>(novedad);
        }

        public List<Novedad> ObtenerUltimasPorIdioma(string idioma)
        {
            var novedades = _novedadesRepository.ObtenerUltimasPorIdioma(idioma);
            return Mapper.Map<List<MVC_Novedades_ObtenerUltimasPorIdioma_Result>, List<Novedad>>(novedades);
        }
    }
}
