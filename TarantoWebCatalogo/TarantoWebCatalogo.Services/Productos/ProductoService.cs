﻿using AutoMapper;
using System.Collections.Generic;
using TarantoWebCatalogo.Data.Repositories.Productos;
using TarantoWebCatalogo.ViewModels.Productos;
using TarantoWebCatalogo.Data;

namespace TarantoWebCatalogo.Services.Productos
{
    public class ProductoService : IProductoService
    {
        private readonly IProductoRepository _productoRepository;

        public ProductoService(IProductoRepository productoRepository)
        {
            _productoRepository = productoRepository;
        }

        public ProductoDetalle ObtenerPorId(string idioma, int idProducto)
        {
            var producto = _productoRepository.ObtenerPorId(idioma, idProducto);
            return Mapper.Map<MVC_Productos_ObtenerPorId_Result, ProductoDetalle>(producto);
        }

        public List<MenuProducto> ObtenerTodosParaMenuPorIdioma(string idioma)
        {
            var productos = _productoRepository.ObtenerTodosParaMenuPorIdioma(idioma);
            return Mapper.Map<List<MVC_Productos_ObtenerTodosParaMenuPorIdioma_Result>, List<MenuProducto>>(productos);
        }
    }
}
