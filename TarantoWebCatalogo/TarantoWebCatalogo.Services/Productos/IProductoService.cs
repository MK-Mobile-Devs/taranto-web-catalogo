﻿using System.Collections.Generic;
using TarantoWebCatalogo.ViewModels.Productos;

namespace TarantoWebCatalogo.Services.Productos
{
    public interface IProductoService
    {
        List<MenuProducto> ObtenerTodosParaMenuPorIdioma(string idioma);
        ProductoDetalle ObtenerPorId(string idioma, int idProducto);
    }
}