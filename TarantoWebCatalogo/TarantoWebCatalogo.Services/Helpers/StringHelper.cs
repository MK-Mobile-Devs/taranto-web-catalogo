﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace TarantoWebCatalogo.Services.Helpers
{
    public static class StringHelper
    {
        public static string Sha256(string texto)
        {
            SHA256Managed crypt = new SHA256Managed();
            string hash = String.Empty;
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(texto), 0, Encoding.UTF8.GetByteCount(texto));
            foreach (byte bit in crypto)
            {
                hash += bit.ToString("x2");
            }
            return hash;
        }
    }
}
