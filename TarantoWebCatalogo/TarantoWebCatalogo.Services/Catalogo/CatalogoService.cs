﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using TarantoWebCatalogo.Data.Repositories.Perfil;
using TarantoWebCatalogo.ViewModels.Catalogo;

namespace TarantoWebCatalogo.Services.Catalogo
{
    public class CatalogoService : ICatalogoService
    {
        private readonly IPerfilRepository perfilRepository;

        public CatalogoService(IPerfilRepository perfilRepository)
        {
            this.perfilRepository = perfilRepository;
        }

        public async Task<List<Marca>> ObtenerMotoresParaDropdown()
        {
            using (var client = new HttpClient())
            {
                var content = await client.GetStringAsync("https://catalogov3.taranto.com.ar:8992/API/catalogo/marcas");
                return JsonConvert.DeserializeObject<List<Marca>>(content);
            }
        }

    }
}
