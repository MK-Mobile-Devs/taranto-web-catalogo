﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TarantoWebCatalogo.ViewModels.Catalogo;

namespace TarantoWebCatalogo.Services.Catalogo
{
    public interface ICatalogoService
    {
        Task<List<Marca>> ObtenerMotoresParaDropdown();
    }
}