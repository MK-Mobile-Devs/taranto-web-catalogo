﻿using System;
using TarantoWebCatalogo.Data.UnitOfWork;
using TarantoWebCatalogo.Services.BoletinLanzamiento;
using TarantoWebCatalogo.Services.Core;
using TarantoWebCatalogo.Services.ListaDePrecios;
using TarantoWebCatalogo.Services.Novedades;
using TarantoWebCatalogo.Services.PaginasInstitucionales;
using TarantoWebCatalogo.Services.Pedidos;
using TarantoWebCatalogo.Services.Perfil;
using TarantoWebCatalogo.Services.Productos;
using TarantoWebCatalogo.Services.RecursosHumanos;
using TarantoWebCatalogo.Services.Utilidades;

namespace TarantoWebCatalogo.Services
{
    public class ServiceFactory
    {
        private readonly IUnitOfWork _unitOfWork;
        private ICarrouselService _carrouselService;
        private INovedadesService _novedadesService;
        private IPaginasInstitucionalesService _paginasInstitucionalesService;
        private IProductoService _productoService;
        private ICookieService _cookieService;
        private ISeguridadService _seguridadService;
        private IRecursosHumanosService _recursosHumanosService;
        private IApiService _apiService;
        private IPedidosService _pedidosService;
        private IListaDePrecioService _listaDePrecioService;
        private IBoletinLanzamientoService _boletinLanzamientoService;
        private IEnvioMailService _envioMailService;
        private IPerfilService _perfilService;

        public ServiceFactory(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IApiService CrearApiService(string apiUrl)
        {
            return _apiService ?? (_apiService = new ApiService(apiUrl));
        }

        public IPerfilService CrearPerfilService()
        {
            return _perfilService ?? (_perfilService = new PerfilService(_unitOfWork.PerfilRepository));
        }

        public ICarrouselService CrearCarrouselService()
        {
            return _carrouselService ?? (_carrouselService = new CarrouselService(_unitOfWork.CarrouselRepository));
        }

        public INovedadesService CrearNovedadesService()
        {
            return _novedadesService ?? (_novedadesService = new NovedadesService(_unitOfWork.NovedadesRepository));
        }

        public IPaginasInstitucionalesService CrearPaginasInstitucionalesService()
        {
            return _paginasInstitucionalesService ?? (_paginasInstitucionalesService = new PaginasInstitucionalesService(_unitOfWork.PaginasInstitucionalRepository));
        }

        public IProductoService CrearProductoService()
        {
            return _productoService ?? (_productoService = new ProductoService(_unitOfWork.ProductoRepository));
        }

        public ICookieService CrearCookieService()
        {
            return _cookieService ?? (_cookieService = new CookieService(_unitOfWork.CookieRepository));
        }

        public ISeguridadService CrearSeguridadService(string apiUrl)
        {
            return _seguridadService ?? (_seguridadService = new SeguridadService(CrearCookieService(), CrearApiService(apiUrl)));
        }

        public IRecursosHumanosService CrearRecursosHumanosService()
        {
            return _recursosHumanosService ?? (_recursosHumanosService = new RecursosHumanosService(_unitOfWork.RecursosHumanosRepository));
        }

        public IPedidosService CrearPedidoService()
        {
            return _pedidosService ?? (_pedidosService = new PedidosService(_unitOfWork));
        }

        public IListaDePrecioService CrearListaDePrecioService()
        {
            return _listaDePrecioService ??
                   (_listaDePrecioService = new ListaDePrecioService(_unitOfWork.ListaDePrecioRepository));
        }

        public IBoletinLanzamientoService CrearBoletinLanzamientoService()
        {
            return _boletinLanzamientoService ??
                   (_boletinLanzamientoService =
                       new BoletinLanzamientoService(_unitOfWork.BoletinLanzamientoRepository));
        }

        public IEnvioMailService CrearEnvioMailService()
        {
            return _envioMailService ??
                   (_envioMailService = new EnvioMailService());
        }
    }
}
