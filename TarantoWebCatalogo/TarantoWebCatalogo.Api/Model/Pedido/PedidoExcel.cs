﻿namespace TarantoWebCatalogo.Api.Model.Pedido
{
    public class PedidoExcel
    {
        public string Usuario { get; set; }
        public string Articulo { get; set; }
        public string Descripcion { get; set; }
        public int Cantidad { get; set; }
        public decimal Subtotal { get; set; }
        public string Stock { get; set; }
        public bool Excel { get; set; }
        public string Observacion { get; set; }
    }
}