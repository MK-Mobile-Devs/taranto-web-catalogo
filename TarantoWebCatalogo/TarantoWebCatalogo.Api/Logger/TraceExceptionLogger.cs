﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.ExceptionHandling;
using log4net;
using Newtonsoft.Json;

namespace TarantoWebCatalogo.Api.Logger
{
    public class TraceExceptionLogger : ExceptionLogger
    {
        private readonly ILog _logger = LogManager.GetLogger("LogFileAppender");

        public override void Log(ExceptionLoggerContext context)
        {
            var postData = string.Empty;

            try
            {
                var data = ((System.Web.Http.ApiController)
                        context.ExceptionContext.ControllerContext.Controller)
                    .ActionContext.ActionArguments.Values;

                postData = JsonConvert.SerializeObject(data);
            }
            catch
            {
                // DO SOMETHING ??          
            }

            _logger.FatalFormat("Fatal error WEBAPI: URL:{ 0} METHOD: { 1} POST DATA:{ 2} Exception: { 3}",
                context.Request.RequestUri, context.Request.Method,
                postData, context.ExceptionContext.Exception);
        }
    }
}