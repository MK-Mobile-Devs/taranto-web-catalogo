﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using TarantoWebCatalogo.Api.Model.Pedido;
using TarantoWebCatalogo.Data.UnitOfWork;
using TarantoWebCatalogo.Services;
using TarantoWebCatalogo.Services.Pedidos;
using TarantoWebCatalogo.ViewModels.Pedidos;

namespace TarantoWebCatalogo.Api.Controllers.Catalogo
{
    [RoutePrefix("catalogo")]
    public class CatalogoController : BaseController
    {
        private readonly IPedidosService _pedidosService;
        //private static readonly ILog _TestLog = LogManager.GetLogger("LogFileAppender");


        public CatalogoController() : this(new UnitOfWork())
        {
            
        }

        public CatalogoController(IUnitOfWork unitOfWork)
        {
            var factory = new ServiceFactory(unitOfWork);
            _pedidosService = factory.CrearPedidoService();
        }

        [Route("pedidosEditarCantidadArticulo")]
        [HttpPatch]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public IHttpActionResult PedidosEditarCantidadArticulo(PedidoDetail pedido)
        {
            _pedidosService.EditarCantidad(pedido.Usuario, pedido.Articulo, pedido.Cantidad, pedido.IdPedido);
            return Ok();
            
        }

        [Route("obtenerArticulosParaIconoCarrito")]
        [HttpGet]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public IHttpActionResult ObtenerArticulosParaIconoCarrito(string usuario)
        {
            var pedido = _pedidosService.ObtenerArticulosParaIconoCarrito(usuario);
            if (!pedido.Any()) { return BadRequest();}
            return Ok(pedido);

        }

        [Route("agregarArticuloAlCarrito")]
        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public IHttpActionResult AgregarArticuloAlCarrito(PedidoDetail pedido)
        {
            var url = $"{System.Web.Configuration.WebConfigurationManager.AppSettings["ApiUrl"]}catalogo/articulosParaCarrito?codigo={pedido.Articulo}";
            var client = new WebClient();
            var response = client.DownloadString(url);
            var articuloActualizado = JsonConvert.DeserializeObject<List<PedidoDetail>>(response);


            if (!articuloActualizado.Any())
            {
                return BadRequest();
            }
            _pedidosService.Agregar(pedido.Usuario, pedido.Articulo, articuloActualizado.FirstOrDefault().Descripcion, 
                pedido.Cantidad, articuloActualizado.FirstOrDefault().PrecioFinal, 
                articuloActualizado.FirstOrDefault().Stock, false, null);

            return Ok();

        }
        
        [Route("insertarPedidoExcel")]
        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public IHttpActionResult InsertarPedidoExcel(Model.Pedido.PedidoExcel pedido)
        {
            try
            {
                _pedidosService.Agregar(pedido.Usuario, pedido.Articulo, pedido.Descripcion, 
                    pedido.Cantidad, pedido.Subtotal, pedido.Stock, pedido.Excel, pedido.Observacion);
                return Ok("Pedido cargado correctamente");
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        [Route("crearPedidoDesdeExcel")]
        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public IHttpActionResult CrearPedidoDesdeExcel([FromBody] PedidoDesdeExcel pedido)
        {
            try
            {
                _pedidosService.InsertarPedidoDesdeExcel(pedido);
                return Ok("Pedido cargado correctamente");
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        [Route("eliminarPedidoExcel")]
        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public IHttpActionResult EliminarPedidoExcel(string usuario)
        {
            try
            {
                _pedidosService.EliminarUltimoPedidoExcel(usuario);
                return Ok("Pedido eliminado correctamente");
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

        [Route("procesarPedidoExcel")]
        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public IHttpActionResult ProcesarPedidoExcel(PedidoProcesadoApi pedido)
        {
            try
            {
                _pedidosService.ProcesarPedidoExcel(pedido.Usuario);
                return Ok("Pedido procesado correctamente");
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }


        [Route("pedidoProcesado")]
        [HttpPatch]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public IHttpActionResult PedidoProcesado(PedidoProcesado pedido)
        {
            try
            {
                _pedidosService.ProcesarPedido(pedido.IdPedido, pedido.Observacion, pedido.Usuario);
                var mensaje = $"Procesado pedido: {pedido.IdPedido}, Observacion: {pedido.Observacion}, usuario: {pedido.Usuario}";
                return Ok(mensaje);
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }
    }
}
