﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using System.Web.Http.Cors;
using TarantoWebCatalogo.ViewModels.Distribuidores;

namespace TarantoWebCatalogo.Api.Controllers.Distribuidores
{
    [RoutePrefix("distribuidores")]
    public class DistribuidoresController : ApiController
    {
        [Route("detallePorSucursal")]
        [HttpGet]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public IHttpActionResult DetallePorSucursal(string clientId)
        {
            var url = $"{System.Web.Configuration.WebConfigurationManager.AppSettings["ApiUrl"]}distribuidores/detallePorSucursal?clientId={clientId}";
            var client = new WebClient();
            var response = client.DownloadString(url);
            return Ok(JsonConvert.DeserializeObject<List<DetalleDistribuidor>>(response));
        }
    }
}
