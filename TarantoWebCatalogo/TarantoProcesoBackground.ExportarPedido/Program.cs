﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading;
using TarantoWebCatalogo.Data.UnitOfWork;
using TarantoWebCatalogo.Services;
using TarantoWebCatalogo.Services.Pedidos;
using TarantoWebCatalogo.ViewModels.Pedidos;

namespace TarantoProcesoBackground.ExportarPedido
{
    static class Program
    {
        private static IUnitOfWork _unitOfWork;
        private static IPedidosService _pedidosService;
        private const string ExportadorTag = "Exportador";

        static void Main()
        {
            var mutexId = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
            using (var mutex = new Mutex(false, mutexId))
            {
                if (!mutex.WaitOne(0, false))
                {
                    return;
                }
                AutoMapperServiceConfiguration.Configure();
                _unitOfWork = new UnitOfWork();
                var factory = new ServiceFactory(_unitOfWork);
                _pedidosService = factory.CrearPedidoService();

                try
                {
                    Exportar();
                }
                catch (Exception e)
                {
                    _pedidosService.LogDeAccion(ExportadorTag, e.ToString());
                    Console.WriteLine(e.ToString());
                    throw;
                }
            }
        }

        private static void Exportar()
        {
            _pedidosService.LogDeAccion(ExportadorTag, "START -> Exportador de pedidos");

            var pedidosPendientes = _pedidosService.ObtenerPedidosParaExportar();
            if (pedidosPendientes.Count == 0)
            {
                _pedidosService.LogDeAccion(ExportadorTag, "No hay pedidos para exportar");
                return;
            }
            foreach (var pedidoPendiente in pedidosPendientes)
            {
                ProcesarPedido(pedidoPendiente);
            }
            _pedidosService.LogDeAccion(ExportadorTag, "STOP -> Exportador de pedidos");
        }

        private static void ProcesarPedido(PedidoProcesado pedidoAExportar)
        {
            var articulos = _pedidosService.ObtenerDetallePedidosProcesadosPorIdPedido(pedidoAExportar.IdPedido);
            var articulosDePedidoAOracle = new List<Articulo>(articulos.Capacity);

            foreach (var articulo in articulos)
            {
                articulosDePedidoAOracle.Add(new Articulo
                {
                    CodigoArticulo = articulo.Articulo,
                    Cantidad = articulo.Cantidad
                });
            }
            
            var pedido = new PedidoAOracle
            {
                ClienteId = pedidoAExportar.Usuario,
                Observacion = pedidoAExportar.Observacion,
                Plataforma = "WEB",
                Items = articulosDePedidoAOracle
            };

            if (pedido.Items.Any())
            {
                _pedidosService.LogDeAccion(ExportadorTag, $"Se va a exportar el pedido {pedidoAExportar.IdPedido}");
                ExportarPedido(pedido, pedidoAExportar.IdPedido);
            }
            else
            {
                _pedidosService.LogDeAccion(ExportadorTag, $"ERROR! El pedido {pedidoAExportar.IdPedido} no tiene items");
            }
        }

        private static void ExportarPedido(PedidoAOracle pedido, int idPedido)
        {
            using (var client = new HttpClient())
            {
                var urlApiV3 = ConfigurationManager.AppSettings["ApiUrl"];
                client.BaseAddress = new Uri(urlApiV3 + "catalogo/insertarPedido");

                var postTask = client.PostAsJsonAsync("insertarPedido", pedido);
                postTask.Wait();

                var result = postTask.Result;
                if (!result.IsSuccessStatusCode)
                {
                    _pedidosService.LogDeAccion(ExportadorTag, $"Error al exportar el pedido: {postTask.Result.Content.ReadAsStringAsync().Result}");
                    return;
                }

                var idOracle = int.Parse(postTask.Result.Content.ReadAsStringAsync().Result.Replace("\"", ""));
                _pedidosService.ExportarPedido(pedido.ClienteId, idPedido,idOracle);
                _pedidosService.LogDeAccion(ExportadorTag, $"Pedido exportado Ok con IdOracle:{idOracle}");
            }
        }

        private class Articulo
        {
            public string CodigoArticulo { get; set; }
            public int Cantidad { get; set; }
        }

        private class PedidoAOracle
        {
            public PedidoAOracle()
            {
                Items = new List<Articulo>();
            }
            public string ClienteId { get; set; }
            public string Observacion { get; set; }
            public string Plataforma { get; set; }

            public List<Articulo> Items { get; set; }
        }
    }
}
